import json
import sys
from pprint import pprint

with open(sys.argv[1]) as f:
    doc = json.load(f)
    features = doc["features"]
    points = []
    for feature in features:
        coordinate = feature["geometry"]["coordinates"];
        points.append(coordinate)
    path = {"type": "Feature", "properties": {}, "geometry": {"type": "LineString", "coordinates": points}}
    features.append(path)
    print(json.dumps(doc, indent=4))
