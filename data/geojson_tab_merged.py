import json
import sys
from pprint import pprint

with open(sys.argv[1]) as f:
    doc = json.load(f)
    features = doc["features"]
    points = []
    for feature in features:
        gtype = feature["geometry"]["type"]
        if gtype != "Point":
            continue
        coordinate = feature["geometry"]["coordinates"]
        name = feature["properties"]["label"]
        index = feature["properties"]["index"]
        times = feature["properties"]["times"]
        points.append({
            "label": name,
            "index": index,
            "coordinate": coordinate,
            "times": times
        })
    for p in points:
        print("{}, {}, {}, {}, {}".format(p['label'], p['index'], p['coordinate'][0], p['coordinate'][1], ', '.join(p['times'])))
