#ifndef MERGEDIALOG_H
#define MERGEDIALOG_H

#include <QDialog>
#include "tracktreemodel.h"
#include <marble/MarbleWidget.h>

namespace Ui {
class MergeDialog;
}

class MergeDialog : public QDialog{
  Q_OBJECT
  
  typedef std::pair<stmr::state_triangle, stmr::merged_data> pair_type;
  typedef std::vector<pair_type> polygon_collection_type;
  
  QColor _color;
  std::vector<stmr::merged_data> _merged_collection;
  polygon_collection_type _iterations;
  ObservationTrackLayer* _observationTrack;
  MergedTrackLayer* _mergedTrack;
  public:
    explicit MergeDialog(TrackTreeModel* treeModel, QModelIndex observationIndex = QModelIndex(), QModelIndex mergedIndex = QModelIndex(), QWidget *parent = nullptr);
    ~MergeDialog();
  private:
    Ui::MergeDialog* ui;
  public slots:
    void colorSelectionClicked();
    void okaySlot();
  public:
    QColor color() const;
    QString name() const;
    MergedTrackLayer* createMergedLayer(Marble::MarbleWidget* widget, MergedTrackLayer* layer_old = 0x0) const;
};

#endif // MERGEDIALOG_H
