#ifndef TREEMODELAUX_H
#define TREEMODELAUX_H

#include <vector>
#include <QModelIndex>

template <typename T>
struct AbstractTreeModelAUXItem{
    typedef T* data_ptr_type;
    typedef AbstractTreeModelAUXItem<T> AbstractType;
    data_ptr_type _data;

    AbstractTreeModelAUXItem(data_ptr_type d = 0x0): _data(d){}
    data_ptr_type actual() const{return _data;}

    virtual AbstractTreeModelAUXItem<T>* parentItem() const = 0;
    virtual AbstractTreeModelAUXItem<T>* item(std::size_t index) const{return 0x0;}
    virtual std::size_t position() const = 0;
    virtual std::size_t count() const = 0;
    virtual AbstractTreeModelAUXItem<T>* lastItem() const = 0;
    virtual int depth() const = 0;
    const AbstractTreeModelAUXItem<T>* self() const{return this;}
  public:
    virtual QModelIndex populate(data_ptr_type data){return QModelIndex();}
    virtual void clear(){}
    const AbstractTreeModelAUXItem<T>* ancestor(int depth) const{
        const AbstractTreeModelAUXItem<T>* p = this;
        while(p && p->depth() != depth){
            p = p->parentItem();
        }
        return p;
    }
    AbstractTreeModelAUXItem<T>* ancestor(int depth){
        AbstractTreeModelAUXItem<T>* p = this;
        while(p && p->depth() != depth){
            p = p->parentItem();
        }
        return p;
    }
};

template <typename T, int Max, int Depth=-1>
struct TreeModelAUXItem: public AbstractTreeModelAUXItem<T>{
    typedef AbstractTreeModelAUXItem<T> base_type;
    typedef typename base_type::data_ptr_type data_ptr_type;
    typedef TreeModelAUXItem<T, Max, Depth-1> parent_type;
    typedef TreeModelAUXItem<T, Max, Depth+1> child_type;
    typedef parent_type* parent_ptr_type;
    typedef child_type*  child_ptr_type;
   
    parent_type*                 _parent;
    std::size_t                  _index;
    std::vector<child_ptr_type>  _children;
    
    TreeModelAUXItem(typename base_type::data_ptr_type d=0x0): _parent(0x0), base_type(d){}

    void add(child_ptr_type child){
        child->_parent = this;
        child->_index  = _children.size();
        _children.push_back(child);
    }
    child_ptr_type create(data_ptr_type data){
        child_ptr_type child = new child_type(data);
        add(child);
        return child;
    }
    child_ptr_type at(std::size_t index) const{return _children[index];}
    virtual AbstractTreeModelAUXItem<T>* item(std::size_t index) const{return at(index);}
    virtual std::size_t position() const{return _index;}
    virtual std::size_t count() const{return _children.size();}
    virtual AbstractTreeModelAUXItem<T>* lastItem() const{return _children.back();}
    virtual AbstractTreeModelAUXItem<T>* parentItem() const{return _parent;}
    virtual int depth() const{return Depth;}
    virtual void clear(){_children.clear();}
};

template <typename T, int Depth>
struct TreeModelAUXItem<T, Depth, Depth>: public AbstractTreeModelAUXItem<T>{
    typedef AbstractTreeModelAUXItem<T>         base_type;
    typedef TreeModelAUXItem<T, Depth, Depth-1> parent_type;
    typedef TreeModelAUXItem<T, Depth, Depth+1> child_type;
    typedef child_type*                         child_ptr_type;
    typedef parent_type*                        parent_ptr_type;
   
    parent_type* _parent;
    std::size_t  _index;
    
    TreeModelAUXItem(typename base_type::data_ptr_type d = 0x0): _parent(0x0), base_type(d){}
    virtual std::size_t position() const{return _index;}
    virtual std::size_t count() const{return 0;}
    virtual AbstractTreeModelAUXItem<T>* lastItem() const{return 0x0;}
    virtual AbstractTreeModelAUXItem<T>* parentItem() const{return _parent;}
    virtual int depth() const{return Depth;}
};

template <typename T, int Max> using TreeModelAUXRootItem = TreeModelAUXItem<T, Max, -1>;

template <typename T>
struct checkable_trait{
    typedef AbstractTreeModelAUXItem<T> AbstractType;
    
    int _checkdepth;
    mutable QMap<QModelIndex, bool> _checkstates;
    
    checkable_trait(): _checkdepth(){}
    void setCheckDepth(int depth){
        _checkstates.clear();
        _checkdepth = depth;
    }
    QList<QModelIndex> selectedIndexes() const{
        QList<QModelIndex> indexes;
        QMap<QModelIndex, bool>::const_iterator i = _checkstates.constBegin();
        while (i != _checkstates.constEnd()) {
            if(i.value()){
                indexes << i.key();
            }
            ++i;
        }
        return indexes;
    }
    int checkdepth() const{return _checkdepth;}
    void addRecord(const QModelIndex index){
        _checkstates[index] = false;
    }
    bool setCheckState(const QModelIndex& index, const QVariant& value, int role){
        AbstractType* node = static_cast<AbstractType*>(index.internalPointer());
        if(node->depth() == _checkdepth){
            _checkstates[index] = (value == Qt::Checked);
            return true;
        }else{
            return false;
        }
        return false;
    }
    QVariant checkState(const QModelIndex &index) const{
        AbstractType* node = static_cast<AbstractType*>(index.internalPointer());
        if(node->depth() == _checkdepth){
            if(!_checkstates.contains(index)){
                _checkstates[index] = false;
            }
            return _checkstates[index] ? Qt::Checked : Qt::Unchecked;
        }else{
            QVariant();
        }
        return QVariant();
    }
    Qt::ItemFlags flags(const QModelIndex &index, Qt::ItemFlags flag) const{
        AbstractType* node = static_cast<AbstractType*>(index.internalPointer());
        if(node->depth() == _checkdepth){
            return flag | Qt::ItemIsUserCheckable;
        }
        return flag;
    }
};

template <typename T>
struct nocheckable_trait{
    bool setCheckState(const QModelIndex& index, const QVariant& value, int role){return false;}
    QVariant checkState(const QModelIndex &index) const{return QVariant();}
    Qt::ItemFlags flags(const QModelIndex &index, Qt::ItemFlags flag) const{return flag;}
    void addRecord(){}
    QList<QModelIndex> selectedIndexes() const{return QList<QModelIndex>();}
};

template <typename T, int Max, template<typename> typename C=checkable_trait>
struct TreeModelAUX: public TreeModelAUXRootItem<T, Max>, C<T>{
    typedef TreeModelAUXItem<T, Max> base_type;
    typedef typename base_type::data_ptr_type data_ptr_type;
    typedef typename base_type::AbstractType AbstractType;
    typedef std::vector<data_ptr_type> records_collection;
    typedef C<T> check_trait;
    
    records_collection _records;
    bool _table_mode;
    
    TreeModelAUX(typename base_type::data_ptr_type d = 0x0): TreeModelAUXRootItem<T, Max>(d), _table_mode(false){}
    void setTableMode(bool enable = true){
        _table_mode = enable;
    }
    int rowCount(const QModelIndex &parent = QModelIndex()) const{
        if (!parent.isValid()){
            return this->count();
        }else{
            return !_table_mode ? static_cast<AbstractType*>(parent.internalPointer())->count() : 0;
        }
    }
    int columnCount(const QModelIndex& parent = QModelIndex()) const{
        AbstractType* node = static_cast<AbstractType*>(parent.internalPointer());
        return this->ncolumns(node);
    }
    virtual int ncolumns(AbstractType* node) const{
        return 1;
    }
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const{
        if (!index.isValid())
            return QVariant();
        
        if(role == Qt::CheckStateRole){
            return check_trait::checkState(index);
        }

        AbstractType* item = static_cast<AbstractType*>(index.internalPointer());
        return value(item, index.column(), role);
    }
    
    Qt::ItemFlags flags(const QModelIndex& index, Qt::ItemFlags flag) const{
        return check_trait::flags(index, flag);
    }
    
    bool setData(const QModelIndex& index, const QVariant& value, int role){
        if(role == Qt::CheckStateRole){
            return check_trait::setCheckState(index, value, role);
        }
        return false;
    }
    
    void addRecord(data_ptr_type data){
        _records.push_back(data);
        QModelIndex index = populate(data);
        check_trait::addRecord(index);
    }
    
    void removeRecord(data_ptr_type data){
        // TODO remove record indexes from check check_trait
        // TODO remove aux tree nodes
        // TODO remove record from _records
    }
    
    std::vector<AbstractType*> selectedItems() const{
        QList<QModelIndex> indexes = check_trait::selectedIndexes();
        std::vector<AbstractType*> items;
        for(QList<QModelIndex>::const_iterator i = indexes.begin(); i != indexes.end(); ++i){
            items.push_back(static_cast<AbstractType*>(i->internalPointer()));
        }
        return items;
    }
    typename records_collection::const_iterator begin() const{return _records.begin();}
    typename records_collection::const_iterator end() const{return _records.end();}
  protected:
    virtual QVariant value(AbstractType* item, std::size_t column, int role) const = 0;
    virtual QModelIndex populate(data_ptr_type data) = 0;
};

#endif // TREEMODELAUX_H
