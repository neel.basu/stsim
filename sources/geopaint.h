#ifndef GEOPAINT_H
#define GEOPAINT_H

#include "stmr.h"
#include <QPen>
#include <QRect>
#include <marble/GeoPainter.h>
#include <marble/GeoDataCoordinates.h>

namespace internal{
template <typename T>
struct geopaint;

using namespace Marble;

template <>
struct geopaint<stmr::observation_data>{
    static GeoDataCoordinates coordinate(const stmr::observation_data& data){
        return GeoDataCoordinates(data.location.lng, data.location.lat, 0.0, GeoDataCoordinates::Degree);
    }
    static void draw_point(GeoPainter* painter, const stmr::observation_data& data, const QPen& pen){
        QBrush brush = painter->brush();
        painter->setBrush(pen.brush());
        painter->drawEllipse(coordinate(data), 10, 10);
        painter->setBrush(brush);
    }
    static bool bubbleRight(){
        return true;
    }
    static unsigned height(const stmr::observation_data& data, unsigned basic_height){
        return basic_height;
    }
};

template <>
struct geopaint<stmr::merged_data>{
    static GeoDataCoordinates coordinate(const stmr::merged_data& data){
        return GeoDataCoordinates(data.location.lng, data.location.lat, 0.0, GeoDataCoordinates::Degree);
    }
    static void draw_point(GeoPainter* painter, const stmr::merged_data& data, const QPen& pen){
        QBrush brush = painter->brush();
        painter->setBrush(Qt::transparent);
        painter->drawEllipse(coordinate(data), 17, 17);
        painter->setBrush(brush);
    }
    static bool bubbleRight(){
        return false;
    }
    static unsigned height(const stmr::merged_data& data, unsigned basic_height){
        return std::ceil((double)data.times.size()/2.0)*basic_height;
    }
};

namespace paint{
QRect highlightSquare(GeoPainter* painter, const stmr::gps_coordinate& point, const QPen& pen);
QRect highlightBrace(GeoPainter* painter, const stmr::gps_coordinate& point, const QPen& pen);
QRect highlightBubble(GeoPainter* painter, const stmr::gps_coordinate& point, const QPen& pen, int height, int width, bool right, int length=150);
void highlightBubbleText(GeoPainter* painter, const stmr::observation_data& data, const QRect& rect, const QPen& pen);
void highlightBubbleText(GeoPainter* painter, const stmr::merged_data& data, const QRect& rect, const QPen& pen);
void highlightBubbleText(GeoPainter* painter, const stmr::intersection& data, const QRect& rect, const QPen& pen);
void highlight(GeoPainter* painter, const stmr::observation_data& data, const QPen& pen, int height, int width, int length=150);
}

};

#endif // GEOPAINT_H
