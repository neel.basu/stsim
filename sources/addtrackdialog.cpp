#include "addtrackdialog.h"
#include "ui_addtrackdialog.h"
#include <QFileDialog>
#include <QColorDialog>

AddTrackDialog::AddTrackDialog(TrackType type, QWidget *parent): QDialog(parent), ui(new Ui::AddTrackDialog){
    ui->setupUi(this);
    
    _color = Qt::blue;
    QPalette pal = ui->colorButton->palette();
    pal.setColor(QPalette::Button, Qt::blue);
    ui->colorButton->setAutoFillBackground(true);
    ui->colorButton->setPalette(pal);
    ui->colorButton->update();
    
    connect(ui->browseButton, SIGNAL(clicked()), this, SLOT(fileSelectionClicked()));
    connect(ui->colorButton, SIGNAL(clicked()), this, SLOT(colorSelectionClicked()));
    
    if(type == MergedTrack){
        ui->typeComboBox->setCurrentIndex(1);
    }
}

AddTrackDialog::~AddTrackDialog(){
    delete ui;
}

void AddTrackDialog::fileSelectionClicked(){
    QString path = QFileDialog::getOpenFileName(this);
    ui->lineEdit->setText(path);
}

void AddTrackDialog::colorSelectionClicked(){
    _color = QColorDialog::getColor(Qt::blue, this);
    QPalette pal = ui->colorButton->palette();
    pal.setColor(QPalette::Button, _color);
    ui->colorButton->setAutoFillBackground(true);
    ui->colorButton->setPalette(pal);
    ui->colorButton->update();
}

QString AddTrackDialog::path() const{
    return ui->lineEdit->text();
}

QColor AddTrackDialog::color() const{
    return _color;
}

TrackType AddTrackDialog::type() const{
    if(ui->typeComboBox->currentIndex() == 0){
        return ObservationTrack;
    }else{
        return MergedTrack;
    }
}

QString AddTrackDialog::name() const{
    return ui->nameEdit->text();
}
