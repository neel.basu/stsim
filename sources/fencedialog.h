#ifndef FENCEDIALOG_H
#define FENCEDIALOG_H

#include <QDialog>
#include <QDateTime>
#include <marble/LatLonEdit.h>
#include <marble/GeoDataCoordinates.h>

namespace Ui {
class FenceDialog;
}

class FenceDialog : public QDialog{
  Q_OBJECT
  
  Marble::LatLonEdit* _latEdit;
  Marble::LatLonEdit* _lonEdit;
  
  public:
    explicit FenceDialog(QWidget *parent = nullptr);
    ~FenceDialog();
  private:
    Ui::FenceDialog *ui;
  public:
    void setLocation(const Marble::GeoDataCoordinates& coordinates);
    void setRadius(double radius_in_meters);
  public:
    QDateTime start() const;
    QDateTime end() const;
};

#endif // FENCEDIALOG_H
