#ifndef ADDOBSERVATIONDIALOG_H
#define ADDOBSERVATIONDIALOG_H

#include <QDialog>
#include "tracklayer.h"

namespace Ui {
class AddTrackDialog;
}

class AddTrackDialog : public QDialog{
  Q_OBJECT
  QColor _color;
  public:
    explicit AddTrackDialog(TrackType type, QWidget *parent = nullptr);
    ~AddTrackDialog();

  private:
    Ui::AddTrackDialog *ui;
  public slots:
    void fileSelectionClicked();
    void colorSelectionClicked();
  public:
    QString path() const;
    QColor color() const;
    TrackType type() const;
    QString name() const;
    
};

#endif // ADDOBSERVATIONDIALOG_H
