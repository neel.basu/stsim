#include "stsimulator.h"
#include "stmr.h"
#include <QApplication>
#include <marble/MarbleWidget.h>
#include <marble/MarbleModel.h>
#include <boost/format.hpp>
#include "tracklayer.h"
#include <QNetworkProxy>
#include <QDebug>

int main(int argc, char *argv[]){
    QApplication app(argc,argv);
   
    const std::string project_home = "..";
        
    STSimulator main;
    
    QNetworkProxy proxy;
    proxy.setType(QNetworkProxy::HttpProxy);
    proxy.setHostName("192.168.250.21");
    proxy.setPort(3128);
    QNetworkProxy::setApplicationProxy(proxy);

    main.addGeoJSONObservationTrack(project_home+"/data/track1.geojson", "H1")->setColor(Qt::blue);
    main.addGeoJSONObservationTrack(project_home+"/data/track2.geojson", "H2")->setColor(Qt::green);

    main.addGeoJSONObservationTrack(project_home+"/data/c1.geojson", "C1")->setColor(Qt::blue);
    main.addGeoJSONObservationTrack(project_home+"/data/c2.geojson", "C2")->setColor(Qt::green);
    main.addGeoJSONObservationTrack(project_home+"/data/c3.geojson", "C3")->setColor(Qt::yellow);
    main.addGeoJSONObservationTrack(project_home+"/data/c4.geojson", "C4")->setColor(Qt::magenta);
    main.addGeoJSONObservationTrack(project_home+"/data/c5.geojson", "C5")->setColor(Qt::cyan);
    main.addGeoJSONObservationTrack(project_home+"/data/c6.geojson", "C6")->setColor(Qt::black);

    main.addGeoJSONObservationTrack(project_home+"/data/d1.geojson", "D1")->setColor(Qt::blue);
    main.addGeoJSONObservationTrack(project_home+"/data/d2.geojson", "D2")->setColor(Qt::green);
    main.addGeoJSONObservationTrack(project_home+"/data/d3.geojson", "D3")->setColor(Qt::yellow);
    main.addGeoJSONObservationTrack(project_home+"/data/d4.geojson", "D4")->setColor(Qt::magenta);
    main.addGeoJSONObservationTrack(project_home+"/data/d5.geojson", "D5")->setColor(Qt::cyan);

    main.addGeoJSONObservationTrack(project_home+"/data/s1.geojson", "S1")->setColor(Qt::blue);
    main.addGeoJSONObservationTrack(project_home+"/data/s2.geojson", "S2")->setColor(Qt::green);
    main.addGeoJSONObservationTrack(project_home+"/data/s3.geojson", "S3")->setColor(Qt::yellow);
    main.addGeoJSONObservationTrack(project_home+"/data/s4.geojson", "S4")->setColor(Qt::magenta);
    
    main.show();
    
    return app.exec();
}
