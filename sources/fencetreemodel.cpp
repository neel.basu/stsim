#include "fencetreemodel.h"
#include "tracklayer.h"
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/assign.hpp>
#include <QIcon>

QVariant FenceTreeModel::data(const QModelIndex& index, int role) const{
    return aux_type::data(index, role);
}

int FenceTreeModel::columnCount(const QModelIndex& parent) const{
    return aux_type::columnCount(parent);
}

int FenceTreeModel::rowCount(const QModelIndex& parent) const{
    return aux_type::rowCount(parent);
}

QModelIndex FenceTreeModel::parent(const QModelIndex& child) const{
    if (!child.isValid())
        return QModelIndex();

    AbstractType* childItem = static_cast<AbstractType*>(child.internalPointer());
    AbstractType* parentItem = childItem->parentItem();
    if (parentItem->depth() == -1)
        return QModelIndex();

    return createIndex(parentItem->position(), 0, parentItem);
}

QModelIndex FenceTreeModel::index(int row, int column, const QModelIndex& parent) const{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    AbstractType* parentItem;

    if (!parent.isValid()){
        parentItem = const_cast<AbstractType*>(self());
    }else{
        parentItem = static_cast<AbstractType*>(parent.internalPointer());
    }
    AbstractType* childItem = parentItem->item(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QVariant FenceTreeModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole && section < 2){
        QStringList headers;
        headers << " " << " ";
        return headers[section];
    }
    return QVariant();
}

int FenceTreeModel::ncolumns(TreeModelAUX<FenceLayer, 4, checkable_trait>::AbstractType* item) const{
    return 2;
}


QVariant FenceTreeModel::value(TreeModelAUX<FenceLayer, 3, checkable_trait>::AbstractType* item, std::size_t column, int role) const{
    if(item->depth() == -1) return QVariant();
    
    if(column == 0 && role == Qt::DecorationRole){
         if(item->depth() == 0){
            // Fence Color
            QColor color = item->actual()->color();
            QPixmap pixmap(20, 20);
            pixmap.fill(Qt::transparent);
            QPainter painter(&pixmap);
            painter.setPen(QPen(color));
            painter.setBrush(QBrush(color));
            painter.drawEllipse(0, 0, 16, 16);
            return QIcon(pixmap);
        }else if(item->depth() == 1){
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->position());
            MergedTrackLayer* mlayer = it->first;
            QColor color = mlayer->color();
            QPixmap pixmap(20, 20);
            pixmap.fill(Qt::transparent);
            QPainter painter(&pixmap);
            painter.setPen(QPen(color));
            painter.setBrush(QBrush(color));
            painter.drawEllipse(0, 0, 16, 16);
            painter.setBrush(Qt::white);
            painter.drawEllipse(4, 4, 10, 10);
            return QIcon(pixmap);
        }else if(item->depth() == 2){
            // Intersection Type (Entry, Exit)
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->parentItem()->position());
            FenceLayer::intersection x = it->second.at(item->position());
            QColor intersection_color = flayer->color();
            intersection_color.setRedF(1.0-intersection_color.redF());
            intersection_color.setBlueF(1.0-intersection_color.blueF());
            intersection_color.setGreenF(1.0-intersection_color.greenF());
            QPixmap pixmap(20, 20);
            pixmap.fill(Qt::transparent);
            QPainter painter(&pixmap);
            painter.setPen(QPen(intersection_color.darker()));
            painter.setBrush(QBrush(intersection_color.darker()));
            painter.drawRect(0, 0, 16, 16);
            if(x._entry){
                painter.setBrush(QBrush(Qt::white));
                painter.drawRect(2, 2, 13, 13);
            }
            return QIcon(pixmap);
        }else if(item->depth() == 3){
            // Time Icon
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->parentItem()->parentItem()->position());
            FenceLayer::intersection x = it->second.at(item->parentItem()->position());
            size_t position = item->position();
            QPixmap pixmap;
            if(position < x._point.times.size()){
                pixmap = QPixmap(":/pix/clock.png");
            }else{
                pixmap = QPixmap(":/pix/future-clock.png");
            }
            return QIcon(pixmap);
        }else if(item->depth() == 4){
            QPixmap pixmap;
            if(item->position() == 0){
                pixmap = QPixmap(":/pix/min.png");
            }else if(item->position() == 1){
                pixmap = QPixmap(":/pix/avg.png");
            }else if(item->position() == 2){
                pixmap = QPixmap(":/pix/max.png");
            }
            return QIcon(pixmap);
        }
    }
    
    if(role == Qt::DisplayRole){
         if(item->depth() == 0 && column == 0){
            // Fence Center, Radius
            return QString("(%1, %2)").arg(item->actual()->center().lat).arg(item->actual()->center().lng);
        }else if(item->depth() == 0 && column == 1){
            return QString("%1 m").arg(item->actual()->radius()*1000);
        }else if(item->depth() == 1 && column == 0){
            // Track Name, Number of Intersection Points
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->position());
            MergedTrackLayer* mlayer = it->first;
            return QString("%1(%2)").arg(mlayer->name().c_str()).arg(mlayer->count());
        }else if(item->depth() == 1 && column == 1){
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->position());
            return QString("X %1").arg(it->second.size());
        }else if(item->depth() == 2){
            // latitude longitude
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->parentItem()->position());
            FenceLayer::intersection x = it->second.at(item->position());
            if(column == 0){
                return QString("%1, %2").arg(x._point.location.lat).arg(x._point.location.lng);
            }else if(column == 1){
                return QString("[%1 ... %2]").arg(x._observation_start).arg(x._observation_end);
            }
        }else if(item->depth() == 3){
            // Interpolated time
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->parentItem()->parentItem()->position());
            FenceLayer::intersection x = it->second.at(item->parentItem()->position());
            size_t position = item->position();
            if(position < x._point.times.size() && column == 0){
                boost::posix_time::ptime time = x._point.times.at(position);
                try{
                    std::string time_str = boost::posix_time::to_simple_string(time);
                    return QDateTime::fromString(QString::fromStdString(time_str), "yyyy-MMM-d HH:mm:ss");
                }catch(...){
                    return std::string("----").c_str();
                }
            }else if(position < x._point.times.size() && column == 1){
                return QVariant();
                // return QString("%1").arg(position);
            }else{
                position = position - x._point.times.size();
                auto it = x._start_times_map.cbegin();
                std::advance(it, position);
                if(column == 1){
                    return QString("%1").arg(it->first);
                }else if(column == 0){
                    try{
                        std::string time_str = boost::posix_time::to_simple_string(it->second);
                        return QDateTime::fromString(QString::fromStdString(time_str), "yyyy-MMM-d HH:mm:ss");
                    }catch(...){
                        return std::string("----").c_str();
                    }
                }
            }
        }else if(item->depth() == 4){
            FenceLayer* flayer = item->actual();
            auto it = flayer->begin();
            std::advance(it, item->parentItem()->parentItem()->parentItem()->position());
            FenceLayer::intersection x = it->second.at(item->parentItem()->parentItem()->position());
            size_t parent_position = item->parentItem()->position() - x._point.times.size();

            auto sit = x._start_times_map.cbegin();
            std::advance(sit, parent_position);
            
            if(item->position() == 0){
                auto it = x._delta_distribution_map.cbegin();
                std::advance(it, parent_position);
                try{
                    boost::posix_time::time_duration duration = boost::get<0>(it->second);
                    duration = boost::posix_time::seconds(duration.total_seconds());
                    if(column == 0){
                        return QDateTime::fromString(QString::fromStdString(boost::posix_time::to_simple_string(sit->second + duration)), "yyyy-MMM-d HH:mm:ss");
                    }else if(column == 1){
                        return QString::fromStdString(boost::posix_time::to_simple_string(duration));
                    }
                }catch(...){
                    return std::string("----").c_str();
                }
            }else if(item->position() == 1){
                auto it = x._delta_distribution_map.cbegin();
                std::advance(it, parent_position);
                try{
                    boost::posix_time::time_duration duration = boost::get<1>(it->second);
                    duration = boost::posix_time::seconds(duration.total_seconds());
                    if(column == 0){
                        return QDateTime::fromString(QString::fromStdString(boost::posix_time::to_simple_string(sit->second + duration)), "yyyy-MMM-d HH:mm:ss");
                    }else if(column == 1){
                        return QString::fromStdString(boost::posix_time::to_simple_string(duration));
                    }
                }catch(...){
                    return std::string("----").c_str();
                }
            }else if(item->position() == 2){
                auto it = x._delta_distribution_map.cbegin();
                std::advance(it, parent_position);
                try{
                    boost::posix_time::time_duration duration = boost::get<2>(it->second);
                    duration = boost::posix_time::seconds(duration.total_seconds());
                    if(column == 0){
                        return QDateTime::fromString(QString::fromStdString(boost::posix_time::to_simple_string(sit->second + duration)), "yyyy-MMM-d HH:mm:ss");
                    }else if(column == 1){
                        return QString::fromStdString(boost::posix_time::to_simple_string(duration));
                    }
                }catch(...){
                    return std::string("----").c_str();
                }
            }
        }
    }
    
    return QVariant();
}

QModelIndex FenceTreeModel::populate(TreeModelAUX<FenceLayer, 3, checkable_trait>::data_ptr_type data){
    auto rc = aux_type::rowCount();
    beginInsertRows(QModelIndex(), rc, rc);
    auto child_fence = create(data);
    for(int i = 0; i != data->tracks_intersected(); ++i){ // iterate through tracks
        auto child_track = child_fence->create(data);
        for(int j = 0; j != data->track_intersections(i); ++j){ // iterate through intersections of track i
            auto child_intersection = child_track->create(data); // show parent scope
            for(int k = 0; k != data->track_intersection_times(i, j); ++k){ // iterate through times of intersection j of track i
                child_intersection->create(data);
            }
            stmr::intersection x = data->track_intersection(i, j);
            std::vector<unsigned> future_observations;
            boost::copy(x._start_times_map | boost::adaptors::map_keys, std::back_inserter(future_observations));
            for(unsigned n : future_observations){
                auto child_observation = child_intersection->create(data);
                for(unsigned i = 0; i < 3; ++i){
                    child_observation->create(data);
                }
            }
        }
    }
    endInsertRows();
    return createIndex(rc, rc, child_fence);
}
