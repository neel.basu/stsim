/*
 * Copyright 2019 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEMPORAL_H
#define TEMPORAL_H

#include <vector>
#include <utility>
#include <numeric>
#include <iterator>
#include <type_traits>
#include <boost/bind.hpp>
#include <boost/date_time.hpp>
#include <boost/range/irange.hpp>

namespace stmr{
namespace temporal{
    
namespace traits{
    template <typename T>
    struct is_valid{
        static bool test(const T& v){return false;}
    };

    template <typename T>
    struct is_invalid{
        static bool test(const T& v){
            return !is_valid<T>::test(v);
        }
    };

    template <>
    struct is_valid<boost::posix_time::ptime>{
        static bool test(const boost::posix_time::ptime& v){
            return !v.is_not_a_date_time();
        }
    };

    template <>
    struct is_valid<boost::posix_time::time_duration>{
        static bool test(const boost::posix_time::time_duration& v){
            return !v.is_not_a_date_time();
        }
    };
    template <typename T>
    struct squared{
        static double value(const T& v){
            return v*v;
        }
    };
    
    template <>
    struct squared<boost::posix_time::time_duration>{
        static double value(const boost::posix_time::time_duration& v){
            return v.total_seconds() * v.total_seconds();
        }
    };
}

template <typename InputIterator, typename OutputIterator>
void forward_diff(InputIterator begin, InputIterator end, OutputIterator out){
    typedef decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) difference_type;
    for(auto it = begin+1; it != end; ++it){
        difference_type diff = *it - *(it-1);;
        out++ = diff;
    }
}
template <typename InputIterator>
std::vector<decltype(typename InputIterator::value_type() - typename InputIterator::value_type())> forward_diff(InputIterator begin, InputIterator end){
    std::vector<decltype(typename InputIterator::value_type() - typename InputIterator::value_type())> result;
    forward_diff(begin, end, std::back_inserter(result));
    return result;
}

template <typename InputIterator>
unsigned mean_squared_error(InputIterator begin, InputIterator end, unsigned period){
    typedef decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) difference_type;
    
    std::vector<difference_type> D = forward_diff(begin, end);
    unsigned size = std::distance(begin, end);
    if(period > size/2) return std::numeric_limits<double>::infinity();
    
    double se = 0.0;
    for(auto it = D.begin()+period; it != D.end(); ++it){
        se += traits::squared<difference_type>::value(*it - *(it-period));
    }
    double count = size-period;
    return se/count;
}

template <typename InputIterator>
unsigned periodicity(InputIterator begin, InputIterator end){
    unsigned size = std::distance(begin, end);
    double min_mse = std::numeric_limits<double>::max();
    unsigned periodicity = 0;
    for(unsigned i = 1; i <= size/2; ++i){
        double cmse = mean_squared_error(begin, end, i);
        if(cmse < min_mse){
            periodicity = i;
            min_mse = cmse;
        }
    }
    return periodicity;
}

template <typename InputIterator>
decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) wave_length(InputIterator begin, InputIterator end, unsigned period = 0){
    typedef decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) difference_type;
    std::vector<difference_type> diffs = forward_diff(begin, end);
    std::vector<difference_type> distribution;
    for(auto it = diffs.begin(); it != diffs.begin()+std::distance(diffs.begin(), diffs.begin())-period; ++it){
        bool all_valid = std::all_of(it, it+period, boost::bind(&traits::is_valid<difference_type>::test, _1));
        if(all_valid){
            difference_type wave_size = std::accumulate(it, it+period, difference_type());
            distribution.push_back(wave_size);
        }
    }
    return std::accumulate(distribution.begin(), distribution.end(), difference_type())/distribution.size();
}

template <typename InputIterator>
typename std::iterator_traits<InputIterator>::value_type extrapolate_time(InputIterator begin, InputIterator end, unsigned n, unsigned periodicity, unsigned long period_size){
    typedef decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) difference_type;
    
    unsigned ncycles = std::floor((n - 1)/periodicity);
    unsigned residue = (n - 1) % periodicity;
    
    std::vector<difference_type> D = forward_diff(begin, end);
    difference_type sum = (*begin -*begin); // set sum to zero equivalent
    for(auto it = D.cbegin(); it != D.begin()+residue; ++it){
        sum += *it;
    }
    
    return *begin + (ncycles * period_size + sum);
}

template <typename InputIterator>
unsigned extrapolate_instance(InputIterator begin, InputIterator end, const typename std::iterator_traits<InputIterator>::value_type& time, unsigned periodicity, decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) period_size){
    typedef decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) difference_type;
    
    unsigned ncycles = std::floor((time - *begin)/period_size);
    difference_type residue = (time - *begin) % period_size;
    
    std::vector<difference_type> D = forward_diff(begin, end);
    unsigned tau = 0;
    difference_type sum = 0;
    for(auto it = D.cbegin(); it != D.cend(); ++it){
        sum += *it;
        if(sum > residue){
            tau = std::distance(D.cbegin(), it);
            break;
        }
    }
    return ncycles * periodicity + tau +1;
}

template <typename InputIterator>
std::pair<unsigned, unsigned> bounding_instances(InputIterator begin, InputIterator end, const typename std::iterator_traits<InputIterator>::value_type& start, const typename std::iterator_traits<InputIterator>::value_type& finish, unsigned periodicity, decltype(typename InputIterator::value_type() - typename InputIterator::value_type()) period_size){
    unsigned eta_s = extrapolate_instance(begin, end, start, periodicity, period_size);
    unsigned eta_e = extrapolate_instance(begin, end, finish, periodicity, period_size) +1;
    return std::make_pair(eta_s, eta_e);
}

std::vector<unsigned> related_instances(unsigned n, unsigned periodicity, unsigned N){
    std::vector<unsigned> observations;
    auto range = boost::irange((unsigned)0, N);
    std::transform(range.begin(), range.end(), std::back_inserter(observations), [periodicity, n](unsigned i){
        return (i * periodicity) + ((n-1) % periodicity) +1;
    });
    return observations;
}



}

#endif // TEMPORAL_H
