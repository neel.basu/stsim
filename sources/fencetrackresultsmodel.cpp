#include "fencetrackresultsmodel.h"
#include "stmr.h"
#include "fencelayer.h"
#include "tracklayer.h"
#include <boost/date_time.hpp>
#include <QDateTime>

FenceTrackResultsModel::FenceTrackResultsModel(FenceLayer* fence, QObject *parent): QAbstractTableModel(parent), _fence(fence), _track(0x0){
    
}

QVariant FenceTrackResultsModel::headerData(int section, Qt::Orientation orientation, int role) const{
    QStringList headers;
    headers << "start" << "end" << "favourability";
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole){
        return headers.at(section);
    }
    return QVariant();
}

int FenceTrackResultsModel::rowCount(const QModelIndex &parent) const{
    if (!_fence || !_track || parent.isValid())
        return 0;

    const stmr::time_interval_map_type& results = _fence->results(_track);
    return results.iterative_size();
}

int FenceTrackResultsModel::columnCount(const QModelIndex &parent) const{
    if (!_fence || !_track || parent.isValid())
        return 0;

    return 3;
}

QVariant FenceTrackResultsModel::data(const QModelIndex& index, int role) const{
    if (!_fence || !_track || !index.isValid())
        return QVariant();

    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    if(role == Qt::DisplayRole){
        const stmr::time_interval_map_type& results = _fence->results(_track);
        stmr::time_interval_map_type::const_iterator it = results.begin();
        std::advance(it, index.row());
        if(index.column() == 0){
            boost::posix_time::ptime lower = it->first.lower();
            unsigned long seconds = (lower - epoch).total_seconds();
            std::string time_start_str = boost::posix_time::to_simple_string(epoch + boost::posix_time::seconds(seconds));
            QDateTime time = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
            return time;
        }else if(index.column() == 1){
            boost::posix_time::ptime upper = it->first.upper();
            unsigned long seconds = (upper - epoch).total_seconds();
            std::string time_start_str = boost::posix_time::to_simple_string(epoch + boost::posix_time::seconds(seconds));
            QDateTime time = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
            return time;
        }else if(index.column() == 2){
            return QString("%1").arg(it->second);
        }
    }
    
    return QVariant();
}

void FenceTrackResultsModel::setTrack(MergedTrackLayer* track){
    beginResetModel();
    _track = track;
    endResetModel();
}

MergedTrackLayer * FenceTrackResultsModel::track() const{
    return _track;
}

void FenceTrackResultsModel::setFence(FenceLayer* fence){
    beginResetModel();
    _fence = fence;
    endResetModel();
}

FenceLayer * FenceTrackResultsModel::fence() const{
    return _fence;
}

QDateTime FenceTrackResultsModel::min() const{
    const stmr::time_interval_map_type& results = _fence->results(_track);
    stmr::time_interval_map_type::const_iterator it = results.begin();
    boost::posix_time::ptime lower = it->first.lower();
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    unsigned long seconds = (lower - epoch).total_seconds();
    std::string time_start_str = boost::posix_time::to_simple_string(epoch + boost::posix_time::seconds(seconds));
    QDateTime time = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
    return time;
}

QDateTime FenceTrackResultsModel::max() const{
    const stmr::time_interval_map_type& results = _fence->results(_track);
    stmr::time_interval_map_type::const_iterator it = results.begin();
    std::advance(it, results.size()-1);
    boost::posix_time::ptime lower = it->first.upper();
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    unsigned long seconds = (lower - epoch).total_seconds();
    std::string time_start_str = boost::posix_time::to_simple_string(epoch + boost::posix_time::seconds(seconds));
    QDateTime time = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
    return time;
}

QDateTime FenceTrackResultsModel::lower(size_t i) const{
    const stmr::time_interval_map_type& results = _fence->results(_track);
    stmr::time_interval_map_type::const_iterator it = results.begin();
    std::advance(it, i);
    boost::posix_time::ptime lower = it->first.lower();
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    unsigned long seconds = (lower - epoch).total_seconds();
    std::string time_start_str = boost::posix_time::to_simple_string(epoch + boost::posix_time::seconds(seconds));
    QDateTime time = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
    return time;
}

QDateTime FenceTrackResultsModel::upper(size_t i) const{
    const stmr::time_interval_map_type& results = _fence->results(_track);
    stmr::time_interval_map_type::const_iterator it = results.begin();
    std::advance(it, i);
    boost::posix_time::ptime lower = it->first.upper();
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    unsigned long seconds = (lower - epoch).total_seconds();
    std::string time_start_str = boost::posix_time::to_simple_string(epoch + boost::posix_time::seconds(seconds));
    QDateTime time = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
    return time;
}

double FenceTrackResultsModel::probability(size_t i) const{
    const stmr::time_interval_map_type& results = _fence->results(_track);
    stmr::time_interval_map_type::const_iterator it = results.begin();
    std::advance(it, i);
    return it->second;
}

