#ifndef STSIM_H
#define STSIM_H

#include <vector>
#include <QEvent>
#include <QPushButton>
#include <QAction>
#include <QItemSelection>
#include <QMainWindow>
#include <QScopedPointer>
#include <marble/MarbleWidget.h>
#include <marble/LatLonEdit.h>
#include <marble/LatLonBoxWidget.h>
#include <boost/filesystem.hpp>
#include "tracklayer.h"
#include "fencelayer.h"
#include "tracktreemodel.h"
#include "mergedtrackhullmodel.h"
#include "fencetreemodel.h"
#include "fenceintersectiondetailtablemodel.h"
#include "fencetrackresultsmodel.h"
#include "timelinegraphicsview.h"

namespace Ui {
class STSimulator;
}

class STSimulator : public QMainWindow{
  Q_OBJECT
  
  enum EditMode{
    NoEdit,
    TrackInitial,
    TrackExtension,
    TrackPointReposition,
    FenceCenter,
    FencePerimeter
  };
  
  Marble::MarbleWidget*   _map;
  Marble::LatLonEdit*     _latEdit;
  Marble::LatLonEdit*     _lonEdit;
  Marble::LatLonEdit*     _first_latEdit;
  Marble::LatLonEdit*     _first_lonEdit;
  ObservationTrackLayer*  _editedTrack;
  QPushButton*            _tracksButton;
  QPushButton*            _resultsButton;
  TimelineGraphicsView*   _timelineView;
  
  TrackTreeModel*                    _model;
  MergedTrackHullModel*              _hullModel;
  FenceTreeModel*                    _fenceModel;
  FenceTrackResultsModel*            _resultsModel;
  FenceIntersectionDetailTableModel* _fenceDetailModel;
  
  std::vector<FenceLayer*> _fences;
  public:
    explicit STSimulator(QWidget *parent = nullptr);
    virtual ~STSimulator() override;
    Marble::MarbleWidget* map() const;
  private:
    QScopedPointer<Ui::STSimulator> _ui;
  public:
    void addTrack(AbstractTrackLayer* track);
    void removeTrack(AbstractTrackLayer* track);
    AbstractTrackLayer* addGeoJSONObservationTrack(const boost::filesystem::path& path, const std::string& name);
    AbstractTrackLayer* addGeoJSONMergedTrack(const boost::filesystem::path& path, const std::string& name);
  private:
    void prepareInsertTab(const stmr::observation_data& previous, const stmr::gps_coordinate& location, const stmr::observation_data& next);
  public slots:
    void addTrackSlot();
    void addMergedTrackSlot();
    void mergeSlot();
    /**
     * extend an existing track
     */
    void extendTrackSlot();
    void locationClickedSlot(ObservationTrackLayer* layer, qreal lat, qreal lon);
    /**
     * compose a new track
     */
    void composeTrackSlot();
    void locationSelectedSlot();
    void locationRejectedSlot();
    void updateInsertTabSlot(const QDateTime& dt);
    void updateVelocity();
  public slots:
    void changeColorSlot();
    void changeNameSlot();
    void simplifySlot();
    void simplifyRDPSlot();
    void saveSelectedSlot();
    void selectionChangedSlot(const QItemSelection& selected, const QItemSelection& deselected);
  public slots:
    void fenceSealed(FenceLayer* fence);
  public:
    virtual bool event(QEvent* event) override;
};

#endif // STSIM_H
