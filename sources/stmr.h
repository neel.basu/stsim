#ifndef STMR_LIB_H
#define STMR_LIB_H

#include <boost/geometry.hpp>
#include <boost/filesystem.hpp>
#include <boost/function.hpp>
#include <boost/date_time.hpp>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <boost/icl/split_interval_map.hpp>
#include <boost/geometry/geometries/register/point.hpp>

#define earthRadiusKm 6371.0
#define VERT_TH       00.004//Threshold for detecting vertical skew
// #define degToRad (std::acos(-1.0f) / 180.0f);

namespace stmr{
    
typedef boost::icl::interval<boost::posix_time::time_duration>::type delta_interval_type;
typedef boost::icl::interval<boost::posix_time::ptime>::type time_interval_type;
typedef boost::icl::split_interval_map<boost::posix_time::time_duration, double> delta_interval_map_type;
typedef boost::icl::split_interval_map<boost::posix_time::ptime, double> time_interval_map_type;

namespace internal{
    double deg2rad(double deg);

    double rad2deg(double rad);

    double unit_distance(double lat1d, double lon1d, double lat2d, double lon2d);

    double distanceEarth(double lat1d, double lon1d, double lat2d, double lon2d);
}

struct gps_coordinate{
    double lat;
    double lng;
    
    gps_coordinate(): lat(0.0f), lng(0.0f){}
    gps_coordinate(double lt, double ln): lat(lt), lng(ln){}
    bool invalid() const{return lat == 0.0f && lng == 0.0f;}
    bool operator==(const gps_coordinate& other) const {return (lat == other.lat && lng == other.lng);}
    bool operator!=(const gps_coordinate& other) const {return !operator==(other);}
    
    double get_lat() const{return lat;}
    double get_lng() const{return lng;}
    void set_lat(double v){lat = v;}
    void set_lng(double v){lng = v;}
};

namespace detail{
    
    /**
     * Small minimal vector class for epherical geometry related computation
     **/
    template <typename T>
    struct geovector{
        typedef T value_type;
        value_type _x, _y, _z;
        
        geovector(): _x(0.0), _y(0.0), _z(0.0){}
        geovector(const geovector<T>& other): _x(other._x), _y(other._y), _z(other._z){}
        geovector(const gps_coordinate& coordinate): _x(0.0), _y(0.0), _z(0.0){
            _x = std::cos(internal::deg2rad(coordinate.lat)) * std::cos(internal::deg2rad(coordinate.lng));
            _y = std::cos(internal::deg2rad(coordinate.lat)) * std::sin(internal::deg2rad(coordinate.lng));
            _z = std::sin(internal::deg2rad(coordinate.lat));
        }
        geovector(value_type x, value_type y, value_type z): _x(x), _y(y), _z(z){}
        value_type magnitude() const{
            return std::sqrt(_x*_x + _y*_y + _z*_z);
        }
        value_type x() const{
            return _x;
        }
        value_type y() const{
            return _y;
        }
        value_type z() const{
            return _z;
        }
        geovector<T> normalized() const{
            return *this / magnitude();
        }
    };
    

    template <typename T>
    T dot(const geovector<T>& A, const geovector<T>& B){
        return A.x() * B.x() + A.y() * B.y() + A.z() * B.z();
    }
    
    template <typename T>
    geovector<T> cross(const geovector<T>& A, const geovector<T>& B){
            return geovector<T>(A.y() * B.z() - B.y() * A.z(),
                                A.z() * B.x() - B.z() * A.x(),
                                A.x() * B.y() - B.x() * A.y()
            );
    }
    
    template <typename T>
    geovector<T> operator-(const geovector<T>& A){
        return -1*A;
    }
    
    template <typename T>
    geovector<T> operator+(const geovector<T>& A, const geovector<T>& B){
        geovector<T> C;
        C._x = B.x() + A.x();
        C._y = B.y() + A.y();
        C._z = B.z() + A.z();
        return C;
    }
    
    template <typename T>
    geovector<T> operator-(const geovector<T>& A, const geovector<T>& B){
        return operator+(A + -B);
    }
    
    template <typename T>
    geovector<T> operator+(const geovector<T>& A, const T& u){
        geovector<T> B = A;
        B._x = A.x() + u;
        B._y = A.y() + u;
        B._z = A.z() + u;
        return B;
    }

    template <typename T>
    geovector<T> operator+(const T& u, const geovector<T>& A){
        return operator+(A, u);
    }
    
    template <typename T>
    geovector<T> operator*(const geovector<T>& A, const T& u){
        geovector<T> B = A;
        B._x = A.x() * u;
        B._y = A.y() * u;
        B._z = A.z() * u;
        return B;
    }

    template <typename T>
    geovector<T> operator*(const T& u, const geovector<T>& A){
        return operator*(A, u);
    }
    
    template <typename T>
    geovector<T> operator/(const geovector<T>& A, const T& u){
        return operator*(A, 1.0/u);
    }
    
    template <typename T>
    T geodistance(const geovector<T>& A, const geovector<T>& B){
        geovector<T> An = A.normalized();
        geovector<T> Bn = B.normalized();
//         geovector<T> C  = detail::cross(An, Bn);
//         double d1 = std::abs(earthRadiusKm * std::atan2(C.magnitude() ,  detail::dot(An, Bn)));
//         double d2 = std::abs(earthRadiusKm * std::atan2(C.magnitude() , -detail::dot(An, Bn)));
//         return std::min(std::abs(d1), std::abs(d2));
        double d1 = std::abs(std::acos( detail::dot(An, Bn)));
        double d2 = std::abs(std::acos(-detail::dot(An, Bn)));
        double a  = std::min(d1, d2);
        return a*earthRadiusKm;
    }

}


double unit_distance(const gps_coordinate& l, const gps_coordinate& r);
double distance(const gps_coordinate& l, const gps_coordinate& r);

struct merged_data;

struct observation_data{
    gps_coordinate location;
    boost::posix_time::ptime time;
    std::size_t index;
    std::string label;
    
    observation_data(){}
    observation_data(const observation_data& other): location(other.location), time(other.time), index(other.index), label(other.label){}
    merged_data to_merged(int K = 1) const;
    bool invalid() const{return location.invalid();}
};

double unit_distance(const observation_data& l, const observation_data& r);
double distance(const observation_data& l, const observation_data& r);

struct merged_data{
    gps_coordinate location;
    std::vector<boost::posix_time::ptime> times;
    std::string label;
    std::size_t index;
    
    merged_data(){}
    merged_data(const merged_data& other): location(other.location), times(other.times), label(other.label), index(other.index){}
    void reshape(int K);
    bool invalid() const{return location.invalid();}
    
    double get_lat() const{return location.lat;}
    double get_lng() const{return location.lng;}
    void set_lat(double lat){location.lat = lat;}
    void set_lng(double lng){location.lng = lng;}
};

double unit_distance(const merged_data& l, const merged_data& r);
double distance(const merged_data& l, const merged_data& r);

enum triangle_state{
    state_null = 0, state_a, state_b, state_c, state_any
};
enum skewness{
    skew_null, skew_p, skew_q, skew_v
};

struct state_triangle{
    merged_data p;
    merged_data q;
    merged_data t;

    state_triangle(){}
    state_triangle(const merged_data& p_, const merged_data& q_, const merged_data& t_): p(p_), q(q_), t(t_){}
    void set(const merged_data& p_, const merged_data& q_, const merged_data& t_);
    triangle_state state() const;
    bool invalid() const{
        return p.location.invalid() || q.location.invalid() || t.location.invalid();
    }
    double perpd() const;
    double vdistance() const;
    double cos_tpq_spherical() const;
    double cos_tqp_spherical() const;
    bool opposite(const state_triangle& other) const;
    bool is_vskew() const;
    skewness skew(const state_triangle& other) const;
    
    double unit_pt() const{
        return unit_distance(p.location, t.location);
    }
    double unit_qt() const{
        return unit_distance(q.location, t.location);
    }
    double unit_pq() const{
        return unit_distance(p.location, q.location);
    }
    double cos_pt() const{
        return std::cos(unit_pt());
    }
    double cos_qt() const{
        return std::cos(unit_qt());
    }
    double cos_pq() const{
        return std::cos(unit_pq());
    }
    double sin_pt() const{
        return std::sin(unit_pt());
    }
    double sin_qt() const{
        return std::sin(unit_qt());
    }
    double sin_pq() const{
        return std::sin(unit_pq());
    }
    double pt() const{
        return unit_pt() * earthRadiusKm;
    }
    double qt() const{
        return unit_qt() * earthRadiusKm;
    }
    double pq() const{
        return unit_pq() * earthRadiusKm;
    }
    
    double projected_unit_distance() const;
};

struct local_view{
    state_triangle lookback;
    state_triangle previous;
    state_triangle current;
    
    triangle_state cstate() const;
    triangle_state pstate() const;
    triangle_state lstate() const;
    
    /**
     * matches previous and lookback state against the given states
     */
    bool match_state(triangle_state lstate, triangle_state pstate);
    /**
     * steps the view ahead. checks whether current state is same as the previous state
     * if not assigns previous to lookback. assigns current to previous unconditionally.
     */
    void step();
    /**
     * steps and then assigns the input triangle to the current
     */
    void step(const state_triangle& c);
};

struct intersection{
    typedef std::map<unsigned, std::vector<unsigned>> related_observations_type;
    typedef std::map<unsigned, boost::posix_time::ptime> observation_start_time_map_type;
    typedef boost::tuple<boost::posix_time::time_duration, boost::posix_time::time_duration, boost::posix_time::time_duration> delta_distribution_type;
    typedef std::vector<boost::posix_time::time_duration> delta_list_type;
    typedef std::map<unsigned, delta_distribution_type>  delta_distribution_map_type;
    typedef std::map<unsigned, delta_list_type> delta_map_type;
    typedef std::vector<boost::posix_time::ptime> time_list_type;
    
    stmr::merged_data        _point;
    bool                     _entry;
    unsigned int             _periodicity;
    unsigned long            _period_size;
    boost::posix_time::ptime _request_start;
    boost::posix_time::ptime _request_end;
    unsigned                 _observation_start;
    unsigned                 _observation_end;
    boost::posix_time::ptime _service_start;
    boost::posix_time::ptime _service_end;
    time_list_type           _observed_start_times;
    
    related_observations_type       _related_observations;
    delta_map_type                  _delta_map;
    observation_start_time_map_type _start_times_map;
    delta_distribution_map_type     _delta_distribution_map;
    
    intersection(){}
    intersection(const stmr::merged_data& m, const boost::posix_time::ptime& start, const boost::posix_time::ptime& end, bool e): _point(m), _entry(e), _periodicity(0), _period_size(0), _request_start(start), _request_end(end), _observation_start(0), _observation_end(0){}
    intersection(const intersection& other): 
            _point(other._point), _entry(other._entry), _periodicity(other._periodicity), _period_size(other._period_size), 
            _request_start(other._request_start), _request_end(other._request_end), 
            _observation_start(other._observation_start), _observation_end(other._observation_end), 
            _service_start(other._service_start), _service_end(other._service_end), 
            _start_times_map(other._start_times_map), _delta_distribution_map(other._delta_distribution_map), 
            _delta_map(other._delta_map), _related_observations(other._related_observations), _observed_start_times(other._observed_start_times){}
};

std::ostream& operator<<(std::ostream& os, const stmr::gps_coordinate& obj);
std::ostream& operator<<(std::ostream& os, const stmr::observation_data& obj);
std::ostream& operator<<(std::ostream& os, const stmr::merged_data& obj);
std::ostream& operator<<(std::ostream& os, const stmr::state_triangle& obj);
std::ostream& operator<<(std::ostream& os, const stmr::local_view& obj);

std::vector<observation_data> parse_csv(const boost::filesystem::path& path, const std::string& label);
std::vector<observation_data> parse_json(const boost::filesystem::path& path, const std::string& label);
std::vector<merged_data> parse_json_merged(const boost::filesystem::path& path, const std::string& label);

std::string save_csv(const std::vector<merged_data>& merged);
std::string save_json(const std::vector<merged_data>& merged);
std::string save_json(const std::vector<observation_data>& observation);
void save_json(const std::vector<merged_data>& merged, const boost::filesystem::path& path);
void save_json(const std::vector<observation_data>& observation, const boost::filesystem::path& path);
void save_csv(const std::vector<merged_data>& merged, const boost::filesystem::path& path);

namespace algorithms{
    typedef std::vector<boost::posix_time::ptime> time_list;
    typedef boost::function<void (const stmr::local_view&, const stmr::merged_data&, stmr::skewness skew)> merge_callback_type;
    void noop(const stmr::local_view&, const stmr::merged_data&, stmr::skewness skew);
    
    void approximate_n(merged_data& t, const merged_data& p, const merged_data& q);
    void approximate_m(merged_data& m, const merged_data& s, const merged_data& t);
    std::vector<merged_data> merge(const std::vector<observation_data>& observation, int K=1);
    std::vector<merged_data> merge(const std::vector<merged_data>& merged, const std::vector<observation_data>& observation, int K, merge_callback_type callback=&noop);
    std::vector<merged_data> simplify(const std::vector<merged_data>& complicated, double th);
    std::vector<merged_data> simplify_rdp(const std::vector<merged_data>& complicated, double th);
    std::vector<merged_data> convex_hull(const std::vector<merged_data>& points);
    std::pair<bool, bool> intersection(const gps_coordinate& center, double radius, const gps_coordinate& p, const gps_coordinate& q, gps_coordinate& x, gps_coordinate& y);
    std::vector<merged_data> intersections(const gps_coordinate& center, double radius, const std::vector<merged_data>& merged, std::vector<bool>& states);
    
    /**
     * calculate first order forward difference difference table for the time sequence defined by [begin, end) and return list of time differences in seconds
     */
    std::vector<unsigned long> forward_difference(time_list::const_iterator begin, time_list::const_iterator end);
    /**
     * calculate mean square error of the time sequence defined by [begin, end) using a sliding window of size period, while assuming the sequence is at least twice large than period
     */
    double mse(time_list::const_iterator begin, time_list::const_iterator end, unsigned period);
    /**
     * calculates the periodicity of the sequence defined by [begin, end)
     */
    unsigned periodicity(time_list::const_iterator begin, time_list::const_iterator end);
    /**
     * calculate average size of a period of the sequence defined by [begin, end) using the given periodicity. if periodicity is 0 then periodicity is calculated using the periodicity function
     */
    unsigned long period_size(time_list::const_iterator begin, time_list::const_iterator end, unsigned period = 0);
    /**
     * calculate the expected time of index n of the time sequence [begin, end)
     */
    boost::posix_time::ptime extrapolate_time(time_list::const_iterator begin, time_list::const_iterator end, unsigned n, unsigned periodicity=0, unsigned long period_size=0);
    /**
     * calculated the expected index on which the sequence [begin, end) is supposed to have a time stamp just less than the time given
     */
    unsigned extrapolate_instance(time_list::const_iterator begin, time_list::const_iterator end, const boost::posix_time::ptime& time, unsigned periodicity=0, unsigned long period_size=0);
    /**
     * 
     */
    std::pair<unsigned, unsigned> bounding_instances(time_list::const_iterator begin, time_list::const_iterator end, const boost::posix_time::ptime& start, const boost::posix_time::ptime& finish, unsigned periodicity=0, unsigned long period_size=0);
    /**
     * instances in [1 ... N] equivalent to observation n given the periodicity
     */
    std::vector<unsigned> related_instances(unsigned n, unsigned periodicity, unsigned N);
    
    std::vector<boost::posix_time::time_duration> delta_distribution_to_vector(const stmr::intersection::delta_distribution_type& distribution);
    
    /**
     * gathers observation start times of different observations from a merged route
     */
    std::vector<boost::posix_time::ptime> observation_start_times(const std::vector<stmr::merged_data>& merged);
    std::vector<unsigned> instances_within_fence(const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, const std::vector<boost::posix_time::time_duration>& delta, const std::vector<boost::posix_time::time_duration>& delta_next, const std::vector<unsigned>& observations);
    void calculate_temporal_detail(stmr::intersection& x);
    
    std::vector<boost::posix_time::time_duration> delta_distribution_to_vector(const stmr::intersection& x, unsigned n);
    std::vector<boost::posix_time::time_duration> piecewise_delta_distribution(const stmr::intersection& x, const stmr::intersection& y, unsigned n);
    std::vector<boost::posix_time::time_duration> combined_delta_distribution(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, unsigned n);
    std::pair<unsigned, unsigned> combined_bounding_instances(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, const boost::posix_time::ptime& start, const boost::posix_time::ptime& finish);
    /**
     * Assuming x is an entry point and y is its corresponding exist point calculates favourability of stay the resource during delay [a, b] from resource journey start time on observation j
     * value of feasibility is [0...1] depending on the duration of stay. If the resource stays (b-a) duration within x -> y then returns 1, scales down otherwise
     */
    double favourability(const stmr::intersection& x, const stmr::intersection& y, const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, unsigned j);
    /**
     * Calculate the probability of being inside transit [x -> y] within fence on observation n within delay [a, b] based on favourabilities of past observations related to observation number n
     */
    double probability(const stmr::intersection& x, const stmr::intersection& y, const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, unsigned n);
    /**
     * Given a set of all intersections X defined by [begin, end) returns the possibility of being inside the fence during delay [a, b] from resource journey start time on observation n
     */
    double rank(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, unsigned n);
    
    delta_interval_map_type favourability_map_delta(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, unsigned n);
    
    time_interval_map_type favourability_map(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, const boost::posix_time::ptime& start, const boost::posix_time::ptime& finish, std::vector<boost::posix_time::ptime> start_times);
}

}

BOOST_GEOMETRY_REGISTER_POINT_2D_GET_SET(stmr::gps_coordinate, double, boost::geometry::cs::spherical_equatorial<boost::geometry::degree>, get_lng, get_lat, set_lng, set_lat)
BOOST_GEOMETRY_REGISTER_POINT_2D_GET_SET(stmr::merged_data, double, boost::geometry::cs::spherical_equatorial<boost::geometry::degree>, get_lng, get_lat, set_lng, set_lat)

#endif
