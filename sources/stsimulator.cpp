#include "stsimulator.h"
#include "ui_stsimulator.h"
#include "addtrackdialog.h"
#include "mergedialog.h"
#include "fencelayer.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QColorDialog>
#include <QInputDialog>
#include <marble/MarbleModel.h>
#include <marble/RouteRequest.h>
#include <marble/RoutingManager.h>
#include <marble/MarbleWidget.h>
#include <marble/GeoDataLatLonBox.h>
#include <marble/GeoDataLookAt.h>
#include <QDebug>

STSimulator::STSimulator(QWidget *parent): QMainWindow(parent), _ui(new Ui::STSimulator), _model(new TrackTreeModel), _map(new Marble::MarbleWidget), _hullModel(new MergedTrackHullModel), _fenceModel(new FenceTreeModel), _fenceDetailModel(new FenceIntersectionDetailTableModel){   
    _ui->setupUi(this);
    _ui->tracksDockWidget->setTitleBarWidget(new QWidget());
    
    _hullModel->setSourceModel(_model);
    _ui->hullTreeView->setModel(_hullModel);
    
    _ui->fenceTreeView->setModel(_fenceModel);
    _fenceModel->setCheckDepth(5);
    
    _ui->fenceIntersectionDetailTableView->setModel(_fenceDetailModel);
    _ui->fenceIntersectionDetailTableView->hide();
    
    _model->setCheckDepth(3);
    
    setCentralWidget(_map);
    _map->setAnimationsEnabled(true);
    _map->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");
    // _map->setMapThemeId("earth/vectorosm/vectorosm.dgml");
    _ui->treeView->setModel(_model);
    
    _latEdit = new Marble::LatLonEdit(this, Marble::LatLonEdit::Latitude,  Marble::GeoDataCoordinates::DMS);
    _lonEdit = new Marble::LatLonEdit(this, Marble::LatLonEdit::Longitude, Marble::GeoDataCoordinates::DMS);
    _first_latEdit = new Marble::LatLonEdit(this, Marble::LatLonEdit::Latitude,  Marble::GeoDataCoordinates::DMS);
    _first_lonEdit = new Marble::LatLonEdit(this, Marble::LatLonEdit::Longitude, Marble::GeoDataCoordinates::DMS);
    
    _latEdit->setDisabled(true);
    _lonEdit->setDisabled(true);
    
    qobject_cast<QBoxLayout*>(_ui->insertTab->layout())->insertWidget(0, _lonEdit);
    qobject_cast<QBoxLayout*>(_ui->insertTab->layout())->insertWidget(0, _latEdit);
    _ui->editorTabWidget->hide();
    
    qobject_cast<QBoxLayout*>(_ui->firstPointGroupBox->layout())->insertWidget(0, _first_lonEdit);
    qobject_cast<QBoxLayout*>(_ui->firstPointGroupBox->layout())->insertWidget(0, _first_latEdit);
    _ui->firstPointGroupBox->hide();
    
    connect(_ui->actionAdd_Observation, SIGNAL(triggered()), this, SLOT(addTrackSlot()));
    connect(_ui->actionAdd_Merge, SIGNAL(triggered()), this, SLOT(addMergedTrackSlot()));
    connect(_ui->action_Merge, SIGNAL(triggered()), this, SLOT(mergeSlot()));
    connect(_ui->actionAdd_Point, SIGNAL(triggered()), this, SLOT(extendTrackSlot()));
    connect(_ui->addObservationDataButtonBox, SIGNAL(accepted()), this, SLOT(locationSelectedSlot()));
    connect(_ui->addObservationDataButtonBox, SIGNAL(rejected()), this, SLOT(locationRejectedSlot()));
    connect(_ui->actionSave, SIGNAL(triggered()), this, SLOT(saveSelectedSlot()));
    connect(_ui->actionColor, SIGNAL(triggered()), this, SLOT(changeColorSlot()));
    connect(_ui->actionName, SIGNAL(triggered()), this, SLOT(changeNameSlot()));
    connect(_ui->actionSimplify_RDP, SIGNAL(triggered()), this, SLOT(simplifyRDPSlot()));
    connect(_ui->treeView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &STSimulator::selectionChangedSlot);
    connect(_ui->firstDateTimeEdit, &QTimeEdit::dateTimeChanged, this, &STSimulator::updateInsertTabSlot);

    connect(_ui->actionShow, &QAction::triggered, [this](){
        auto items = _model->selectedItems();
        for(auto item: items){
            item->actual()->setVisible(true);
        }
        _ui->treeView->update();
        _map->update();
    });
    
    connect(_ui->actionHide, &QAction::triggered, [this](){
        auto items = _model->selectedItems();
        for(auto item: items){
            item->actual()->setVisible(false);
        }
        _ui->treeView->update();
        _map->update();
    });
    
    connect(_ui->markTracksButton, &QPushButton::clicked, [this](bool checked){
       if(checked) _ui->markDataButton->setChecked(false);
       _model->setCheckDepth(checked ? 0 : 3);
       _model->reset();
    });
    connect(_ui->markDataButton, &QPushButton::clicked, [this](bool checked){
       if(checked) _ui->markTracksButton->setChecked(false);
       _model->setCheckDepth(checked ? 1 : 3);
       _model->reset();
    });
    
    connect(_ui->action_Compose, SIGNAL(triggered()), this, SLOT(composeTrackSlot()));
    
    connect(_ui->firstPointButtonBox, &QDialogButtonBox::rejected, [this](){
        _ui->firstPointGroupBox->hide();
        _ui->treeView->setDisabled(false);
    });
    connect(_ui->firstPointButtonBox, &QDialogButtonBox::accepted, [this](){
        ObservationTrackLayer* layer = new ObservationTrackLayer(_map);
        
        QDateTime dt = _ui->firstPointDateTimeEdit->dateTime();
        boost::posix_time::ptime time = boost::posix_time::time_from_string(dt.toString("yyyy-MMM-d HH:mm:ss").toStdString());
        
        layer->setName(_ui->firstPointLabelLineEdit->text().toStdString());
        
        stmr::observation_data data;
        data.location.lat = _first_latEdit->value();
        data.location.lng = _first_lonEdit->value();
        data.time = time;
        
        layer->append(data);
        
        addTrack(layer);
        _ui->firstPointGroupBox->hide();
        _ui->treeView->setDisabled(false);
        
        _map->update();
    });
    connect(_ui->actionUpdateTime, &QAction::triggered, [this](){
        QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
        if(!index.isValid()) return;
        
        AbstractTreeModelAUXItem<AbstractTrackLayer>* node = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer());
        
        if(node->depth() != 2){
            return;
        }
        
        AbstractTrackLayer*     alayer = node->actual();
        ObservationTrackLayer*  olayer = dynamic_cast<ObservationTrackLayer*>(alayer);
        int position = node->parentItem()->position();
        
        stmr::observation_data previous, next;
        stmr::observation_data current = olayer->observation_at(position);
        if(position != 0){
            previous = olayer->observation_at(position-1);
        }
        if(position < olayer->count()-1){
            next = olayer->observation_at(position+1);
        }
        
        prepareInsertTab(previous, current.location, next);
        _ui->editorTabWidget->show();
    
        std::string time_start_str = boost::posix_time::to_simple_string(current.time);
        QDateTime time_start = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
        _ui->firstDateTimeEdit->setDateTime(time_start);
        updateInsertTabSlot(time_start);
        
        _ui->treeView->setDisabled(true);
    });
    
    connect(_ui->action_Fence, &QAction::triggered, [this](){
        FenceLayer* layer = new FenceLayer(_map);
        _map->addLayer(layer);
        _map->installEventFilter(layer);
        _fences.push_back(layer);
        connect(layer, &FenceLayer::sealed, this, &STSimulator::fenceSealed);
    });
    
    connect(_ui->fenceTreeView->selectionModel(), &QItemSelectionModel::selectionChanged, [this](const  QItemSelection& selected, const  QItemSelection& deselected){
        QModelIndex selectedIndex;
        if(selected.indexes().size() >= 1){
            selectedIndex = selected.indexes()[0];
        }else{
            return;
        }
        
        AbstractTreeModelAUXItem<FenceLayer>* node = static_cast<AbstractTreeModelAUXItem<FenceLayer>*>(selectedIndex.internalPointer());
        if(node->depth() == 2){
            unsigned ic = node->position();
            unsigned mc = node->parentItem()->position();
            stmr::intersection x = node->actual()->track_intersection(mc, ic);
            _fenceDetailModel->setIntersection(x);
            _ui->fenceIntersectionDetailTableView->show();
        }else if(node->depth() == 1){
            FenceLayer* fence = node->actual();
            MergedTrackLayer* track = fence->intersected_track(node->position());
            
            _resultsModel->setFence(fence);
            _resultsModel->setTrack(track);
            
            if(_resultsButton->isChecked()){
                _resultsButton->animateClick();
            }
            _ui->fenceIntersectionDetailTableView->hide();
        }else{
            _ui->fenceIntersectionDetailTableView->hide();
            if(!_resultsButton->isChecked()){
                _resultsButton->animateClick();
            }
        }
    });
    
    _tracksButton  = new QPushButton("Tracks");
    _resultsButton = new QPushButton("Results");
    _tracksButton->setFlat(true);
    _resultsButton->setFlat(true);
    _tracksButton->setCheckable(true);
    _resultsButton->setCheckable(true);
    _ui->statusBar->addWidget(_tracksButton);
    _ui->statusBar->addWidget(_resultsButton);
    
    _tracksButton->setChecked(_ui->tracksDockWidget->toggleViewAction()->isChecked());
    _resultsButton->setChecked(_ui->resultsDockWidget->toggleViewAction()->isChecked());
    connect(_tracksButton, &QPushButton::clicked, _ui->tracksDockWidget->toggleViewAction(), &QAction::trigger);
    connect(_resultsButton, &QPushButton::clicked, _ui->resultsDockWidget->toggleViewAction(), &QAction::trigger);
    _resultsButton->animateClick();
    
    _resultsModel = new FenceTrackResultsModel;
    _ui->resultsTtableView->setModel(_resultsModel);
    
    _timelineView = new TimelineView(_resultsModel);
    _ui->timelineTab->layout()->addWidget(_timelineView);
}

Marble::MarbleWidget* STSimulator::map() const{
    return _map;
}

void STSimulator::addTrack(AbstractTrackLayer* track){
    _model->addRecord(track);
    track->setVisible(true);

    if(track->type() == ObservationTrack){
        _map->installEventFilter(dynamic_cast<ObservationTrackLayer*>(track));
        connect(dynamic_cast<ObservationTrackLayer*>(track), SIGNAL(locationClicked(ObservationTrackLayer*, double, double)), this, SLOT(locationClickedSlot(ObservationTrackLayer*, qreal, qreal)));
    }else if(track->type() == MergedTrack){
        _map->installEventFilter(dynamic_cast<MergedTrackLayer*>(track));
    }
    
    stmr::gps_coordinate last = track->at(track->count()-1);
//     _map->centerOn(GeoDataCoordinates(last.lng, last.lat, 0.0, GeoDataCoordinates::Degree), true);

    GeoDataLineString polyline;
    for(int i = 0; i < track->count(); ++i){
        stmr::gps_coordinate coordinate = track->at(i);
        polyline.append(GeoDataCoordinates(coordinate.lng, coordinate.lat, 0.0, GeoDataCoordinates::Degree));
    }
    
    GeoDataLatLonBox box = GeoDataLatLonBox::fromLineString(polyline);
    _map->centerOn(box, true);
    
}

void STSimulator::removeTrack(AbstractTrackLayer* track){
    // TODO remove record
    // TODO remo map layer
}

AbstractTrackLayer* STSimulator::addGeoJSONObservationTrack(const boost::filesystem::path& path, const std::string& name){
    std::vector<stmr::observation_data> data = stmr::parse_json(path, name);
    ObservationTrackLayer* layer = new ObservationTrackLayer(map());
    layer->setName(name);
    layer->copy(data.cbegin(), data.cend());
    addTrack(layer);
    return layer;
}

AbstractTrackLayer* STSimulator::addGeoJSONMergedTrack(const boost::filesystem::path& path, const std::string& name){
    std::vector<stmr::merged_data> data = stmr::parse_json_merged(path, name);
    MergedTrackLayer* layer = new MergedTrackLayer(map());
    layer->setName(name);
    layer->copy(data.cbegin(), data.cend());
    addTrack(layer);
    return layer;
}

STSimulator::~STSimulator() = default;

void STSimulator::addTrackSlot(){
    AddTrackDialog dialog(ObservationTrack);
    if(dialog.exec()){
        if(dialog.type() == ObservationTrack){
            addGeoJSONObservationTrack(dialog.path().toStdString(), dialog.name().toStdString())->setColor(dialog.color());
        }else{
            addGeoJSONMergedTrack(dialog.path().toStdString(), dialog.name().toStdString())->setColor(dialog.color());
        }
    }
}

void STSimulator::addMergedTrackSlot(){
    AddTrackDialog dialog(MergedTrack);
    if(dialog.exec()){
        if(dialog.type() == ObservationTrack){
            addGeoJSONObservationTrack(dialog.path().toStdString(), dialog.name().toStdString())->setColor(dialog.color());
        }else{
            addGeoJSONMergedTrack(dialog.path().toStdString(), dialog.name().toStdString())->setColor(dialog.color());
        }
    }
}

void STSimulator::mergeSlot(){
    QList<QModelIndex> indexes = _model->selectedIndexes();
    auto items = _model->selectedItems();
    _model->setCheckDepth(3);
    _model->setTableMode(false);

    if(items.size() == 0){
        _ui->markTracksButton->setChecked(false);
        return;
    }
    
    if(items.size() > 2){
        QMessageBox::critical(this, "Input not enough to merge", "Please select either only an ObservationTrack or a pair of ObservationTrack and MergedTrack");
        _ui->markTracksButton->setChecked(false);
        return;
    }

    if(items.size() == 2 && items[0]->actual()->type() == items[1]->actual()->type()){
        QMessageBox::critical(this, "Input not enough to merge", "Please select either only an ObservationTrack or a pair of ObservationTrack and MergedTrack");
        _ui->markTracksButton->setChecked(false);
        return;
    }
    
    QModelIndex observationIndex, mergedIndex;
    
    if(items.size() == 1){
        if(items[0]->actual()->type() == ObservationTrack){
            observationIndex = indexes[0];
        }else{
            QMessageBox::critical(this, "Input not enough to merge", "Please select either only an ObservationTrack or a pair of ObservationTrack and MergedTrack");
            _ui->markTracksButton->setChecked(false);
            return;
        }
    }else if(items.size() == 2){
        if(items[0]->actual()->type() == ObservationTrack){
            observationIndex = indexes[0];
            mergedIndex = indexes[1];
        }else{
            observationIndex = indexes[1];
            mergedIndex = indexes[0];
        }
    }
    
    MergeDialog dialog(_model, observationIndex, mergedIndex);
    if(dialog.exec()){
        AbstractTreeModelAUXItem<AbstractTrackLayer>* internal_data = mergedIndex.isValid() ? static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(mergedIndex.internalPointer()) : 0x0;
        MergedTrackLayer* layer_old = internal_data ? dynamic_cast<MergedTrackLayer*>(internal_data->actual()) : 0x0;
        MergedTrackLayer* layer_new = dialog.createMergedLayer(_map, layer_old);
        if(!layer_old){
            std::string name = dialog.name().toStdString();
            QColor color = dialog.color();
            layer_new->setName(name);
            layer_new->setColor(color);
            addTrack(layer_new);
        }else{
            _model->repopulate(layer_new);
        }
        layer_new->computeHull();
        if(layer_old == layer_new){
            for(FenceLayer* fence: _fences){
                layer_new->intersect(fence);
            }
        }
        if(observationIndex.isValid() && observationIndex.internalPointer()){
            AbstractTreeModelAUXItem<AbstractTrackLayer>* observation_internal_data = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(observationIndex.internalPointer());
            if(observation_internal_data){
                ObservationTrackLayer* olayer = dynamic_cast<ObservationTrackLayer*>(observation_internal_data->actual());
                olayer->setVisible(false);
            }
        }
    }
    _ui->markTracksButton->setChecked(false);
}

void STSimulator::extendTrackSlot(){
    QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;
    
    AbstractTreeModelAUXItem<AbstractTrackLayer>* node = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer());
    
    if(node->depth() == -1){
        return;
    }
    
    AbstractTrackLayer*     layer  = node->actual();
    ObservationTrackLayer*  olayer = dynamic_cast<ObservationTrackLayer*>(layer);
    
    if(node->depth() == 2){ // TimeNode
        node = node->parentItem();
    }else if(node->depth() == 0){
        node = node->lastItem();
    }
    //{ Set Mode to Display for all Layers
    for(auto record: _model->_records){
        ObservationTrackLayer* l = dynamic_cast<ObservationTrackLayer*>(record);
        l->setMode(ObservationTrackLayer::Mode::Display);
    }
    //}
    olayer->setMode(ObservationTrackLayer::Mode::Insert, node->position());
    
    // Now olayer->locationClicked signal is supposed to be emitted which is supposed to be captured by a slot.
}

void STSimulator::prepareInsertTab(const stmr::observation_data& previous, const stmr::gps_coordinate& location, const stmr::observation_data& next){
    _latEdit->setValue(location.lat);
    _lonEdit->setValue(location.lng);
    
    //{ flush
    _ui->firstDateTimeEdit->clearMinimumDateTime();
    _ui->firstDateTimeEdit->clearMaximumDateTime();
    
    _ui->firstDurationEdit->setVisible(false);
    _ui->firstDistanceLabel->setVisible(false);
    _ui->firstDistanceSpinBox->setVisible(false);
    _ui->firstVelocitySpinBox->setVisible(false);
    
    _ui->secondDurationEdit->setVisible(false);
    _ui->secondDistanceLabel->setVisible(false);
    _ui->secondDistanceSpinBox->setVisible(false);
    _ui->secondVelocitySpinBox->setVisible(false);
    //}

    if(!previous.invalid()){
        std::string time_start_str = boost::posix_time::to_simple_string(previous.time);
        QDateTime time_start = QDateTime::fromString(QString::fromStdString(time_start_str), "yyyy-MMM-d HH:mm:ss");
        _ui->firstDateTimeEdit->setDateTime(time_start);
        _ui->firstDateTimeEdit->setMinimumDateTime(time_start);
        
        _ui->firstDurationEdit->setTime(QTime::fromMSecsSinceStartOfDay(0));
        double distance = stmr::distance(previous.location, location)*1000;  
        _ui->firstDistanceSpinBox->setValue(distance);
        
        _ui->firstDurationEdit->setVisible(true);
        _ui->firstDistanceLabel->setVisible(true);
        _ui->firstDistanceSpinBox->setVisible(true);
        _ui->firstVelocitySpinBox->setVisible(true);
    }
    if(!next.invalid()){
        std::string time_end_str = boost::posix_time::to_simple_string(next.time);
        QDateTime time_end = QDateTime::fromString(QString::fromStdString(time_end_str), "yyyy-MMM-d HH:mm:ss");
        _ui->firstDateTimeEdit->setMaximumDateTime(time_end);
        
        _ui->secondDurationEdit->setTime(QTime::fromMSecsSinceStartOfDay(0));
        double distance = stmr::distance(next.location, location)*1000;
        _ui->secondDistanceSpinBox->setValue(distance);
        
        _ui->secondDurationEdit->setVisible(true);
        _ui->secondDistanceLabel->setVisible(true);
        _ui->secondDistanceSpinBox->setVisible(true);
        _ui->secondVelocitySpinBox->setVisible(true);
    }
}


void STSimulator::locationClickedSlot(ObservationTrackLayer* layer, qreal lat, qreal lon){
    if(layer->mode() == ObservationTrackLayer::Mode::Insert){
        _editedTrack = layer;
        
        stmr::observation_data pivot = layer->pivot();
        stmr::gps_coordinate location(lat, lon);
        
        if(layer->has_pivot_next()){
            prepareInsertTab(pivot, location, layer->pivot_next());
        }else{
            prepareInsertTab(pivot, location, stmr::observation_data());
        }
        
        _ui->editorTabWidget->show();
        _ui->treeView->setDisabled(true);
        _map->setDisabled(true);
    }
}

void STSimulator::composeTrackSlot(){
    ObservationTrackLayer* layer = new ObservationTrackLayer(_map);
    stmr::observation_data data;  
    Marble::GeoDataLookAt lookat = _map->lookAt();
    data.location.lat = lookat.latitude(GeoDataCoordinates::Degree);
    data.location.lng = lookat.longitude(GeoDataCoordinates::Degree);
    data.index = 0;
    data.label = "N";
    data.time = boost::posix_time::second_clock::local_time();
    layer->setColor(Qt::blue);
    layer->setName("N");
    layer->append(data);
    addTrack(layer);
}

void STSimulator::locationSelectedSlot(){
    QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;
    QDateTime dt = _ui->firstDateTimeEdit->dateTime();
    boost::posix_time::ptime time = boost::posix_time::time_from_string(dt.toString("yyyy-MMM-d HH:mm:ss").toStdString());
    
    if(_editedTrack && _editedTrack->mode() == ObservationTrackLayer::Mode::Insert){
        stmr::observation_data pivot = _editedTrack->pivot();
        stmr::observation_data data;
        data.location = stmr::gps_coordinate(_latEdit->value(), _lonEdit->value());
        data.time = time;
        
        _model->insertObservationPoint(index, data);
        
        _map->update();
    }else{  
        AbstractTreeModelAUXItem<AbstractTrackLayer>* node = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer());
        
        if(node->depth() != 2){
            return;
        }
        
        AbstractTrackLayer*     layer  = node->actual();
        ObservationTrackLayer*  olayer = dynamic_cast<ObservationTrackLayer*>(layer);

        int position = node->parentItem()->position();
        olayer->update(position, time);
    }
    locationRejectedSlot();
}

void STSimulator::locationRejectedSlot(){
    _ui->editorTabWidget->hide();
    _ui->treeView->setDisabled(false);
    _map->setDisabled(false);
    //{ Set Mode to Display for all Layers
    for(auto record: _model->_records){
        ObservationTrackLayer* l = dynamic_cast<ObservationTrackLayer*>(record);
        l->setMode(ObservationTrackLayer::Mode::Display);
    }
    //}
    _editedTrack = 0x0;
}

void STSimulator::updateInsertTabSlot(const QDateTime& dt){
    if(_ui->firstDurationEdit->isVisible()){
        double time_duration_msecs = _ui->firstDateTimeEdit->minimumDateTime().msecsTo(dt);
        _ui->firstDurationEdit->setTime(QTime::fromMSecsSinceStartOfDay(time_duration_msecs));
    }
    if(_ui->secondDurationEdit->isVisible()){
        double time_duration_msecs = dt.msecsTo(_ui->firstDateTimeEdit->maximumDateTime());
        _ui->secondDurationEdit->setTime(QTime::fromMSecsSinceStartOfDay(time_duration_msecs));
    }
    updateVelocity();
}

void STSimulator::updateVelocity(){
    if(_ui->firstDurationEdit->isVisible()){
        QTime duration = _ui->firstDurationEdit->time();
        double time_duration_msecs = duration.msecsSinceStartOfDay();
        double time_duration_secs = time_duration_msecs/1000.0f;
        if(time_duration_secs == 0){
            _ui->firstVelocitySpinBox->setValue(0);
            _ui->firstVelocitySpinBox->setSuffix("");
            _ui->addObservationDataButtonBox->setDisabled(true);
            return;
        }
        double distance = _ui->firstDistanceSpinBox->value();
        double velocity = (distance/time_duration_secs)*3600.0f;
        if(velocity > 1000.0f){
            velocity = velocity / 1000.0f;
            _ui->firstVelocitySpinBox->setSuffix("Km/h");
        }else{
            _ui->firstVelocitySpinBox->setSuffix("m/h");
        }
        _ui->firstVelocitySpinBox->setValue(velocity);
        _ui->addObservationDataButtonBox->setEnabled(true);
    }
    if(_ui->secondDurationEdit->isVisible()){
        QTime duration = _ui->secondDurationEdit->time();
        double time_duration_msecs = duration.msecsSinceStartOfDay();
        double time_duration_secs = time_duration_msecs/1000.0f;
        if(time_duration_secs == 0){
            _ui->secondVelocitySpinBox->setValue(0);
            _ui->secondVelocitySpinBox->setSuffix("");
            _ui->addObservationDataButtonBox->setDisabled(true);
            return;
        }
        double distance = _ui->firstDistanceSpinBox->value();
        double velocity = (distance/time_duration_secs)*3600.0f;
        if(velocity > 1000.0f){
            velocity = velocity / 1000.0f;
            _ui->secondVelocitySpinBox->setSuffix("Km/h");
        }else{
            _ui->secondVelocitySpinBox->setSuffix("m/h");
        }
        _ui->secondVelocitySpinBox->setValue(velocity);
        _ui->addObservationDataButtonBox->setEnabled(true);
    }
}

void STSimulator::saveSelectedSlot(){
    QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;
    
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), ".", tr("GeoJSON (*.geojson)"));
    static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer())->actual()->save(fileName.toStdString());
}

void STSimulator::changeColorSlot(){
    if(_ui->treeView->isVisible()){
        QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
        if(!index.isValid()) return;
        
        AbstractTrackLayer* layer = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer())->actual();
        QColor color = QColorDialog::getColor(layer->color(), this);
        layer->setColor(color);
    }else if(_ui->fenceTreeView->isVisible()){
        QModelIndex index = _ui->fenceTreeView->selectionModel()->currentIndex();
        if(!index.isValid()) return;
        
        FenceLayer* layer = static_cast<AbstractTreeModelAUXItem<FenceLayer>*>(index.internalPointer())->actual();
        QColor color = QColorDialog::getColor(layer->color(), this);
        layer->setColor(color);
    }
}

void STSimulator::changeNameSlot(){
    QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;
    
    AbstractTrackLayer* layer = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer())->actual();
    bool ok;
    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"), tr("Track name:"), QLineEdit::Normal,  QString::fromStdString(layer->name()), &ok);
    if (ok && !text.isEmpty()){
        layer->setName(text.toStdString());
    }
}

void STSimulator::simplifySlot(){
    QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;
    
    AbstractTrackLayer* layer = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer())->actual();
    MergedTrackLayer* mlayer = dynamic_cast<MergedTrackLayer*>(layer);
    auto merged_collection = mlayer->collection();
    bool okay;
    double th = QInputDialog::getDouble(this, "Threshold in m", "Threshold", 1, 0.0, 1000.0, 2, &okay);
    if(!okay){
        return;
    }
    
    th = th/1000.0f;
    auto simplified_collection = stmr::algorithms::simplify(merged_collection, th);
    MergedTrackLayer* simplified_mlayer = new MergedTrackLayer(_map);
    simplified_mlayer->clear();
    for(auto it = simplified_collection.cbegin(); it != simplified_collection.cend(); ++it){
        simplified_mlayer->append(*it);
    }
    QString track_name = QString("S %1 m").arg(th*1000);
    simplified_mlayer->setName(track_name.toStdString());
    addTrack(simplified_mlayer);
}

void STSimulator::simplifyRDPSlot(){
    QModelIndex index = _ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;
    
    AbstractTrackLayer* layer = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(index.internalPointer())->actual();
    MergedTrackLayer* mlayer = dynamic_cast<MergedTrackLayer*>(layer);
    auto merged_collection = mlayer->collection();
    bool okay;
    double th = QInputDialog::getDouble(this, "Threshold in m", "Threshold", 1, 0.0, 1000.0, 2, &okay);
    if(!okay){
        return;
    }
    
    th = th/1000.0f;
    auto simplified_collection = stmr::algorithms::simplify_rdp(merged_collection, th);
    MergedTrackLayer* simplified_mlayer = new MergedTrackLayer(_map);
    simplified_mlayer->clear();    
    for(auto it = simplified_collection.cbegin(); it != simplified_collection.cend(); ++it){
        simplified_mlayer->append(*it);
    }
    QString track_name = QString("S %1 m").arg(th*1000);
    simplified_mlayer->setName(track_name.toStdString());
    addTrack(simplified_mlayer);
}

void STSimulator::selectionChangedSlot(const QItemSelection& selected, const QItemSelection& deselected){
    QModelIndexList sindexes = selected.indexes();
    QModelIndexList dindexes = deselected.indexes();
    if(sindexes.count() != 1){
        return;
    }
    QModelIndex sindex = sindexes.first();
    QModelIndex dindex = !dindexes.isEmpty() ? dindexes.first() : QModelIndex();
    AbstractTreeModelAUXItem<AbstractTrackLayer>* snode = sindex.isValid() ? static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(sindex.internalPointer()) : 0x0;
    AbstractTreeModelAUXItem<AbstractTrackLayer>* dnode = dindex.isValid() ? static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(dindex.internalPointer()) : 0x0;
    if(!snode){
        return;
    }
    AbstractTrackLayer* slayer      = snode->actual();
    stmr::gps_coordinate slocation  = slayer->at(0);
    AbstractTrackLayer* dlayer = 0x0;
    if(dnode){
        dlayer = dnode->actual();
        stmr::gps_coordinate dlocation  = dlayer->at(0);
        if(stmr::distance(slocation, dlocation) < 0.5){
            _map->setDistance(0.05);
        }else{
            _map->setDistance(0.2);
        }
    }else{
        _map->setDistance(0.2);
    }
    
    if(dlayer && snode->depth() == 0){
        GeoDataLineString polyline;
        for(int i = 0; i < dlayer->count(); ++i){
            stmr::gps_coordinate coordinate = dlayer->at(i);
            polyline.append(GeoDataCoordinates(coordinate.lng, coordinate.lat, 0.0, GeoDataCoordinates::Degree));
        }
        _ui->actionAdd_Point->setVisible(true);
        _ui->actionUpdateTime->setVisible(false);
        GeoDataLatLonBox box = GeoDataLatLonBox::fromLineString(polyline);
        _map->centerOn(box, true);
    }else if(snode->depth() == 1){
        slocation = slayer->at(snode->position());
        _ui->actionAdd_Point->setVisible(true);
        _ui->actionUpdateTime->setVisible(false);
        _map->centerOn(slocation.lng, slocation.lat, true);
    }else if(snode->depth() == 2){
        slocation = slayer->at(snode->parentItem()->position());
        _ui->actionAdd_Point->setVisible(false);
        _ui->actionUpdateTime->setVisible(true);
        _map->centerOn(slocation.lng, slocation.lat, true);
    }
}

bool STSimulator::event(QEvent* event){
    bool ret = QMainWindow::event(event);
    if(event->type() == QEvent::KeyPress){
        QKeyEvent* ev = static_cast<QKeyEvent*>(event);
        if(ev->matches(QKeySequence::Cancel)){
            _ui->editorTabWidget->hide();
            _ui->treeView->setDisabled(false);
            _map->setDisabled(false);
            //{ Set Mode to Display for all Layers
            for(auto record: _model->_records){
                ObservationTrackLayer* l = dynamic_cast<ObservationTrackLayer*>(record);
                l->setMode(ObservationTrackLayer::Mode::Display);
            }
            //}
            _editedTrack = 0x0;
            _map->update();
        }
        return true;
    }
    return ret;
}

void STSimulator::fenceSealed(FenceLayer* fence){
    for(auto i = _model->begin(); i != _model->end(); ++i){
        AbstractTrackLayer* layer = *i;
        if(layer->type() == MergedTrack){
            MergedTrackLayer* merged_layer = dynamic_cast<MergedTrackLayer*>(layer);
            merged_layer->intersect(fence);
        }
    }
    _fenceModel->addRecord(fence);
}
