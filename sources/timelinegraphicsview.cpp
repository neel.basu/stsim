/*
 * Copyright 2019 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "timelinegraphicsview.h"
#include "fencetrackresultsmodel.h"
#include <QGraphicsTextItem>
#include <QLinearGradient>
#include <QWheelEvent>
#include <cmath>

TimelineGraphicsView::TimelineGraphicsView(QWidget* parent): QGraphicsView(parent), _rulerHeight(10), _labelHeight(20), _extraWidth(0){
    setScene(&_scene);
    setAlignment(Qt::AlignTop | Qt::AlignLeft);
}

void TimelineGraphicsView::render(){
    int w = length()+_extraWidth;
    
    setSceneRect(0.0, 0.0, w, height());
//     fitInView(0.0, 0.0, w, height());
    QLinearGradient gradiant;
    
    _scene.addRect(QRectF(0.0, 0.0, w, _labelHeight+_rulerHeight), QPen(Qt::transparent), QBrush(QColor("#CAC8C8")));
    
    QFont label_font("sans", 9);
    QFontMetrics metric(label_font);
    
    for(size_t i = 0; i < count(); ++i){
        TimelineGraphicsView::bounds_type range = bounds(i);
        if(i == 0){
            QString lower_label = lower(i);
            QRectF trect = QRectF(0.0, 0.0, metric.width(lower_label)+7, _labelHeight);
            trect.moveLeft(range.first * w - trect.width()/2);
            _scene.addRect(trect, QPen(Qt::transparent), QBrush(Qt::black));
            QGraphicsTextItem* ltitem = _scene.addText(lower_label, label_font);
            ltitem->setPos(trect.topLeft());
            ltitem->setDefaultTextColor(Qt::white);
            
            _scene.addRect(QRectF(range.first * w, _labelHeight, 2, _rulerHeight), QPen(Qt::transparent), QBrush(QColor("#0e0e0e")));
        }
        
        {
            QString upper_label = upper(i);
            QRectF trect = QRectF(0.0, 0.0, metric.width(upper_label)+7, _labelHeight);
            trect.moveLeft(range.second * w - trect.width()/2);
            _scene.addRect(trect, QPen(Qt::transparent), QBrush(Qt::black));
            QGraphicsTextItem* utitem = _scene.addText(upper_label, label_font);
            utitem->setPos(trect.topLeft());
            utitem->setDefaultTextColor(Qt::white);
            
            _scene.addRect(QRectF(range.second * w, _labelHeight, 2, _rulerHeight), QPen(Qt::transparent), QBrush(QColor("#0e0e0e")));
        }
        
        {
            QRectF value_rect(range.first* w, height() - (value(i)*(height()-_labelHeight-_rulerHeight)), (range.second-range.first) * w, value(i)*(height()-_labelHeight-_rulerHeight));
            QString value_label = QString("%1").arg(value(i));
            QRectF trect = QRectF(0.0, 0.0, metric.width(value_label)+7, _labelHeight);
            trect.moveCenter(QPointF(value_rect.center().x(), value_rect.top()-trect.height()));
            QGraphicsTextItem* vtitem = _scene.addText(value_label, label_font);
            vtitem->setPos(trect.topLeft());
            vtitem->setDefaultTextColor(QColor("#c3b4ff"));
                
            _scene.addRect(value_rect, QPen(Qt::transparent), QBrush(QColor("#c3b4ff")));
        }
    }
}

int TimelineGraphicsView::length() const{
    return width();
}

void TimelineGraphicsView::resizeEvent(QResizeEvent* event){
    _scene.clear();
    render();
}

void TimelineGraphicsView::wheelEvent(QWheelEvent* event){
//     double angle = event->angleDelta().y();
//     double factor = std::pow(1.0015, angle);
//     scale(factor, 0.0);
    _extraWidth = _extraWidth+event->angleDelta().y()*100;
    _scene.clear();
    render();
}

TimelineView::TimelineView(FenceTrackResultsModel* model): _model(model){
    
}

size_t TimelineView::count() const{
    return _model->rowCount();
}

TimelineGraphicsView::bounds_type TimelineView::bounds(size_t i) const{
    QDateTime min   = _model->min();
    QDateTime max   = _model->max();
    double duration = min.secsTo(max);
    QDateTime lower = _model->lower(i);
    QDateTime upper = _model->upper(i);
    
    return std::make_pair((double)min.secsTo(lower)/duration, (double)min.secsTo(upper)/duration);
}

QString TimelineView::lower(size_t i) const{
    return _model->lower(i).toString();
}

QString TimelineView::upper(size_t i) const{
    return _model->upper(i).toString();
}

TimelineGraphicsView::value_type TimelineView::value(size_t i) const{
    return _model->probability(i);
}

int TimelineView::length() const{
    QDateTime min   = _model->min();
    QDateTime max   = _model->max();
    return min.secsTo(max);
}
