#include "stmr.h"
#include "fencelayer.h"
#include "tracklayer.h"
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <marble/GeoDataLookAt.h>
#include <marble/GeoDataLatLonBox.h>
#include <marble/GeoDataLatLonAltBox.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoDataLinearRing.h>
#include <marble/ViewportParams.h>
#include <boost/range/irange.hpp>
#include <boost/range/combine.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/bind.hpp>
#include "fencedialog.h"
#include "geopaint.h"

FenceLayer::FenceLayer(MarbleWidget* widget): _map(widget), _mode(Editable), _color(Qt::yellow){
    GeoDataLookAt focus = _map->lookAt();
    _center = GeoDataCoordinates(focus.longitude(GeoDataCoordinates::Degree), focus.latitude(GeoDataCoordinates::Degree), 0.0, GeoDataCoordinates::Degree);
    _border = _center;
}

QStringList FenceLayer::renderPosition() const{
    return QStringList() << "HOVERS_ABOVE_SURFACE";
}

void FenceLayer::drawFence(Marble::GeoPainter* painter, Marble::GeoDataCoordinates center, double radius){
    double height = radius;
    double width  = radius;
    // Initialize variables
    const qreal centerLon = center.longitude( GeoDataCoordinates::Degree );
    const qreal centerLat = center.latitude( GeoDataCoordinates::Degree );
    const qreal altitude = center.altitude();

    // Ensure a valid latitude range: 
    if ( centerLat + 0.5 * height > 90.0 || centerLat - 0.5 * height < -90.0 ) {
        return;
    }

    // Don't show the ellipse if it's too small:
    GeoDataLatLonBox ellipseBox( centerLat + 0.5 * height, centerLat - 0.5 * height,
                            centerLon + 0.5 * width,  centerLon - 0.5 * width, 
                            GeoDataCoordinates::Degree );
    if ( !_map->viewport()->viewLatLonAltBox().intersects( ellipseBox ) ||
    !_map->viewport()->resolves( ellipseBox ) ) return;

    GeoDataLinearRing ellipse;

    // Optimizing the precision by determining the size which the 
    // ellipse covers on the screen:
    const qreal degreeResolution = _map->viewport()->angularResolution() * RAD2DEG;
    // To create a circle shape even for very small precision we require uneven numbers:
    const int scaled_resolution = qMin<qreal>( width / degreeResolution / 8 + 1, 81 );
    const int precision = qMin<qreal>( width / degreeResolution / 8 + 1, 81 ) < 81 ? 81 : scaled_resolution ;

    // Calculate the shape of the upper half of the ellipse:
    for ( int i = 0; i <= precision; ++i ) {
        const qreal t = 1.0 - 2.0 * (qreal)(i) / (qreal)(precision);
        const qreal lat = centerLat + 0.5 * height * sqrt( 1.0 - t * t );
        const qreal lon = centerLon + 0.5 * width * t;
        ellipse << GeoDataCoordinates(lon, lat, altitude, GeoDataCoordinates::Degree);
    }
    // Calculate the shape of the lower half of the ellipse:
    for ( int i = 0; i <= precision; ++i ) {
        const qreal t = 2.0 * (qreal)(i) / (qreal)(precision) -  1.0;
        const qreal lat = centerLat - 0.5 * height * sqrt( 1.0 - t * t );
        const qreal lon = centerLon + 0.5 * width * t;
        ellipse << GeoDataCoordinates(lon, lat, altitude, GeoDataCoordinates::Degree);
    }

    painter->drawPolygon(ellipse);
}

bool FenceLayer::render(Marble::GeoPainter* painter, Marble::ViewportParams* /*viewport*/, const QString& /*renderPos*/, Marble::GeoSceneLayer* /*layer*/){
    stmr::gps_coordinate gcenter(_center.latitude(GeoDataCoordinates::Degree), _center.longitude(GeoDataCoordinates::Degree));
    stmr::gps_coordinate gborder(_border.latitude(GeoDataCoordinates::Degree), _border.longitude(GeoDataCoordinates::Degree));
    double distance  = stmr::distance(gcenter, gborder);
    
    painter->save();
    QColor fence_background_color = _color;
    fence_background_color.setAlphaF(0.5);
    painter->setBrush(fence_background_color);
    painter->setPen(QPen(Qt::transparent));
    // painter->drawEllipse(_center, 2*(distance/earthRadiusKm)* 180.0/3.14159, 2*(distance/earthRadiusKm)* 180.0/3.14159, true);
    drawFence(painter, _center, 2*(distance/earthRadiusKm)* 180.0/3.14159);
    
    {
        QColor fence_border_color = _color;
        fence_border_color.setRedF  (1.0-_color.redF()  );
        fence_border_color.setBlueF (1.0-_color.blueF() );
        fence_border_color.setGreenF(1.0-_color.greenF());
        fence_border_color.setAlphaF(1.0);
        painter->setPen(QPen(fence_border_color));
        if(_mode == Editable){
            GeoDataLineString radius_line;
            radius_line.append(_center);
            radius_line.append(_border);
            painter->drawPolyline(radius_line, QString("%1 m").arg(distance*1000));
        }
    }
    
    QColor intersection_color = _color;
    intersection_color.setRedF(1.0-intersection_color.redF());
    intersection_color.setBlueF(1.0-intersection_color.blueF());
    intersection_color.setGreenF(1.0-intersection_color.greenF());
    intersection_color.setAlphaF(0.7);
    painter->setPen(QPen(intersection_color));
    for(auto track_intersections: _intersections){
        for(const intersection& x: track_intersections.second){
            painter->setBrush(QBrush(intersection_color.darker()));
            painter->drawRect(GeoDataCoordinates(x._point.location.lng, x._point.location.lat, 0.0, GeoDataCoordinates::Degree), 15.0, 15.0);
            if(x._entry){
                painter->setBrush(QBrush(Qt::white));
                painter->drawRect(GeoDataCoordinates(x._point.location.lng, x._point.location.lat, 0.0, GeoDataCoordinates::Degree), 8.0, 8.0);
            }
        }
    }
    
    if(!_hover._point.invalid()){
        QPen pen(intersection_color);
        QRect rect = internal::paint::highlightSquare(painter, _hover._point.location, pen);
        QRect area = internal::paint::highlightBubble(painter, _hover._point.location, pen, internal::geopaint<stmr::merged_data>::height(_hover._point, rect.height()), rect.width()/2, internal::geopaint<stmr::merged_data>::bubbleRight());
        internal::paint::highlightBubbleText(painter, _hover, area, pen);
    }

    painter->restore();
    return true;
}

bool FenceLayer::eventFilter(QObject* watched, QEvent* event){
    if(_mode == Editable && event->type() == QEvent::MouseButtonRelease){
        QMouseEvent* ev = static_cast<QMouseEvent*>(event);
        QPoint pos = ev->pos();
        double lat, lng;
        bool okay = _map->geoCoordinates(pos.x(), pos.y(), lng, lat, GeoDataCoordinates::Degree);
        if(okay){
            if(ev->modifiers() & Qt::ControlModifier){// border
                _border = GeoDataCoordinates(lng, lat, 0.0, GeoDataCoordinates::Degree);
                
                stmr::gps_coordinate gcenter(_center.latitude(GeoDataCoordinates::Degree), _center.longitude(GeoDataCoordinates::Degree));
                stmr::gps_coordinate gborder(_border.latitude(GeoDataCoordinates::Degree), _border.longitude(GeoDataCoordinates::Degree));
                double distance  = stmr::distance(gcenter, gborder);
                
                FenceDialog dialog;
                dialog.setLocation(_center);
                dialog.setRadius(distance*1000.0);
                if(dialog.exec() == QDialog::Accepted){
                    _mode  = Sealed;
                    _start = boost::posix_time::time_from_string(dialog.start().toString("yyyy-MMM-d HH:mm:ss").toStdString());
                    _end   = boost::posix_time::time_from_string(dialog.end().toString("yyyy-MMM-d HH:mm:ss").toStdString());
                    emit sealed(this);
                }
            }else{// center
                _center = GeoDataCoordinates(lng, lat, 0.0, GeoDataCoordinates::Degree);
                _border = _center;
            }
        }
    }else if(event->type() == QEvent::MouseMove){
        QMouseEvent* ev = static_cast<QMouseEvent*>(event);
        QPoint pos = ev->pos();
        double lat, lng;
        bool okay = _map->geoCoordinates(pos.x(), pos.y(), lng, lat, GeoDataCoordinates::Degree);
        if(okay){
            _hover = intersection();
            stmr::gps_coordinate hover(lat, lng);
            for(auto pair: _intersections){
                for(const intersection& x: pair.second){
                    double distance = stmr::distance(x._point.location, hover);
                    if(distance <= 0.005){
                        _hover = x;
                        break;
                    }
                }
            }
            _map->update();
        }
    }
    return false;
}

stmr::gps_coordinate FenceLayer::center() const{
    return stmr::gps_coordinate(_center.latitude(GeoDataCoordinates::Degree), _center.longitude(GeoDataCoordinates::Degree));
}

double FenceLayer::radius() const{
    stmr::gps_coordinate gcenter(_center.latitude(GeoDataCoordinates::Degree), _center.longitude(GeoDataCoordinates::Degree));
    stmr::gps_coordinate gborder(_border.latitude(GeoDataCoordinates::Degree), _border.longitude(GeoDataCoordinates::Degree));
    return stmr::distance(gcenter, gborder);
}

boost::posix_time::ptime FenceLayer::start() const{
    return _start;
}

boost::posix_time::ptime FenceLayer::finish() const{
    return _end;
}

QColor FenceLayer::color() const{
    return _color;
}

void FenceLayer::setColor(const QColor& color){
    _color = color;
    _map->update();
}

FenceLayer::intersection_map_type::const_iterator FenceLayer::begin() const{
    return _intersections.begin();
}

FenceLayer::intersection_map_type::const_iterator FenceLayer::end() const{
    return _intersections.end();
}

void FenceLayer::clear_intersections(){
    _intersections.clear();
}

std::size_t FenceLayer::tracks_intersected() const{
    return _intersections.size();
}

void FenceLayer::add_intersection(MergedTrackLayer* layer, const stmr::merged_data& m, bool entry){
    intersection_map_type::iterator i = _intersections.find(layer);
    if(i == _intersections.end()){
        std::vector<intersection> track_intersections;
        i = _intersections.insert(std::make_pair(layer, track_intersections)).first;
    }
    intersection x(m, _start, _end, entry);
    stmr::algorithms::calculate_temporal_detail(x);
    std::vector<boost::posix_time::ptime> start_times = stmr::algorithms::observation_start_times(layer->collection());
    unsigned mu_s = stmr::algorithms::periodicity(start_times.begin(), start_times.end());
    unsigned long lambda_s = stmr::algorithms::period_size(start_times.begin(), start_times.end(), mu_s);
    
    for(unsigned n = x._observation_start; n <= x._observation_end; ++n){
        std::vector<unsigned> observations = stmr::algorithms::related_instances(n, x._periodicity, m.times.size()/x._periodicity);
        x._related_observations.insert(std::make_pair(n, observations));
        std::vector<boost::posix_time::time_duration> delta_n;
        for(auto i: observations){
            boost::posix_time::time_duration delta = x._point.times[i-1] - start_times[i-1];
            delta_n.push_back(delta);
        }
        x._delta_map.insert(std::make_pair(n, delta_n));
        boost::posix_time::time_duration minimum = *std::min_element(delta_n.begin(), delta_n.end()), 
                                         maximum = *std::max_element(delta_n.begin(), delta_n.end()), 
                                         average =  std::accumulate (delta_n.begin(), delta_n.end(), boost::posix_time::time_duration())/delta_n.size();
        boost::posix_time::ptime start_n = stmr::algorithms::extrapolate_time(start_times.begin(), start_times.end(), n , mu_s, lambda_s);
        x._delta_distribution_map.insert(std::make_pair(n, boost::make_tuple(minimum, average, maximum)));
        x._start_times_map.insert(std::make_pair(n, start_n));
    }

    i->second.push_back(x);
    _map->update();
}

void FenceLayer::set_intersections(MergedTrackLayer* layer, const std::vector<stmr::merged_data>& X, const std::vector<bool>& states){
    std::vector<boost::posix_time::ptime> start_times = stmr::algorithms::observation_start_times(layer->collection());
    clear_intersections();
    for(auto i = 0; i != X.size() ; ++i){
        add_intersection(layer, X[i], states[i]);
    }

    std::vector<intersection>& intersections = _intersections.at(layer);
    for(stmr::intersection& x: intersections){
        x._observed_start_times = start_times;
    }
    
    stmr::time_interval_map_type results = stmr::algorithms::favourability_map(intersections.cbegin(), intersections.cend(), _start, _end, start_times);
    _results.insert(std::make_pair(layer, results));
}

MergedTrackLayer* FenceLayer::intersected_track(std::size_t i) const{
    auto track_index = _intersections.begin();
    std::advance(track_index, i);
    return track_index->first;
}

std::size_t FenceLayer::track_intersections(MergedTrackLayer* layer) const{
    intersection_map_type::const_iterator i = _intersections.find(layer);
    if(i == _intersections.end()){
        return 0;
    }
    return i->second.size();
}

std::size_t FenceLayer::track_intersections(std::size_t i) const{
    intersection_map_type::const_iterator it = _intersections.begin();
    std::advance(it, i);
    return it->second.size();
}

FenceLayer::intersection FenceLayer::track_intersection(MergedTrackLayer* layer, std::size_t i) const{
    intersection_map_type::const_iterator it = _intersections.find(layer);
    if(it == _intersections.end()){
        return FenceLayer::intersection();
    }
    if(i >= it->second.size()){
        return FenceLayer::intersection();
    }
    return it->second.at(i);
}

FenceLayer::intersection FenceLayer::track_intersection(std::size_t i, std::size_t j) const{
    intersection_map_type::const_iterator it = _intersections.begin();
    std::advance(it, i);
    return track_intersection(it->first, j);
}

std::size_t FenceLayer::track_intersection_times(MergedTrackLayer* layer, std::size_t i) const{
    return track_intersection(layer, i)._point.times.size();
}

std::size_t FenceLayer::track_intersection_times(std::size_t i, std::size_t j) const{
    intersection_map_type::const_iterator it = _intersections.begin();
    std::advance(it, i);
    return track_intersection_times(it->first, j);
}

boost::posix_time::ptime FenceLayer::track_intersection_time(MergedTrackLayer* layer, std::size_t i, std::size_t j) const{
    return track_intersection(layer, i)._point.times[j];
}

const stmr::time_interval_map_type& FenceLayer::results(MergedTrackLayer* track) const{
    return _results.at(track);
}
