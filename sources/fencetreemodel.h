#ifndef FENCETREEMODEL_H
#define FENCETREEMODEL_H

#include "fencelayer.h"
#include "treemodelaux.h"
#include <QAbstractItemModel>

/**
 * @todo write docs
 */
class FenceTreeModel : public QAbstractItemModel,  public TreeModelAUX<FenceLayer, 4>{
  typedef TreeModelAUX<FenceLayer, 4>         aux_type;
  typedef TreeModelAUXItem<FenceLayer, 2, -1> RootNode;
  typedef TreeModelAUXItem<FenceLayer, 2,  0> FenceNode;
  typedef TreeModelAUXItem<FenceLayer, 2,  1> TrackNode;
  typedef TreeModelAUXItem<FenceLayer, 2,  2> PointNode;
  typedef TreeModelAUXItem<FenceLayer, 2,  3> TimeNode;
  typedef TreeModelAUXItem<FenceLayer, 2,  3> ObservationNode;
  typedef TreeModelAUXItem<FenceLayer, 2,  4> ObservationTimeNode;
    
  public:
    virtual QVariant data(const QModelIndex& index, int role) const;
    virtual int columnCount(const QModelIndex& parent) const;
    virtual int rowCount(const QModelIndex& parent) const;
    virtual QModelIndex parent(const QModelIndex& child) const;
    virtual QModelIndex index(int row, int column, const QModelIndex& parent) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  public:
    virtual int ncolumns(AbstractType* item) const;
    virtual QVariant value(AbstractType* item, std::size_t column, int role) const;
    virtual QModelIndex populate(data_ptr_type data) override;
};

#endif // FENCETREEMODEL_H
