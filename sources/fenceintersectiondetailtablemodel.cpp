#include "fenceintersectiondetailtablemodel.h"

FenceIntersectionDetailTableModel::FenceIntersectionDetailTableModel(QObject *parent): QAbstractTableModel(parent){
}

QVariant FenceIntersectionDetailTableModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(orientation == Qt::Horizontal) return QVariant();
    if(role != Qt::DisplayRole) return QVariant();
    
    QStringList vheaders;
    vheaders << "qstart" << "qend" << "ostart" << "oend" << "sstart" << "send";
    return vheaders[section];
}

int FenceIntersectionDetailTableModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return 6;
}

int FenceIntersectionDetailTableModel::columnCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return 1;
}

QVariant FenceIntersectionDetailTableModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole){
        if(index.row() == 0){
            return boost::posix_time::to_simple_string(_intersection._request_start).c_str();
        }else if(index.row() == 1){
            return boost::posix_time::to_simple_string(_intersection._request_end).c_str();
        }else if(index.row() == 2){
            return QString("%1").arg(_intersection._observation_start);
        }else if(index.row() == 3){
            return QString("%2").arg(_intersection._observation_end);
        }else if(index.row() == 4){
            try{
                return boost::posix_time::to_simple_string(_intersection._service_start).c_str();
            }catch(...){return QVariant();}
        }else if(index.row() == 5){
            try{
                return boost::posix_time::to_simple_string(_intersection._service_end).c_str();
            }catch(...){return QVariant();}
        }
    }
    
    return QVariant();
}

void FenceIntersectionDetailTableModel::setIntersection(const stmr::intersection& x){
    _intersection = x;
}
