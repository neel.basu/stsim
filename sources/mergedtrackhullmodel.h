#ifndef MERGEDTRACKHULLMODEL_H
#define MERGEDTRACKHULLMODEL_H

#include <QSortFilterProxyModel>

/**
 * @todo write docs
 */
class MergedTrackHullModel : public QSortFilterProxyModel{
  public:
    MergedTrackHullModel(QObject *parent = 0);
  protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
    QVariant data(const QModelIndex & index, int role) const override;
};

#endif // MERGEDTRACKHULLMODEL_H
