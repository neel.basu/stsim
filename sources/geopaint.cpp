#include "geopaint.h"

QRect internal::paint::highlightSquare(GeoPainter* painter, const stmr::gps_coordinate& point, const QPen& pen){
    GeoDataCoordinates coordinate(point.lng, point.lat, 0.0, GeoDataCoordinates::Degree);
    
    QColor color = pen.color();
    color.setRedF(1.0f-color.redF());
    color.setGreenF(1.0f-color.greenF());
    color.setBlueF(1.0f-color.blueF());
    
    QPen hpen(QBrush(color), 2.0f);
    
    QPen old_pen = painter->pen();
    QBrush old_brush = painter->brush();
    painter->setBrush(Qt::transparent);
    painter->setPen(hpen);
    
    QRegion region = painter->regionFromEllipse(coordinate, 30, 30);
    QRect rect = region.boundingRect();
    rect.moveTop(rect.top()-rect.height()/2);
    rect.moveLeft(rect.left()-rect.width()/2);
    
    painter->drawRect(rect);
    painter->setBrush(color);
    
    painter->setBrush(old_brush);
    painter->setPen(old_pen);
    
    return rect;
}

QRect internal::paint::highlightBrace(Marble::GeoPainter* painter, const stmr::gps_coordinate& point, const QPen& pen){
    GeoDataCoordinates coordinate(point.lng, point.lat, 0.0, GeoDataCoordinates::Degree);
    
    QColor color = pen.color();
    color.setRedF(1.0f-color.redF());
    color.setGreenF(1.0f-color.greenF());
    color.setBlueF(1.0f-color.blueF());
    
    QPen hpen(QBrush(color), 2.0f);
    
    QPen old_pen = painter->pen();
    QBrush old_brush = painter->brush();
    painter->setBrush(Qt::transparent);
    painter->setPen(hpen);
    
    QPolygon upper, lower;
    
    QRegion region = painter->regionFromEllipse(coordinate, 30, 30);
    QRect rect = region.boundingRect();
    rect.moveTop(rect.top()-rect.height()/2);
    rect.moveLeft(rect.left()-rect.width()/2);
    QPoint br = rect.bottomRight();
    rect.setTop(rect.top()-rect.height()/2);
    rect.setLeft(rect.left()-rect.width()/2);
    rect.setBottomRight(br);
    
    QPoint utr = QPoint(rect.left()+2*rect.width()/3, rect.top());
    QPoint utl = rect.topLeft();
    QPoint ubl = QPoint(rect.left(), rect.top()+2*rect.height()/3);
    upper << utr << utl << ubl;
    
    QPoint ltr = QPoint(rect.right(), rect.top()+rect.height()/3);
    QPoint lbr = rect.bottomRight();
    QPoint lbl = QPoint(rect.left()+rect.width()/3, rect.bottom());
    lower << ltr << lbr << lbl;
    
    painter->drawPolyline(upper);
    painter->drawPolyline(lower);
    
    painter->setBrush(old_brush);
    painter->setPen(old_pen);
    
    return rect;
}

QRect internal::paint::highlightBubble(Marble::GeoPainter* painter, const stmr::gps_coordinate& point, const QPen& pen, int height, int width, bool right, int length){
    GeoDataCoordinates coordinate(point.lng, point.lat, 0.0, GeoDataCoordinates::Degree);
    QRegion region = painter->regionFromEllipse(coordinate, 30, 30);
    QRect rect = region.boundingRect();
    rect.moveTop(rect.top()-rect.height()/2);
    rect.moveLeft(rect.left()-rect.width()/2);
    
    QColor color = pen.color();
    color.setRedF(1.0f-color.redF());
    color.setGreenF(1.0f-color.greenF());
    color.setBlueF(1.0f-color.blueF());
    color.setAlphaF(0.7f);
    
    QPen hpen(QBrush(color), 2.0f);
    
    QPen old_pen = painter->pen();
    QBrush old_brush = painter->brush();
    painter->setBrush(QBrush(color.lighter()));
    painter->setPen(hpen);
    
    int w = width;
    int h = height;
    int l = length;
    
    QRect box;
    QPolygon bubble;
    if(right){
        QPoint tip(rect.right()+rect.width()/2, rect.top()+rect.height()/2);
        bubble << tip
            << QPoint(tip.x()+w/2,   tip.y()-h/2)
            << QPoint(tip.x()+w/2+l, tip.y()-h/2)
            << QPoint(tip.x()+w/2+l, tip.y()+h/2)
            << QPoint(tip.x()+w/2,   tip.y()+h/2);
        box = QRect(QPoint(tip.x()+w/2, tip.y()-h/2), QPoint(tip.x()+w/2+l, tip.y()+h/2));
    }else{
        QPoint tip(rect.left()-rect.width()/2, rect.top()+rect.height()/2);
        bubble << tip
            << QPoint(tip.x()-w/2,   tip.y()-h/2)
            << QPoint(tip.x()-w/2-l, tip.y()-h/2)
            << QPoint(tip.x()-w/2-l, tip.y()+h/2)
            << QPoint(tip.x()-w/2,   tip.y()+h/2);
        box = QRect(QPoint(tip.x()-w/2-l, tip.y()-h/2), QPoint(tip.x()-w/2, tip.y()+h/2));
    }

    painter->drawPolygon(bubble);
    
    painter->setBrush(old_brush);
    painter->setPen(old_pen);

    return box;
}

void internal::paint::highlightBubbleText(Marble::GeoPainter* painter, const stmr::observation_data& data, const QRect& rect, const QPen& pen){
    GeoDataCoordinates coordinate(data.location.lng, data.location.lat, 0.0, GeoDataCoordinates::Degree);
    QRegion region = painter->regionFromEllipse(coordinate, 30, 30);
    QRect bounds = region.boundingRect();
    bounds.moveTop(bounds.top()-bounds.height()/2);
    bounds.moveLeft(bounds.left()-bounds.width()/2);
    
    QColor color = pen.color();
    color.setAlphaF(0.7f);
    QColor ncolor = color;
    ncolor.setRedF(1.0f-color.redF());
    ncolor.setGreenF(1.0f-color.greenF());
    ncolor.setBlueF(1.0f-color.blueF());
    ncolor.setAlphaF(0.7f);
    
    QFont label_font("Helvetica [Cronyx]", 10, QFont::Normal);
    QFontMetrics fm(label_font);
    QString label = QString("N: %1/%2").arg(data.label.c_str()).arg(data.index);
    int pixelsWide = fm.width(label);
    int pixelsHigh = fm.height();
    painter->setFont(label_font);
    
    QRect label_rect(QPoint(rect.left(), rect.top()-pixelsHigh), QPoint(rect.left()+(pixelsWide > rect.width() ? pixelsWide : rect.width()), rect.top()));
    painter->setBrush(QBrush(ncolor));
    painter->setPen(QPen(ncolor.lighter()));
    painter->drawRect(label_rect);
    painter->setPen(QPen(color.darker()));
    painter->drawText(QPoint(rect.left(), rect.top()-5), label);
    
    QString text(boost::posix_time::to_simple_string(data.time).c_str());
    painter->drawText(rect, Qt::AlignLeft, text);
}

void internal::paint::highlightBubbleText(Marble::GeoPainter* painter, const stmr::merged_data& data, const QRect& rect, const QPen& pen){
    GeoDataCoordinates coordinate(data.location.lng, data.location.lat, 0.0, GeoDataCoordinates::Degree);
    QRegion region = painter->regionFromEllipse(coordinate, 30, 30);
    QRect bounds = region.boundingRect();
    bounds.moveTop(bounds.top()-bounds.height()/2);
    bounds.moveLeft(bounds.left()-bounds.width()/2);
    
    QColor color = pen.color();
    color.setAlphaF(0.7f);
    QColor ncolor = color;
    ncolor.setRedF(1.0f-color.redF());
    ncolor.setGreenF(1.0f-color.greenF());
    ncolor.setBlueF(1.0f-color.blueF());
    ncolor.setAlphaF(0.7f);
    
    QFont label_font("Helvetica [Cronyx]", 10, QFont::Normal);
    QFontMetrics fm(label_font);
    QString label = QString("M: %1/%2").arg(data.label.c_str()).arg(data.index);
    int pixelsWide = fm.width(label);
    int pixelsHigh = fm.height();
    painter->setFont(label_font);
    
    QRect label_rect(QPoint(rect.left(), rect.top()-pixelsHigh), QPoint(rect.left()+(pixelsWide > rect.width() ? pixelsWide : rect.width()), rect.top()));
    painter->setBrush(QBrush(ncolor));
    painter->setPen(QPen(ncolor.lighter()));
    painter->drawRect(label_rect);
    painter->setPen(QPen(color.darker()));
    painter->drawText(QPoint(rect.left(), rect.top()-5), label);
    
    std::vector<std::string> time_list;
    std::transform(data.times.cbegin(), data.times.cend(), std::back_inserter(time_list), [](const boost::posix_time::ptime& time){
        try{
            return boost::posix_time::to_simple_string(time);
        }catch(...){
            return std::string("----");
        }
    });
    std::string time_list_str = boost::algorithm::join(time_list, "\n");
    
    QFontMetrics fmat(label_font);
    QSize sz = fmat.size(0, time_list_str.c_str());
    QRect time_body = rect;
    time_body.setSize(sz);
    
    painter->drawText(time_body, 0, time_list_str.c_str());
}

void internal::paint::highlightBubbleText(Marble::GeoPainter* painter, const stmr::intersection& data, const QRect& rect, const QPen& pen){
    GeoDataCoordinates coordinate(data._point.location.lng, data._point.location.lat, 0.0, GeoDataCoordinates::Degree);
    QRegion region = painter->regionFromEllipse(coordinate, 30, 30);
    QRect bounds = region.boundingRect();
    bounds.moveTop(bounds.top()-bounds.height()/2);
    bounds.moveLeft(bounds.left()-bounds.width()/2);
    
    QColor color = pen.color();
    color.setAlphaF(0.7f);
    QColor ncolor = color;
    ncolor.setRedF(1.0f-color.redF());
    ncolor.setGreenF(1.0f-color.greenF());
    ncolor.setBlueF(1.0f-color.blueF());
    ncolor.setAlphaF(0.7f);
    
    QFont label_font("Helvetica [Cronyx]", 10, QFont::Normal);
    QFontMetrics fm(label_font);
    QString label = QString("%1 %2 | %3").arg(data._entry ? '+' : '-').arg(data._periodicity).arg(data._period_size);
    int pixelsWide = fm.width(label);
    int pixelsHigh = fm.height();
    painter->setFont(label_font);
    
    QRect label_rect(QPoint(rect.left(), rect.top()-pixelsHigh), QPoint(rect.left()+(pixelsWide > rect.width() ? pixelsWide : rect.width()), rect.top()));
    painter->setBrush(QBrush(ncolor));
    painter->setPen(QPen(ncolor.lighter()));
    painter->drawRect(label_rect);
    painter->setPen(QPen(color.darker()));
    painter->drawText(QPoint(rect.left(), rect.top()-5), label);
    
    std::vector<std::string> time_list;
    std::transform(data._point.times.cbegin(), data._point.times.cend(), std::back_inserter(time_list), [](const boost::posix_time::ptime& time){
        try{
            return boost::posix_time::to_simple_string(time);
        }catch(...){
            return std::string("----");
        }
    });
    std::string time_list_str = boost::algorithm::join(time_list, "\n");
    
    QFontMetrics fmat(label_font);
    QSize sz = fmat.size(0, time_list_str.c_str());
    QRect time_body = rect;
    time_body.setSize(sz);
    
    painter->drawText(time_body, 0, time_list_str.c_str());
}

void internal::paint::highlight(Marble::GeoPainter* painter, const stmr::observation_data& data, const QPen& pen, int height, int width, int length){
    QRect square = highlightSquare(painter, data.location, pen);
}

