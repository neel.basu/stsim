#ifndef FENCEINTERSECTIONDETAILTABLEMODEL_H
#define FENCEINTERSECTIONDETAILTABLEMODEL_H

#include "stmr.h"
#include "treemodelaux.h"
#include <QAbstractItemModel>
#include <QAbstractTableModel>

// class FenceIntersectionDetailTreeModel : public QAbstractItemModel,  public TreeModelAUX<stmr::intersection, 1>{
//   typedef TreeModelAUX<stmr::intersection, 1>         aux_type;
//   typedef TreeModelAUXItem<stmr::intersection, 2, -1> RootNode;
//   typedef TreeModelAUXItem<stmr::intersection, 2,  0> ObservationNode; // (observation number, start time)
//   typedef TreeModelAUXItem<stmr::intersection, 2,  1> Temporal; // (delta, time = start + delta)
// }

class FenceIntersectionDetailTableModel : public QAbstractTableModel{
  Q_OBJECT
  stmr::intersection _intersection;
  public:
    explicit FenceIntersectionDetailTableModel(QObject *parent = nullptr);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    void setIntersection(const stmr::intersection& x);
};

#endif // FENCEINTERSECTIONDETAILTABLEMODEL_H
