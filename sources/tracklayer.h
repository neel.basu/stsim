#ifndef TRACKLAYER_H
#define TRACKLAYER_H

#include <boost/filesystem/path.hpp>
#include <marble/GeoPainter.h>
#include <marble/LayerInterface.h>
#include <marble/MarbleWidget.h>
#include <marble/MarbleMap.h>
#include <marble/MarbleModel.h>
#include <marble/GeoPainter.h>
#include <marble/GeoDataLineString.h>
#include <QtGui/QKeyEvent>
#include <QStringList>
#include <QColor>
#include <QPen>
#include "stmr.h"
#include "geopaint.h"

class FenceLayer;

using namespace Marble;

enum TrackType{
    NullTrack = 0,
    ObservationTrack,
    MergedTrack,
    IterationTrack
};

class AbstractTrackLayer : public LayerInterface{
  protected:
    TrackType       _type;
    MarbleWidget*   _widget;
    QPen            _vpen;
    QPen            _epen;
    std::string     _name;
    bool            _visible;
  public:
    AbstractTrackLayer(MarbleWidget* widget, TrackType type);
    virtual ~AbstractTrackLayer();
  public:
    virtual QStringList renderPosition() const;
  public:
    void setName(const std::string& name);
    std::string name() const;
    TrackType type() const;
  public:
    void setVisible(bool visible);
    bool visible() const;
    void setVPen(const QPen& pen);
    void setEPen(const QPen& pen);
    void setPen(const QPen& pen);
    void setColor(const QColor& color);
    QPen pen() const;
    QColor color() const;
  public:
    virtual void clear() = 0;
    virtual size_t count() const = 0;
    virtual stmr::gps_coordinate at(size_t index) const = 0;
    virtual size_t expectations(std::size_t track_index) const = 0;
    virtual boost::posix_time::ptime expectation(std::size_t track_index, std::size_t time_index) const = 0;
    virtual std::string label(size_t index) const = 0;
    virtual long idx(size_t index) const = 0;
    virtual void save(const std::string& path) const = 0;
};

/**
 * @todo write docs
 */
template <typename T>
class TrackLayer: public AbstractTrackLayer{
  typedef T vertex_type;

  vertex_type     _focus;
  protected:
    typedef std::vector<vertex_type> collection_type;
    collection_type _track;
  public:
    TrackLayer(MarbleWidget* widget, TrackType type): AbstractTrackLayer(widget, type){}
    virtual bool render(GeoPainter* painter, ViewportParams* /*viewport*/, const QString& /*renderPos = "NONE"*/, GeoSceneLayer* /*layer = 0*/){
        painter->setRenderHint(QPainter::Antialiasing, true);
        painter->setPen(_vpen);
        GeoDataLineString path;
        for(typename collection_type::const_iterator it = _track.begin(); it != _track.end(); ++it){
            GeoDataCoordinates coordinate = internal::geopaint<vertex_type>::coordinate(*it);
            path.append(coordinate);
        }
        painter->setPen(_epen);
        if(_name.empty()){
            painter->drawPolyline(path);
        }else{
            painter->drawPolyline(path, QString::fromStdString(_name));
        }
        for(typename collection_type::const_iterator it = _track.begin(); it != _track.end(); ++it){
            internal::geopaint<vertex_type>::draw_point(painter, *it, _vpen);
        }
        if(!_focus.location.invalid()){
            QRect rect = internal::paint::highlightSquare(painter, _focus.location, _vpen);
            QRect area = internal::paint::highlightBubble(painter, _focus.location, _vpen, internal::geopaint<vertex_type>::height(_focus, rect.height()), rect.width()/2, internal::geopaint<vertex_type>::bubbleRight());
            internal::paint::highlightBubbleText(painter, _focus, area, _vpen);
        }
        return true;
    }
    virtual ~TrackLayer(){}
  public:
    void insert(typename collection_type::iterator it, const vertex_type& vertex){
        _track.insert(it, vertex);
    }
    void append(const vertex_type& vertex){
        _track.push_back(vertex);
    }
    virtual void clear(){
        _track.clear();
    }
    template <typename U>
    void copy(U begin, U end){
        for(U i = begin; i != end; ++i){
            append(*i);
        }
    }
    virtual size_t count() const{
        return _track.size();
    }
  public:
    vertex_type nearest(const stmr::gps_coordinate& coordinate) const{
        for(typename collection_type::const_iterator i = _track.begin(); i != _track.end(); ++i){
            double distance = stmr::distance(i->location, coordinate);
            if(distance <= 0.005){
                return *i;
            }
        }
        return vertex_type();
    }
    void setFocus(const stmr::gps_coordinate& coordinate){
        vertex_type v = nearest(coordinate);
        if(v.location != _focus.location){
            _focus = v;
            _widget->update();
        }
    }
    virtual stmr::gps_coordinate at(size_t index) const{
        return _track[index].location;
    }
    virtual std::string label(size_t index) const{
        return _track[index].label;
    }
    virtual long idx(size_t index) const{
        return _track[index].index;
    }
    collection_type collection() const{
        return _track;
    }
};

class ObservationTrackLayer: public QObject, public TrackLayer<stmr::observation_data>{
    Q_OBJECT
    public:
        typedef typename TrackLayer<stmr::observation_data>::collection_type::iterator       iterator;
        typedef typename TrackLayer<stmr::observation_data>::collection_type::const_iterator const_iterator;
        
        enum class Mode{
            Display,
            Insert,
            Alter
        };
        
        ObservationTrackLayer(MarbleWidget* widget);
        virtual size_t expectations(std::size_t track_index) const;
        virtual boost::posix_time::ptime expectation(std::size_t track_index, std::size_t index) const;
        virtual bool eventFilter(QObject* obj, QEvent *event);
        virtual void save(const std::string& path) const;
        virtual bool render(GeoPainter* painter, ViewportParams* viewport, const QString& renderPos = "NONE", GeoSceneLayer* layer = 0);
        void drawVelocityLimit(GeoPainter* painter);
    public:
        Mode mode() const;
        void setMode(Mode mode, int pivot=-1);
        stmr::observation_data pivot() const;
        bool has_pivot_next() const;
        stmr::observation_data pivot_next() const;
        void pivot_insert(const stmr::observation_data& vertex);
        void update(size_t index, const boost::posix_time::ptime& time);
        stmr::observation_data observation_at(size_t index) const;
    signals:
        void locationClicked(ObservationTrackLayer* self, double lat, double lon);
        void needsToUpdate();
    private:
        Mode     _mode;
        iterator _pivot;
        stmr::gps_coordinate _hover;
        int _vlim; // velocity limit in km/h -1 for unlimited
        
};
struct MergedTrackLayer: public QObject, public TrackLayer<stmr::merged_data>{
  Q_OBJECT
  typedef typename TrackLayer<stmr::merged_data>::collection_type collection_type;
  typedef typename collection_type::const_iterator const_iterator;
  collection_type _hull;
  std::vector<std::pair<stmr::merged_data, bool>> _intersections;
  public:
    MergedTrackLayer(MarbleWidget* widget);
    virtual size_t expectations(std::size_t track_index) const;
    virtual boost::posix_time::ptime expectation(std::size_t track_index, std::size_t index) const;
    virtual bool eventFilter(QObject* obj, QEvent *event);
    virtual void save(const std::string& path) const;
  public slots:
    void computeHull();
    void intersect(FenceLayer* fence);
  public:
    const_iterator hbegin() const{return _hull.begin();}
    const_iterator hend() const{return _hull.end();}
};

#endif // TRACKLAYER_H
