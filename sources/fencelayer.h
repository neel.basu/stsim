#ifndef FENCELAYER_H
#define FENCELAYER_H

#include <QObject>
#include "stmr.h"
#include <marble/MarbleWidget.h>
#include <marble/MarbleMap.h>
#include <marble/MarbleModel.h>
#include <marble/GeoPainter.h>
#include <marble/LayerInterface.h>
#include <boost/date_time.hpp>

class MergedTrackLayer;

using namespace Marble;

/**
 * @todo write docs
 */
class FenceLayer : public QObject,  public LayerInterface{
  typedef std::map<MergedTrackLayer*, stmr::time_interval_map_type> results_type;
    
  Q_OBJECT
  enum Mode{
    Editable,
    Sealed
  };
  Mode                      _mode;
  QColor                    _color;
  MarbleWidget*             _map;
  GeoDataCoordinates        _center;
  GeoDataCoordinates        _border;
  stmr::intersection        _hover;
  boost::posix_time::ptime  _start;
  boost::posix_time::ptime  _end;
  results_type              _results;

  public:
    typedef stmr::intersection intersection;
    typedef std::map<MergedTrackLayer*, std::vector<intersection>> intersection_map_type;
  private:
    intersection_map_type _intersections;
  public:
    FenceLayer(MarbleWidget* widget);
  public:
    QStringList renderPosition() const override;
    bool render(Marble::GeoPainter * painter, Marble::ViewportParams* /*viewport*/, const QString& /*renderPos*/, Marble::GeoSceneLayer* /*layer*/) override;
    bool eventFilter(QObject * watched, QEvent * event) override;
  private:
    void drawFence(Marble::GeoPainter* painter, GeoDataCoordinates center, double radius);
  signals:
    void sealed(FenceLayer*);
  public:
    stmr::gps_coordinate center() const;
    double radius() const;
    boost::posix_time::ptime start() const;
    boost::posix_time::ptime finish() const;
    QColor color() const;
    void setColor(const QColor& color);
  public:
    intersection_map_type::const_iterator begin() const;
    intersection_map_type::const_iterator end() const;
    void clear_intersections();
    void add_intersection(MergedTrackLayer* layer, const stmr::merged_data& m, bool entry);
    void set_intersections(MergedTrackLayer* layer, const std::vector<stmr::merged_data>& X, const std::vector<bool>& states);
    std::size_t tracks_intersected() const;
    MergedTrackLayer* intersected_track(std::size_t i) const;
    std::size_t track_intersections(std::size_t i) const;
    std::size_t track_intersections(MergedTrackLayer* layer) const;
    intersection track_intersection(MergedTrackLayer* layer, std::size_t i) const;
    intersection track_intersection(std::size_t i, std::size_t j) const;
    std::size_t track_intersection_times(MergedTrackLayer* layer, std::size_t i) const;
    std::size_t track_intersection_times(std::size_t i, std::size_t j) const;
    boost::posix_time::ptime track_intersection_time(MergedTrackLayer* layer, std::size_t i, std::size_t j) const;
    const stmr::time_interval_map_type& results(MergedTrackLayer* track) const;
};

#endif // FENCELAYER_H
