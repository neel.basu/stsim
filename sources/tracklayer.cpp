#include "tracklayer.h"
#include <QMouseEvent>
#include <QRegion>
#include <QPolygon>
#include <QDebug>
#include <boost/algorithm/string/join.hpp>
#include <boost/bind.hpp>
#include <QGeoCoordinate>
#include "fencelayer.h"

AbstractTrackLayer::AbstractTrackLayer(Marble::MarbleWidget* widget, TrackType type): _widget(widget), _type(type), _visible(false){
    setColor(Qt::red);
}

AbstractTrackLayer::~AbstractTrackLayer(){
    
}

QStringList AbstractTrackLayer::renderPosition() const{
    return QStringList() << "HOVERS_ABOVE_SURFACE";
}

void AbstractTrackLayer::setVisible(bool visible){
    if(visible != _visible){
        _visible = visible;
        if(_visible){
            _widget->addLayer(this);
        }else{
            _widget->removeLayer(this);
        }
    }
}

bool AbstractTrackLayer::visible() const{
    return _visible;
}

void AbstractTrackLayer::setName(const std::string& name){
    _name = name;
}

void AbstractTrackLayer::setVPen(const QPen& pen){
    _vpen = pen;
}

void AbstractTrackLayer::setEPen(const QPen& pen){
    _epen = pen;
}

void AbstractTrackLayer::setPen(const QPen& pen){
    setVPen(pen);
    QPen epen = pen;
    epen.setColor(pen.color().lighter());
    setEPen(epen);
}

void AbstractTrackLayer::setColor(const QColor& color){
    QPen pen(QBrush(color), 4.0, Qt::SolidLine, Qt::RoundCap);
    setPen(pen);
}

std::string AbstractTrackLayer::name() const{
    return _name;
}

TrackType AbstractTrackLayer::type() const{
    return _type;
}

QPen AbstractTrackLayer::pen() const{
    return _vpen;
}

QColor AbstractTrackLayer::color() const{
    return _vpen.color();
}

ObservationTrackLayer::Mode ObservationTrackLayer::mode() const{
    return _mode;
}

stmr::observation_data ObservationTrackLayer::pivot() const{
    return *_pivot;
}

bool ObservationTrackLayer::has_pivot_next() const{
    return _pivot+1 < _track.end();
}

stmr::observation_data ObservationTrackLayer::pivot_next() const{
    return *(_pivot+1);
}

void ObservationTrackLayer::pivot_insert(const stmr::observation_data& vertex){
    TrackLayer<stmr::observation_data>::insert(_pivot+1, vertex);
}

void ObservationTrackLayer::update(size_t index, const boost::posix_time::ptime& time){
    _track[index].time = time;
}

stmr::observation_data ObservationTrackLayer::observation_at(size_t index) const{
    return _track[index];
}

void ObservationTrackLayer::setMode(ObservationTrackLayer::Mode mode, int pivot){
    _mode = mode;
    if(_mode != Mode::Display){
        _pivot = (pivot == -1) ? (_track.end()-1) : _track.begin()+pivot;
    }
}

bool ObservationTrackLayer::render(Marble::GeoPainter* painter, Marble::ViewportParams* viewport, const QString& renderPos, GeoSceneLayer* layer){
    TrackLayer<stmr::observation_data>::render(painter, viewport, renderPos, layer);
    if(_mode != Mode::Display){
        internal::paint::highlightSquare(painter, _hover, _vpen);
        GeoDataLineString path;
        path.append(internal::geopaint<stmr::observation_data>::coordinate(*_pivot));
        path.append(GeoDataCoordinates(_hover.lng, _hover.lat, 0.0, GeoDataCoordinates::Degree));
        if(_pivot+1 < _track.end()){
            path.append(internal::geopaint<stmr::observation_data>::coordinate(*(_pivot+1)));
        }
        painter->drawPolyline(path);
        
        if(_mode == Mode::Insert && !pivot().location.invalid()){
            {
                stmr::observation_data pivotp = *(_pivot);
                QRect rect = internal::paint::highlightSquare(painter, pivotp.location, _vpen);
                QRect area = internal::paint::highlightBubble(painter, pivotp.location, _vpen, internal::geopaint<stmr::observation_data>::height(pivotp, rect.height()), rect.width()/2, internal::geopaint<stmr::observation_data>::bubbleRight());
                internal::paint::highlightBubbleText(painter, pivotp, area, _vpen);
            }
            if(has_pivot_next()){
                stmr::observation_data pivotn = *(_pivot+1);
                QRect rect = internal::paint::highlightSquare(painter, pivotn.location, _vpen);
                QRect area = internal::paint::highlightBubble(painter, pivotn.location, _vpen, internal::geopaint<stmr::observation_data>::height(pivotn, rect.height()), rect.width()/2, internal::geopaint<stmr::observation_data>::bubbleRight());
                internal::paint::highlightBubbleText(painter, pivotn, area, _vpen);
            }
        }
    }
    return true;
}


void ObservationTrackLayer::drawVelocityLimit(Marble::GeoPainter* painter){
    //{ distance from velicity limit
    double duration = 60;
    
    double distance = 0.0;
    //}
    QBrush brush = painter->brush();
    painter->drawEllipse(GeoDataCoordinates(_pivot->location.lng, _pivot->location.lat, 0.0, GeoDataCoordinates::Degree), distance, distance);
}

bool ObservationTrackLayer::eventFilter(QObject* obj, QEvent* event){
    if(!_widget->isEnabled()){
        return false;
    }
    if(event->type() == QEvent::MouseMove){
        QMouseEvent* ev = static_cast<QMouseEvent*>(event);
        QPoint pos = ev->pos();
        double lat, lng;
        bool okay = _widget->geoCoordinates(pos.x(), pos.y(), lng, lat);
        stmr::gps_coordinate focus(lat, lng);
        if(okay){
            setFocus(focus);
        }
        
        if(_mode != Mode::Display){
            _hover = focus;
            _widget->update();
        }
    }else if(event->type() == QEvent::MouseButtonDblClick && _mode != Mode::Display){
        QMouseEvent* ev = static_cast<QMouseEvent*>(event);
        QPoint pos = ev->pos();
        double lat, lng;
        bool okay = _widget->geoCoordinates(pos.x(), pos.y(), lng, lat);
        if(okay){
            emit locationClicked(this, lat, lng);
        }
    }/*else if(_mode != Mode::Display && event->type() == QEvent::KeyPress){
        QKeyEvent* ev = static_cast<QKeyEvent*>(event);
        if(ev->matches(QKeySequence::Cancel)){
            _mode = Mode::Display;
            _widget->update();
        }
    }*/
    return false;
}

bool MergedTrackLayer::eventFilter(QObject* obj, QEvent* event){
    if(event->type() == QEvent::MouseMove){
        QMouseEvent* ev = static_cast<QMouseEvent*>(event);
        QPoint pos = ev->pos();
        double lat, lng;
        bool okay = _widget->geoCoordinates(pos.x(), pos.y(), lng, lat);
        if(okay){
            setFocus(stmr::gps_coordinate(lat, lng));
        }
        return false;
    }
    return false;
}

ObservationTrackLayer::ObservationTrackLayer(Marble::MarbleWidget* widget): TrackLayer<stmr::observation_data>(widget, ObservationTrack), _mode(Mode::Display){
    setColor(Qt::blue);
}

MergedTrackLayer::MergedTrackLayer(Marble::MarbleWidget* widget): TrackLayer<stmr::merged_data>(widget, MergedTrack){
    setColor(Qt::red);
}

size_t ObservationTrackLayer::expectations(std::size_t track_index) const{
    return 1;
}

boost::posix_time::ptime ObservationTrackLayer::expectation(std::size_t track_index, std::size_t index) const{
    return _track[track_index].time;
}

size_t MergedTrackLayer::expectations(std::size_t track_index) const{
    return _track[track_index].times.size();
}

boost::posix_time::ptime MergedTrackLayer::expectation(std::size_t track_index, std::size_t index) const{
    return _track[track_index].times[index];
}

void ObservationTrackLayer::save(const std::string& path) const{
    stmr::save_json(_track, path);
}

void MergedTrackLayer::save(const std::string& path) const{
    stmr::save_json(_track, path);
}

void MergedTrackLayer::computeHull(){
    _hull = stmr::algorithms::convex_hull(collection());
}

void MergedTrackLayer::intersect(FenceLayer* fence){
    stmr::gps_coordinate center = fence->center();
    double radius = fence->radius();
    
    std::vector<bool> states;
    std::vector<stmr::merged_data> X = stmr::algorithms::intersections(center, radius, _track, states);
    fence->set_intersections(this, X, states);
}
