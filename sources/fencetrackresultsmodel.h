#ifndef FENCETRACKRESULTSMODEL_H
#define FENCETRACKRESULTSMODEL_H

#include <QDateTime>
#include <QAbstractTableModel>

class FenceLayer;
class MergedTrackLayer;

class FenceTrackResultsModel : public QAbstractTableModel{
  Q_OBJECT
  public:
    explicit FenceTrackResultsModel(FenceLayer* fence = 0x0, QObject *parent = nullptr);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    QDateTime min() const;
    QDateTime max() const;
    QDateTime lower(size_t i) const;
    QDateTime upper(size_t i) const;
    double probability(size_t i) const;
  public:
    void setTrack(MergedTrackLayer* track);
    MergedTrackLayer* track() const;
    void setFence(FenceLayer* fence);
    FenceLayer* fence() const;
  private:
    FenceLayer* _fence;
    MergedTrackLayer* _track;
};

#endif // FENCETRACKRESULTSMODEL_H
