#include "fencedialog.h"
#include "ui_fencedialog.h"

FenceDialog::FenceDialog(QWidget *parent): QDialog(parent), ui(new Ui::FenceDialog), _latEdit(new Marble::LatLonEdit(0x0, Marble::LatLonEdit::Latitude)), _lonEdit(new Marble::LatLonEdit(0x0, Marble::LatLonEdit::Longitude)){
    ui->setupUi(this);
    
    _latEdit->setDisabled(true);
    _lonEdit->setDisabled(true);
    
    ui->latLayout->addWidget(_latEdit);
    ui->lonLayout->addWidget(_lonEdit);
}

FenceDialog::~FenceDialog(){
    delete ui;
}

void FenceDialog::setLocation(const Marble::GeoDataCoordinates& coordinates){
    _latEdit->setValue(coordinates.latitude(Marble::GeoDataCoordinates::Degree));
    _lonEdit->setValue(coordinates.latitude(Marble::GeoDataCoordinates::Degree));
}

void FenceDialog::setRadius(double radius_in_meters){
    ui->radiusSpinBox->setValue(radius_in_meters);
}

QDateTime FenceDialog::start() const{
    return ui->startDateTimeEdit->dateTime();
}

QDateTime FenceDialog::end() const{
    return ui->endDateTimeEdit->dateTime();
}
