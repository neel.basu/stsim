#include <boost/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/format.hpp>
#include <vector>
#include <sstream>
#include <numeric>
#include <boost/tokenizer.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include "stmr.h"
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/prettywriter.h>
#include <boost/range/irange.hpp>
#include <boost/geometry/strategies/agnostic/hull_graham_andrew.hpp>

double stmr::internal::deg2rad(double deg) {
    return (deg * M_PI / 180);
}

double stmr::internal::rad2deg(double rad) {
    return (rad * 180 / M_PI);
}

double stmr::internal::unit_distance(double lat1d, double lon1d, double lat2d, double lon2d){
    typedef boost::geometry::model::point<
        double, 2, boost::geometry::cs::spherical_equatorial<boost::geometry::degree>
    > spherical_point;
    double dist = boost::geometry::distance(spherical_point(lon1d, lat1d), spherical_point(lon2d, lat2d));
    return dist;
}


double stmr::internal::distanceEarth(double lat1d, double lon1d, double lat2d, double lon2d) {
    return unit_distance(lat1d, lon1d, lat2d, lon2d) * earthRadiusKm;
}

double stmr::unit_distance(const gps_coordinate& l, const gps_coordinate& r){
    return internal::unit_distance(l.lat, l.lng, r.lat, r.lng);
}

double stmr::distance(const gps_coordinate& l, const gps_coordinate& r){
    return internal::distanceEarth(l.lat, l.lng, r.lat, r.lng);
}

double stmr::unit_distance(const stmr::observation_data& l, const stmr::observation_data& r){
    return stmr::unit_distance(l.location, r.location);
}

double stmr::distance(const stmr::observation_data& l, const stmr::observation_data& r){
    return stmr::distance(l.location, r.location);
}

double stmr::unit_distance(const stmr::merged_data& l, const stmr::merged_data& r){
    return stmr::unit_distance(l.location, r.location);
}

double stmr::distance(const stmr::merged_data& l, const stmr::merged_data& r){
    return stmr::distance(l.location, r.location);
}

void stmr::merged_data::reshape(int K){
    size_t pre_size = times.size();
    times.resize(K);
    std::fill(times.begin()+pre_size, times.end(), boost::posix_time::ptime());
}

stmr::merged_data stmr::observation_data::to_merged(int K) const{
    stmr::merged_data md;
    md.location = location;
    md.reshape(K);
    *(md.times.rbegin()) = time;
    md.label = label;
    md.index = index;
    return md;
}

std::vector<stmr::merged_data> stmr::algorithms::merge(const std::vector<observation_data>& observation, int K){
    std::vector<stmr::merged_data> merged;
    merged.resize(observation.size());
    std::transform(observation.begin(), observation.end(), merged.begin(), [K](const observation_data& data){
        stmr::merged_data md = data.to_merged(K);
        return md;
    });
    return merged;
}

std::vector<stmr::merged_data> stmr::algorithms::merge(const std::vector<stmr::merged_data>& merged, const std::vector<observation_data>& observation, int K, merge_callback_type callback){
    typedef std::vector<stmr::observation_data> observation_collection_type;
    typedef std::vector<stmr::merged_data> collection_type;
    
    local_view view;

    collection_type pink_track;
    {
        collection_type::const_iterator it = merged.begin();
        collection_type::const_iterator next = it;
        pink_track.push_back(*it);
        while(++next != merged.end()){
            double dst = stmr::distance(*it, *next);
            if(dst <= 0.001){
                continue;
            }
            it = next;
            pink_track.push_back(*it);
        }
    }

    std::for_each(pink_track.begin(), pink_track.end(), boost::bind(&stmr::merged_data::reshape, _1, K));
    collection_type blue_track = merge(observation, K);
    collection_type red_track;

    collection_type::iterator t = blue_track.begin();
    collection_type::iterator p = pink_track.begin();

    state_triangle initial(*p, *(p+1), *t);
    if(initial.state() != state_b){
        red_track.push_back(*p);
    }else{
        while(t != blue_track.end() && state_triangle(*p,*(p+1), *t).state() == state_b){
            red_track.push_back(*t++);
        }
        red_track.push_back(*p);
    }

    // merged the pink_track with blue_track and produce the red_track
    while(std::distance(p, pink_track.end()) > 1){
        collection_type::iterator q = p+1;
        for(;t != blue_track.end();){
            state_triangle triangle(*p, *q, *t);
            triangle_state state = triangle.state();
            skewness skew = view.current.invalid() ? skew_q : triangle.skew(view.current);
            
            bool on_track = true;
            collection_type::iterator t_next = t+1;
            if(skew == skew_v){
                if(t_next != blue_track.end()){
                    // change on_track depending on Divergence Elimination Policy
                    
                    //{ Point Distance Policy (if t_next is closer to q that t, then t is assumed to be on_track)
                    double dist_qt      = stmr::distance(*q, *t);
                    double dist_qtnext  = stmr::distance(*q, *t_next);
                    on_track = (dist_qtnext < dist_qt);
                    //}
                    
                    //{ Mirror Policy (if t_next is in the opposite side of pq line that t, then t is assumed to be on_track)
                    state_triangle tri_next(*p, *q, *t_next);
                    on_track = on_track || triangle.opposite(tri_next);
                    //}
                }else{
                    on_track = false;
                }
            }
            std::cout << "ontrack: " << on_track << std::endl;
            if(skew != skew_p || state == state_c || (!view.current.invalid() && view.current.state() == state_c)){
                view.step(triangle); // unless we step first what we are refering as previous is actually current
                
                if(state == state_a){
                    if(!on_track){
                        if(view.pstate() != state_c){
                            approximate_m(*q, view.previous.t, *t);
                        }else{
                            approximate_m(*q, view.lookback.t, *t);
                        }
                        p = q;
                        callback(view, *q, skew);
                        red_track.push_back(*q);
                        break;
                    }
                    approximate_n(*t, *p, *q);
                    callback(view, *t, skew);
                    red_track.push_back(*t++);
                }else if(state == state_b){
                    if(!on_track){
                        if(view.pstate() != state_c){
                            approximate_m(*q, view.previous.t, *t);
                        }else{
                            approximate_m(*q, view.lookback.t, *t);
                        }
                        p = q;
                        callback(view, *q, skew);
                        red_track.push_back(*q);
                        break;
                    }
                    if(view.pstate() == state_b){
                        approximate_n(*t, view.lookback.p, *q);
                    }else if(view.pstate() == state_c){
                        approximate_n(*t, view.previous.p, *q);
                    }else if(view.pstate() == state_null && view.previous.invalid()){
                        // NOOP
                    }else{
                        double dtt = stmr::distance(*(t-1), *t);
                        double dpt = stmr::distance(*p, *t);
                        if(dtt <= 0.001){
                            t->times = (t-1)->times;
                        }else if(dpt <= 0.001){
                            t->times = p->times;
                        }else{
                            // TODO Must have recovered from a backgear or U-Turn
//                             throw std::runtime_error((boost::format("encountered unexpected pstate %1%") % view.pstate()).str());
                        }
                    }
                    callback(view, *t, skew);
                    red_track.push_back(*t++);
                }else if(state == state_c){
                    if(view.pstate() == state_b){
                        if(p == pink_track.begin()){
                            approximate_m(*p, view.previous.t, *t);
                            callback(view, *p, skew);
                            red_track.push_back(*p);
                        }
                    }
                    if(view.pstate() != state_c){
                        approximate_m(*q, view.previous.t, *t);
                    }else{
                        approximate_m(*q, view.lookback.t, *t);
                    }
                    p = q;
                    callback(view, *q, skew);
                    red_track.push_back(*q);
                    break;
                }else{
                    throw std::runtime_error("encountered null state");
                }
            }else if(t+1 == blue_track.end() && q+1 == pink_track.end()){
                state_triangle last_triangle(*p, *q, red_track.back());
                if(triangle.opposite(last_triangle)){
                    p = q;
                    callback(view, *q, skew);
                    red_track.push_back(*q);
                }else{
                    callback(view, *t, skew);
                    red_track.push_back(*t++);
                }
                break;
            }else if(skew == skew_p){
                collection_type::iterator t_dashed = t;
                state_triangle last = view.current;
                while(t_dashed != blue_track.end()){// backgear
                    state_triangle escape_triangle(*p, *q, *t_dashed);
                    if (escape_triangle.skew(last) != skew_p){
                        break;
                    }
                    last = escape_triangle;
                    ++t_dashed;
                }
                t = t_dashed;
            }
        }
        //{ blue track ended
        if(t == blue_track.end()){
            p = q;
            red_track.push_back(*q);
        }
        //}
    }
    //{ pink track ended
    while(t < blue_track.end()){
        red_track.push_back(*t++);
    }
    //}

    return red_track;
}

void stmr::algorithms::noop(const stmr::local_view&, const stmr::merged_data&, stmr::skewness skew){}


void stmr::state_triangle::set(const stmr::merged_data& p_, const stmr::merged_data& q_, const stmr::merged_data& t_){
    p = p_;
    q = q_;
    t = t_;
}

double stmr::state_triangle::projected_unit_distance() const{
    typedef boost::geometry::model::point<
        double, 2, boost::geometry::cs::spherical_equatorial<boost::geometry::degree>
    > spherical_point;
    typedef boost::geometry::model::segment<spherical_point> spherical_segment;
    
    spherical_point p_(p.location.lng, p.location.lat);
    spherical_point q_(q.location.lng, p.location.lat);
    spherical_point t_(t.location.lng, t.location.lat);
    
    spherical_segment pq_(p_, q_);
    
    double dist = boost::geometry::distance(t_, pq_);
    
    return dist;
}

bool stmr::state_triangle::is_vskew() const{
    return vdistance() > 0.008;
}

stmr::skewness stmr::state_triangle::skew(const state_triangle& other) const{
    skewness sq;

    if(is_vskew()){
        sq = skew_v;
    }else{
        if(other.state() != state_c){
            if (cos_tpq_spherical() < other.cos_tpq_spherical() && cos_tqp_spherical() > other.cos_tqp_spherical()){
                sq = skew_p;
            }else if(cos_tpq_spherical() > other.cos_tpq_spherical() && cos_tqp_spherical() < other.cos_tqp_spherical()){
                sq = skew_q;
            }else{
                double pt_dashed = std::acos(cos_pt()/std::cos(vdistance()));
                double pt_dashed_other = std::acos(other.cos_pt()/std::cos(other.vdistance()));
                if(pt_dashed < pt_dashed_other){
                    sq = skew_p;
                }else{
                    sq = skew_q;
                }
            }
        }else{
            sq = skew_null;
        }
    }

    return sq;
}

double stmr::state_triangle::perpd() const{
    double a;
    double b;
    double c;
    double d;

    //x->lng
    //y->lat
    a = (q.location.lat - p.location.lat);
    b = (p.location.lng - q.location.lng);
    c = ((q.location.lng*p.location.lat) - (p.location.lng*q.location.lat));
    d = (abs((a*t.location.lng) + (b*t.location.lat) + c )/(sqrt(pow(a,2)+pow(b,2))));

    return d;

    //     return internal::vdistance(p.location, q.location, t.location);
}

double stmr::state_triangle::vdistance() const{
    typedef boost::geometry::model::point<
        double, 2, boost::geometry::cs::spherical_equatorial<boost::geometry::degree>
    > geo_point;
    typedef boost::geometry::model::segment<geo_point> geo_segment;

    geo_point p_(p.location.lat, p.location.lng);
    geo_point q_(q.location.lat, q.location.lng);
    geo_point t_(t.location.lat, t.location.lng);

    geo_segment line(p_, q_);
    double perp_dist = boost::geometry::distance(t_, line);
    return perp_dist * earthRadiusKm;
}

bool stmr::state_triangle::opposite(const state_triangle& other) const{
    typedef boost::geometry::model::point<
        double, 2, boost::geometry::cs::spherical_equatorial<boost::geometry::degree>
    > geo_point;
    typedef boost::geometry::model::polygon<geo_point> geo_polygon;
    
    geo_polygon self_triangle, other_triangle;
    
    boost::geometry::append(self_triangle.outer(), geo_point(p.location.lng, p.location.lat));
    boost::geometry::append(self_triangle.outer(), geo_point(q.location.lng, q.location.lat));
    boost::geometry::append(self_triangle.outer(), geo_point(t.location.lng, t.location.lat));
    boost::geometry::append(self_triangle.outer(), geo_point(p.location.lng, p.location.lat));
    
    boost::geometry::append(other_triangle.outer(), geo_point(other.p.location.lng, other.p.location.lat));
    boost::geometry::append(other_triangle.outer(), geo_point(other.q.location.lng, other.q.location.lat));
    boost::geometry::append(other_triangle.outer(), geo_point(other.t.location.lng, other.t.location.lat));
    boost::geometry::append(other_triangle.outer(), geo_point(other.p.location.lng, other.p.location.lat));
    
    std::list<geo_polygon> output;
    boost::geometry::sym_difference(self_triangle, other_triangle, output);

    return output.size() == 0;
}

double stmr::state_triangle::cos_tpq_spherical() const{
    double pq = unit_distance(p, q);
    double pt = unit_distance(p, t);
    double qt = unit_distance(q, t);

    double cos_theta = (cos(qt)-cos(pt)*cos(pq))/(sin(pt)*sin(pq));
    return cos_theta;
}

double stmr::state_triangle::cos_tqp_spherical() const{
    double pq = unit_distance(p, q);
    double pt = unit_distance(p, t);
    double qt = unit_distance(q, t);

    double cos_phi = (cos(pt)-cos(qt)*cos(pq))/(sin(qt)*sin(pq));
    return cos_phi;
}

stmr::triangle_state stmr::state_triangle::state() const{
    if(invalid()){
        return state_null;
    }

    double pq = distance(p, q);
    double pt = distance(p, t);
    double qt = distance(q, t);

    double pq_squared = std::pow(pq, 2);
    double pt_squared = std::pow(pt, 2);
    double qt_squared = std::pow(qt, 2);

    if(pt_squared + pq_squared >= qt_squared && pq_squared + qt_squared >= pt_squared){
        return state_a;
    }else if(pq_squared + qt_squared > pt_squared){
        return state_b;
    }else if(pt_squared + pq_squared > qt_squared){
        return state_c;
    }
    std::clog << "NOT MATCHING ANY OF THE STATES" << std::endl;
    return state_null;
}

stmr::triangle_state stmr::local_view::cstate() const{
    return current.state();
}

stmr::triangle_state stmr::local_view::pstate() const{
    return previous.state();
}

stmr::triangle_state stmr::local_view::lstate() const{
    return lookback.state();
}

bool stmr::local_view::match_state(stmr::triangle_state lstate, stmr::triangle_state pstate){
    stmr::triangle_state vlstate, vpstate, vcstate;
    vlstate = lookback.state();
    vpstate = previous.state();

    if(lstate != state_null && lstate != vlstate) return false;
    if(pstate != state_null && pstate != vpstate) return false;

    return true;
}

void stmr::local_view::step(){
    if(current.state() != previous.state()){
        lookback = previous;
    }
    previous = current;
}

void stmr::local_view::step(const stmr::state_triangle& c){
    step();
    current = c;
}

void stmr::algorithms::approximate_m(stmr::merged_data& m, const stmr::merged_data& s, const stmr::merged_data& t){
    double ms = distance(m.location, s.location);
    double mt = distance(m.location, t.location);
    double r = ms/(ms+mt);
    int K = t.times.size()-1;

    if(s.invalid() || t.invalid()){
        m.times[K] = boost::posix_time::ptime();
    }else{
        boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
        boost::posix_time::time_duration::sec_type tstamp = (t.times[K] - epoch).total_seconds();
        boost::posix_time::time_duration::sec_type sstamp = (s.times[K] - epoch).total_seconds();

        boost::posix_time::time_duration::sec_type mstamp = sstamp + r*(tstamp-sstamp);

        m.times[K] = boost::posix_time::from_time_t(mstamp);
    }
}

void stmr::algorithms::approximate_n(stmr::merged_data& t, const stmr::merged_data& p, const stmr::merged_data& q){
    double pt = distance(p.location, t.location);
    double qt = distance(q.location, t.location);
    double r = pt/(pt+qt);

    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

    for(int i =0; i < t.times.size()-1; ++i){
        if(p.invalid() || q.invalid()){
            t.times[i] = boost::posix_time::ptime();
        }else{
            boost::posix_time::time_duration::sec_type qstamp = (q.times[i] - epoch).total_seconds();
            boost::posix_time::time_duration::sec_type pstamp = (p.times[i] - epoch).total_seconds();
            boost::posix_time::time_duration::sec_type tstamp = pstamp + r*(qstamp-pstamp);

            t.times[i] = boost::posix_time::from_time_t(tstamp);
        }
    }
}

std::vector<stmr::observation_data> stmr::parse_csv(const boost::filesystem::path& path, const std::string& label){
    std::ifstream in(path.c_str());
    if (!in.is_open()){
        throw std::ios_base::failure((boost::format("failed to open file %1%") % path).str());
    };

    std::vector<stmr::observation_data> parsed;

    int index = 0;
    typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer_type;
    std::string line;
    while(std::getline(in, line)){
        tokenizer_type tok(line);
        std::vector<std::string> vec;
        vec.assign(tok.begin(), tok.end());
        if(vec.size() < 3) continue;

        stmr::observation_data data;
        data.label = label;
        data.location = stmr::gps_coordinate(boost::lexical_cast<double>(vec[1]), boost::lexical_cast<double>(vec[0]));
        std::string time_str = vec[2].substr(0, vec[2].size()-3);
        data.time = boost::posix_time::time_from_string(time_str);
        data.index = index++;
        parsed.push_back(data);
    }

    return parsed;
}

std::vector<stmr::observation_data> stmr::parse_json(const boost::filesystem::path& path, const std::string& label){
    std::ifstream in(path.c_str());
    if (!in.is_open()){
        throw std::ios_base::failure((boost::format("failed to open file %1%") % path).str());
    };

    std::vector<stmr::observation_data> parsed;

    rapidjson::IStreamWrapper stream(in);
    rapidjson::Document doc;
    doc.ParseStream(stream);

    const rapidjson::Value& features = doc["features"];
    int index = 0;
    for (const rapidjson::Value& feature : features.GetArray()){
        const rapidjson::Value& coordinates = feature["geometry"]["coordinates"];
        auto coordinates_array = coordinates.GetArray();
        double lng = coordinates_array[0].GetDouble();
        double lat = coordinates_array[1].GetDouble();
        const rapidjson::Value& time = feature["properties"]["when"];
        boost::posix_time::ptime t = boost::posix_time::time_from_string(time.GetString());
        stmr::observation_data data;
        data.label = label;
        data.location = stmr::gps_coordinate(lat, lng);
        data.time = t;
        data.index = index++;
        parsed.push_back(data);
    }

    return parsed;
}

std::vector<stmr::merged_data> stmr::parse_json_merged(const boost::filesystem::path& path, const std::string& label){
    std::ifstream in(path.c_str());
    if (!in.is_open()){
        throw std::ios_base::failure((boost::format("failed to open file %1%") % path).str());
    };

    std::vector<stmr::merged_data> parsed;

    rapidjson::IStreamWrapper stream(in);
    rapidjson::Document doc;
    doc.ParseStream(stream);

    const rapidjson::Value& features = doc["features"];
    int index = 0;
    for (const rapidjson::Value& feature : features.GetArray()){
        std::string type = feature["geometry"]["type"].GetString();
        if(type != "Point"){
            continue;
        }
        const rapidjson::Value& coordinates = feature["geometry"]["coordinates"];
        auto coordinates_array = coordinates.GetArray();

        double lng = coordinates_array[0].GetDouble();
        double lat = coordinates_array[1].GetDouble();
        auto times = feature["properties"]["times"].GetArray();

        stmr::merged_data data;
        data.label = label;
        data.location = stmr::gps_coordinate(lat, lng);

        for(int i = 0; i < times.Size(); ++i){
            std::string time_str = times[i].GetString();
            try{
                data.times.push_back(boost::posix_time::time_from_string(time_str));
            }catch(...){
                data.times.push_back(boost::posix_time::ptime());
            }
        }

        data.index = index++;
        parsed.push_back(data);
    }

    return parsed;
}

std::string stmr::save_csv(const std::vector<merged_data>& merged){
    std::stringstream stream;
    for(const merged_data& d: merged){
        stream << d.label << ", " << d.index << ", " << d.location.lat << ", " << d.location.lng << ", ";
        for(auto i=d.times.begin(); i != d.times.end(); ++i){
            if(i != d.times.begin()){
                stream << ", ";
            }
            stream << *i;
        }
        stream << std::endl;
    }
    return stream.str();
}


std::string stmr::save_json(const std::vector<merged_data>& merged){
    rapidjson::Document doc;
    rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

    doc.SetObject();
    doc.AddMember("type", "FeatureCollection", allocator);
    doc.AddMember("name", "merged", allocator);

    rapidjson::Value features(rapidjson::kArrayType);
    features.SetArray();

    rapidjson::Value path;
    path.SetArray();

    for(const merged_data& d: merged){
        rapidjson::Value feature;
        feature.SetObject();
        feature.AddMember("type", "Feature", allocator);
        rapidjson::Value properties = rapidjson::Value();
        rapidjson::Value geometry = rapidjson::Value();
        geometry.SetObject();
        geometry.AddMember("type", "Point", allocator);
        rapidjson::Value coordinates = rapidjson::Value();
        rapidjson::Value vertex = rapidjson::Value();
        coordinates.SetArray();
        vertex.SetArray();
        coordinates.PushBack(d.location.lng, allocator);
        coordinates.PushBack(d.location.lat, allocator);
        vertex.PushBack(d.location.lng, allocator);
        vertex.PushBack(d.location.lat, allocator);
        path.PushBack(vertex, allocator);
        geometry.AddMember("coordinates", coordinates, allocator);
        properties.SetObject();
        rapidjson::Value times = rapidjson::Value();
        times.SetArray();
        feature.AddMember("geometry", geometry, allocator);
        for(auto t: d.times){
            rapidjson::Value time_jstr;
            std::string time_str;
            try{
                time_str = boost::posix_time::to_simple_string(t);
            }catch(...){
                time_str = "---";
            }
            time_jstr.SetString(time_str.c_str(), time_str.length(), allocator);
            times.PushBack(time_jstr, allocator);
        }
        rapidjson::Value label_str;
        label_str.SetString(d.label.c_str(), d.label.length(), allocator);
        properties.AddMember("label", label_str, allocator);
        properties.AddMember("index", d.index, allocator);
        properties.AddMember("times", times, allocator);
        feature.AddMember("properties", properties, allocator);
        features.PushBack(feature, allocator);
    }
    rapidjson::Value geometry;
    geometry.SetObject();
    geometry.AddMember("type", "LineString", allocator);
    geometry.AddMember("coordinates", path, allocator);
    rapidjson::Value feature;
    feature.SetObject();
    feature.AddMember("type", "Feature", allocator);
    rapidjson::Value props;
    props.SetObject();
    feature.AddMember("properties", props, allocator);
    feature.AddMember("geometry", geometry, allocator);
    features.PushBack(feature, allocator);
    doc.AddMember("features", features, allocator);

    rapidjson::StringBuffer strbuf;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(strbuf);
    doc.Accept(writer);

    std::string buff(strbuf.GetString());
    return buff;
}

std::string stmr::save_json(const std::vector<observation_data>& observation){
    rapidjson::Document doc;
    rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

    doc.SetObject();
    doc.AddMember("type", "FeatureCollection", allocator);
    doc.AddMember("name", "track", allocator);
    rapidjson::Value features(rapidjson::kArrayType);
    features.SetArray();
    for(const observation_data& d: observation){
        rapidjson::Value feature;
        feature.SetObject();
        feature.AddMember("type", "Feature", allocator);
        rapidjson::Value properties = rapidjson::Value();
        rapidjson::Value geometry = rapidjson::Value();
        geometry.SetObject();
        geometry.AddMember("type", "Point", allocator);
        rapidjson::Value coordinates = rapidjson::Value();
        coordinates.SetArray();
        coordinates.PushBack(d.location.lng, allocator);
        coordinates.PushBack(d.location.lat, allocator);
        geometry.AddMember("coordinates", coordinates, allocator);
        properties.SetObject();
        rapidjson::Value label_str;
        label_str.SetString(d.label.c_str(), d.label.length(), allocator);
        properties.AddMember("label", label_str, allocator);
        properties.AddMember("index", d.index, allocator);
        rapidjson::Value time_jstr;
        std::string time_str = boost::posix_time::to_simple_string(d.time);
        time_jstr.SetString(time_str.c_str(), time_str.length(), allocator);
        properties.AddMember("when", time_jstr, allocator);
        feature.AddMember("properties", properties, allocator);
        feature.AddMember("geometry", geometry, allocator);

        features.PushBack(feature, allocator);
    }
    doc.AddMember("features", features, allocator);

    rapidjson::StringBuffer strbuf;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(strbuf);
    doc.Accept(writer);

    std::string buff(strbuf.GetString());
    return buff;
}

void stmr::save_json(const std::vector<merged_data>& merged, const boost::filesystem::path& path){
    std::ofstream out(path.c_str());
    if (!out.is_open()){
        throw std::ios_base::failure((boost::format("failed to open file %1%") % path).str());
    };
    out << save_json(merged);
}

void stmr::save_json(const std::vector<observation_data>& observation, const boost::filesystem::path& path){
    std::ofstream out(path.c_str());
    if (!out.is_open()){
        throw std::ios_base::failure((boost::format("failed to open file %1%") % path).str());
    };
    out << save_json(observation);
}

void stmr::save_csv(const std::vector<merged_data>& merged, const boost::filesystem::path& path){
    std::ofstream out(path.c_str());
    if (!out.is_open()){
        throw std::ios_base::failure((boost::format("failed to open file %1%") % path).str());
    };
    out << save_csv(merged);
}

std::ostream& stmr::operator<<(std::ostream& os, const stmr::gps_coordinate& obj){
    os << boost::format("(%1%, %2%)") % obj.lat % obj.lng;
    return os;
}

std::ostream& stmr::operator<<(std::ostream& os, const stmr::observation_data& obj){
    os << boost::format("%1% *%2% |%3%/%4%") % obj.location % obj.time % obj.label % obj.index;
    return os;
}

std::ostream & stmr::operator<<(std::ostream& os, const stmr::merged_data& obj){
    os << obj.location;
    os << " [";
    for(auto t: obj.times){
        os << t << ", ";
    }
    os << "] ";
    os << boost::format("|%1%/%2%") % obj.label % obj.index;
    return os;
}

std::ostream & stmr::operator<<(std::ostream& os, const stmr::state_triangle& obj){
    os << boost::format("p: %1% q: %2% t: %3% state: %4%") % obj.p % obj.q % obj.t % obj.state();
    return os;
}

std::ostream & stmr::operator<<(std::ostream& os, const stmr::local_view& obj){
    os << boost::format("current: %1%; previous: %2%; lookback: %3%") % obj.current % obj.previous % obj.lookback;
    return os;
}

std::vector<stmr::merged_data> stmr::algorithms::simplify(const std::vector<stmr::merged_data>& complicated, double th){
    std::vector<stmr::merged_data> M;
    auto i = complicated.cbegin();
    auto p = *i++;
    auto r = *i++;
    M.push_back(p);
    for(; i!= complicated.end(); ++i){
        const merged_data& q = *i;
        state_triangle triangle(p,q,r);
        if(triangle.vdistance() > th){
            M.push_back(r);
            p = r;
        }
        r = q;
    }
    M.push_back(r);
    return M;
}

std::vector<stmr::merged_data> stmr::algorithms::simplify_rdp(const std::vector<stmr::merged_data>& complicated, double th){
    boost::geometry::model::linestring<stmr::merged_data> complicated_polyline, simplified_polyline;
    for(auto m: complicated){
        boost::geometry::append(complicated_polyline, m);
    }
    boost::geometry::simplify(complicated_polyline, simplified_polyline, th/earthRadiusKm);
    std::vector<stmr::merged_data> simplified;
    for(auto p: simplified_polyline){
        simplified.push_back(p);
    }
    return simplified;
}

std::vector<stmr::merged_data> stmr::algorithms::convex_hull(const std::vector<stmr::merged_data>& points){
    boost::geometry::model::linestring<stmr::merged_data> points_polyline, hull_polyline;
    for(auto m: points){
        boost::geometry::append(points_polyline, m);
    }
    boost::geometry::convex_hull(points_polyline, hull_polyline, boost::geometry::strategy::convex_hull::graham_andrew<boost::geometry::model::linestring<stmr::merged_data>, stmr::merged_data>());
    std::vector<stmr::merged_data> hull;
    for(auto p: hull_polyline){
        hull.push_back(p);
    }
    return hull;
}

std::pair<bool, bool> stmr::algorithms::intersection(const stmr::gps_coordinate& center, double radius, const stmr::gps_coordinate& p, const stmr::gps_coordinate& q, stmr::gps_coordinate& x, stmr::gps_coordinate& y){
    typedef detail::geovector<double> geovec;
    
    std::pair<bool, bool> res = std::make_pair(false, false);
    
    geovec vc(center), vp(p), vq(q);
    geovec opq = detail::cross(vp, vq);
    geovec otc = detail::cross(opq, vc);
    geovec vt  = detail::cross(opq, otc);
    
    double dpq = detail::geodistance(vp, vq);
    double dcp = detail::geodistance(vc, vp);
    double dcq = detail::geodistance(vc, vq);
    double dct = detail::geodistance(vc, vt);
    
    boost::geometry::model::segment<stmr::gps_coordinate> segment(p, q);
    double dcs = boost::geometry::distance(center, segment) * earthRadiusKm; // shortest distance
    
    bool intersection_xy = (dcp >= radius && dcq >= radius && dcs <= radius);
    bool intersection_y  = (dcp <  radius && dcq >= radius && dcs <= radius);
    bool intersection_x  = (dcp >= radius && dcq <  radius && dcs <= radius);
    
    if(intersection_xy || intersection_x){
        double dpt = detail::geodistance(vp, vt);
        double dxt = std::acos(std::cos(radius)/std::cos(dct));
        
        // http://www.movable-type.co.uk/scripts/latlong.html Intermediate point
        double f     = (dpt - dxt)/dpq;
        double delta = dpq/earthRadiusKm;
    
        double a = std::sin((1.0-f)*delta)/std::sin(delta);
        double b = std::sin(f*delta)/std::sin(delta);
        
        double u = a * std::cos(internal::deg2rad(p.lat)) * std::cos(internal::deg2rad(p.lng)) + b * std::cos(internal::deg2rad(q.lat)) * std::cos(internal::deg2rad(q.lng));
        double v = a * std::cos(internal::deg2rad(p.lat)) * std::sin(internal::deg2rad(p.lng)) + b * std::cos(internal::deg2rad(q.lat)) * std::sin(internal::deg2rad(q.lng));
        double w = a * std::sin(internal::deg2rad(p.lat)) + b * std::sin(internal::deg2rad(q.lat));
        
        double lat = std::atan2(w, std::sqrt(u * u + v * v));
        double lng = std::atan2(v, u);
        
        x = gps_coordinate(internal::rad2deg(lat), internal::rad2deg(lng));
        std::cout << "x: " << x << std::endl;
        res.first = true;
    }
    
    if(intersection_xy || intersection_y){
        double dqt = detail::geodistance(vq, vt);
        double dyt = std::acos(std::cos(radius)/std::cos(dct));
        
        // http://www.movable-type.co.uk/scripts/latlong.html Intermediate point
        double f     = (dqt - dyt)/dpq;
        double delta = dpq/earthRadiusKm;

        double a = std::sin((1.0-f)*delta)/std::sin(delta);
        double b = std::sin(f*delta)/std::sin(delta);
              
        double u = b * std::cos(internal::deg2rad(p.lat)) * std::cos(internal::deg2rad(p.lng)) + a * std::cos(internal::deg2rad(q.lat)) * std::cos(internal::deg2rad(q.lng));
        double v = b * std::cos(internal::deg2rad(p.lat)) * std::sin(internal::deg2rad(p.lng)) + a * std::cos(internal::deg2rad(q.lat)) * std::sin(internal::deg2rad(q.lng));
        double w = b * std::sin(internal::deg2rad(p.lat)) + a * std::sin(internal::deg2rad(q.lat));
        
        double lat = std::atan2(w, std::sqrt(u * u + v * v));
        double lng = std::atan2(v, u);
        
        y = gps_coordinate(internal::rad2deg(lat), internal::rad2deg(lng));
        std::cout << "y: " << y << std::endl;
        res.second = true;
    }
    
    return res;
}

std::vector<stmr::merged_data> stmr::algorithms::intersections(const stmr::gps_coordinate& center, double radius, const std::vector<merged_data>& merged, std::vector<bool>& states){
    std::vector<stmr::merged_data> X;
    for(auto i = merged.begin()+1; i != merged.end(); ++i){
        const merged_data& p = *(i-1);
        const merged_data& q = *i;
        
        gps_coordinate x, y;
        std::pair<bool, bool> state_pair = stmr::algorithms::intersection(center, radius, p.location, q.location, x, y);
        
        if(state_pair.first){
            states.push_back(true);
            stmr::merged_data m;
            m.location = x;
            m.reshape(p.times.size());
            std::fill(m.times.begin(), m.times.end(), boost::posix_time::ptime());
            // TODO p, q might have invalid not a date time
            approximate_m(m, p, q);
            approximate_n(m, p, q);

            for(unsigned i = 0; i < m.times.size(); ++i){
                if(!state_pair.second && !q.times[i].is_not_a_date_time() && m.times[i].is_not_a_date_time()){
                    m.times[i] = q.times[i];
                }
            }

            X.push_back(m);
        }
        
        if(state_pair.second){
            states.push_back(false);
            stmr::merged_data m;
            m.location = y;
            m.reshape(p.times.size());
            std::fill(m.times.begin(), m.times.end(), boost::posix_time::ptime());
            // TODO p, q might have invalid not a date time
            approximate_m(m, p, q);
            approximate_n(m, p, q);

            for(unsigned i = 0; i < m.times.size(); ++i){
                if(!state_pair.first && !q.times[i].is_not_a_date_time() && m.times[i].is_not_a_date_time()){
                    m.times[i] = p.times[i];
                }
            }

            X.push_back(m);
        }
    }
    
    return X;
}


std::vector<unsigned long> stmr::algorithms::forward_difference(time_list::const_iterator begin, time_list::const_iterator end){
    std::vector<unsigned long> D;
    for(auto it = begin+1; it != end; ++it){
        D.push_back((*it - *(it-1)).total_seconds());
    }
    return D;
}

double stmr::algorithms::mse(time_list::const_iterator begin, time_list::const_iterator end, unsigned int period){
    std::vector<unsigned long> D = forward_difference(begin, end);
    unsigned size = std::distance(begin, end);
    if(period > size/2) return std::numeric_limits<double>::infinity();
    
    double se = 0.0;
    for(auto it = D.begin()+period; it != D.end(); ++it){
        se += std::pow(*it - *(it-period), 2);
    }
    double count = size-period;
    return se/count;
}

unsigned stmr::algorithms::periodicity(time_list::const_iterator begin, time_list::const_iterator end){
    unsigned size = std::distance(begin, end);
    double min_mse = std::numeric_limits<double>::max();
    unsigned periodicity = 0;
    for(unsigned i = 1; i <= size/2; ++i){
        double cmse = mse(begin, end, i);
        if(cmse < min_mse){
            periodicity = i;
            min_mse = cmse;
        }
    }
    return periodicity;
}

unsigned long stmr::algorithms::period_size(time_list::const_iterator begin, time_list::const_iterator end, unsigned int period){
    if(period == 0){
        period = periodicity(begin, end);
    }
    
    std::vector<unsigned long> D = forward_difference(begin, end);
    std::vector<unsigned long> distribution;
    for(auto it = D.begin(); it != D.begin()+D.size()-period; ++it){
        distribution.push_back(std::accumulate(it, it+period, 0));
    }
    return std::accumulate(distribution.begin(), distribution.end(), 0.0)/distribution.size();
}

unsigned stmr::algorithms::extrapolate_instance(time_list::const_iterator begin, time_list::const_iterator end, const boost::posix_time::ptime& time, unsigned int periodicity, unsigned long period_size){
    if(periodicity == 0){
        periodicity = stmr::algorithms::periodicity(begin, end);
    }
    
    if(period_size == 0){
        period_size = stmr::algorithms::period_size(begin, end, periodicity);
    }
    
    unsigned ncycles = std::floor((time - *begin).total_seconds()/period_size);
    unsigned residue = (time - *begin).total_seconds() % period_size;
    
    std::vector<unsigned long> D = forward_difference(begin, end);
    unsigned tau = 0;
    unsigned long sum = 0;
    for(auto it = D.cbegin(); it != D.cend(); ++it){
        sum += *it;
        if(sum > residue){
            tau = std::distance(D.cbegin(), it);
            break;
        }
    }
    return ncycles * periodicity + tau +1;
}

std::pair<unsigned int, unsigned int> stmr::algorithms::bounding_instances(time_list::const_iterator begin, time_list::const_iterator end, const boost::posix_time::ptime& start, const boost::posix_time::ptime& finish, unsigned int periodicity, unsigned long period_size){
    unsigned eta_s = extrapolate_instance(begin, end, start, periodicity, period_size);
    unsigned eta_e = extrapolate_instance(begin, end, finish, periodicity, period_size) +1;
    return std::make_pair(eta_s, eta_e);
}

boost::posix_time::ptime stmr::algorithms::extrapolate_time(time_list::const_iterator begin, time_list::const_iterator end, unsigned int n, unsigned int periodicity, unsigned long period_size){
    if(periodicity == 0){
        periodicity = stmr::algorithms::periodicity(begin, end);
    }
    
    if(period_size == 0){
        period_size = stmr::algorithms::period_size(begin, end, periodicity);
    }
    
    unsigned ncycles = std::floor((n - 1)/periodicity);
    unsigned residue = (n - 1) % periodicity;
    
    std::vector<unsigned long> D = forward_difference(begin, end);
    unsigned long sum = 0;
    for(auto it = D.cbegin(); it != D.begin()+residue; ++it){
        sum += *it;
    }
    
    return *begin + boost::posix_time::seconds(ncycles * period_size + sum);
}

std::vector<unsigned int> stmr::algorithms::related_instances(unsigned int n, unsigned int periodicity, unsigned int N){
    std::vector<unsigned> observations;
    auto range = boost::irange((unsigned)0, N);
    std::transform(range.begin(), range.end(), std::back_inserter(observations), [periodicity, n](unsigned i){
        return (i * periodicity) + ((n-1) % periodicity) +1;
    });
    return observations;
}

void stmr::algorithms::calculate_temporal_detail(stmr::intersection& x){
    x._periodicity = stmr::algorithms::periodicity(x._point.times.begin(), x._point.times.end());
    x._period_size = stmr::algorithms::period_size(x._point.times.begin(), x._point.times.end(), x._periodicity);
    std::tie(x._observation_start, x._observation_end) = bounding_instances(x._point.times.begin(), x._point.times.end(), x._request_start, x._request_end, x._periodicity, x._period_size);
    x._service_start = extrapolate_time(x._point.times.begin(), x._point.times.end(), x._observation_start, x._periodicity, x._period_size);
    x._service_end   = extrapolate_time(x._point.times.begin(), x._point.times.end(), x._observation_end,   x._periodicity, x._period_size);
}

std::vector<boost::posix_time::time_duration> stmr::algorithms::delta_distribution_to_vector(const stmr::intersection::delta_distribution_type& distribution){
    std::vector<boost::posix_time::time_duration> durations;
    durations.push_back(boost::get<0>(distribution));
    durations.push_back(boost::get<1>(distribution));
    durations.push_back(boost::get<2>(distribution));
    return durations;
}

std::vector<boost::posix_time::time_duration> stmr::algorithms::delta_distribution_to_vector(const stmr::intersection& x, unsigned n){
    return delta_distribution_to_vector(x._delta_distribution_map.at(n));
}

std::vector<boost::posix_time::time_duration> stmr::algorithms::piecewise_delta_distribution(const stmr::intersection& x, const stmr::intersection& y, unsigned int n){
    std::vector<boost::posix_time::time_duration> D;
    std::vector<boost::posix_time::time_duration> durations_x = delta_distribution_to_vector(x, n);
    std::vector<boost::posix_time::time_duration> durations_y = delta_distribution_to_vector(y, n);
    std::copy(durations_x.cbegin(), durations_x.cend(), std::back_inserter(D));
    std::copy(durations_y.cbegin(), durations_y.cend(), std::back_inserter(D));
    std::sort(D.begin(), D.end());
    return D;
}

std::vector<boost::posix_time::time_duration> stmr::algorithms::combined_delta_distribution(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, unsigned int n){
    std::vector<boost::posix_time::time_duration> D;
    for(std::vector<stmr::intersection>::const_iterator xit = begin; xit < std::prev(end); xit += 2){
        std::vector<boost::posix_time::time_duration> Dx = piecewise_delta_distribution(*xit, *(xit+1), n);
        std::copy(Dx.cbegin(), Dx.cend(), std::back_inserter(D));
    }
    std::sort(D.begin(), D.end());
    return D;
}

std::vector<boost::posix_time::ptime> stmr::algorithms::observation_start_times(const std::vector<stmr::merged_data>& merged){
    unsigned nobservations = merged[0].times.size();
    std::vector<stmr::merged_data>::const_iterator it = merged.begin();
    std::vector<boost::posix_time::ptime> times;
    times.resize(nobservations);
    
    for(unsigned i = 0; i < nobservations; ++i){
        auto mit = it;
        while((mit++)->times[i].is_not_a_date_time()){
            continue;
        }
        times[i] = mit->times[i];
        continue;
    }
    
    return times;
}

std::vector<unsigned int> stmr::algorithms::instances_within_fence(const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, const std::vector<boost::posix_time::time_duration>& delta, const std::vector<boost::posix_time::time_duration>& delta_next, const std::vector<unsigned int>& observations){
    std::vector<unsigned int> res;
    for(unsigned i: observations){
        if(delta[i-1] <= b && delta_next[i-1] >= b){
            res.push_back(i);
        }
    }
    return res;
}

double stmr::algorithms::favourability(const stmr::intersection& x, const stmr::intersection& y, const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, unsigned int j){
    if(x._point.times[j-1].is_not_a_date_time()) return 0.0;
    if(y._point.times[j-1].is_not_a_date_time()) return 0.0;

    boost::posix_time::time_duration delta_x_j = x._point.times[j-1] - x._observed_start_times[j-1];
    boost::posix_time::time_duration delta_y_j = y._point.times[j-1] - y._observed_start_times[j-1];
    if(delta_x_j <= b && delta_y_j >= b){
        return (double)(std::min(b, delta_y_j)-std::max(a, delta_x_j)).total_seconds() / (double)(b-a).total_seconds();
    }
    return 0.0;
}

double stmr::algorithms::probability(const stmr::intersection& x, const stmr::intersection& y, const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, unsigned int n){
    const std::vector<unsigned>& related_observations = stmr::algorithms::related_instances(n, x._periodicity, x._point.times.size());
    return std::accumulate(related_observations.cbegin(), related_observations.cend(), (double)0.0, boost::lambda::_1 + boost::lambda::bind(&stmr::algorithms::favourability, x, y, a, b, boost::lambda::_2)) / (double)related_observations.size();
}

double stmr::algorithms::rank(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, const boost::posix_time::time_duration& a, const boost::posix_time::time_duration& b, unsigned int n){
    double sum = 0.0;
    for(std::vector<stmr::intersection>::const_iterator it = begin; it < std::prev(end); it += 2){
        sum += probability(*it, *(it+1), a, b, n);
    }
    return sum;
}

stmr::delta_interval_map_type stmr::algorithms::favourability_map_delta(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, unsigned int n){
    delta_interval_map_type interval_map;
    std::vector<boost::posix_time::time_duration> Dn = combined_delta_distribution(begin, end, n);
    for(std::vector<boost::posix_time::time_duration>::const_iterator dit = Dn.cbegin(); dit != std::prev(Dn.cend()); ++dit){
        const boost::posix_time::time_duration& a = *dit;
        const boost::posix_time::time_duration& b = *(dit+1);
        double rank_n_ab = stmr::algorithms::rank(begin, end, a, b, n);
        interval_map += std::make_pair(stmr::delta_interval_type(a, b), rank_n_ab);
    }
    return interval_map;
}

std::pair<unsigned int, unsigned int> stmr::algorithms::combined_bounding_instances(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, const boost::posix_time::ptime& start, const boost::posix_time::ptime& finish){
    unsigned min_ns = std::numeric_limits<unsigned>::max(), max_ne = std::numeric_limits<unsigned>::min();
    for(auto xit = begin; xit < end; ++xit){
        unsigned ns, ne;
        std::tie(ns, ne) = bounding_instances(xit->_point.times.cbegin(), xit->_point.times.cend(), start, finish, xit->_periodicity, xit->_period_size);
        if(ns < min_ns){
            min_ns = ns;
        }
        if(ne > max_ne){
            max_ne = ne;
        }
    }
    return std::make_pair(min_ns, max_ne);
}


stmr::time_interval_map_type stmr::algorithms::favourability_map(std::vector<stmr::intersection>::const_iterator begin, std::vector<stmr::intersection>::const_iterator end, const boost::posix_time::ptime& start, const boost::posix_time::ptime& finish, std::vector<boost::posix_time::ptime> start_times){
    unsigned mu_s = stmr::algorithms::periodicity(start_times.begin(), start_times.end());
    unsigned long lambda_s = stmr::algorithms::period_size(start_times.begin(), start_times.end(), mu_s);
    
    unsigned ns, ne;
    std::tie(ns, ne) = combined_bounding_instances(begin, end, start, finish);
    
    stmr::time_interval_map_type temporal_map;
    for(unsigned n = ns; n <= ne; ++n){
        stmr::delta_interval_map_type delta_map = favourability_map_delta(begin, end, n);
        for(stmr::delta_interval_map_type::const_iterator dit = delta_map.begin(); dit != delta_map.end(); ++dit){
            stmr::delta_interval_type delta_interval = dit->first;
            double rank = dit->second;
            
            boost::posix_time::time_duration a = delta_interval.lower();
            boost::posix_time::time_duration b = delta_interval.upper();
            boost::posix_time::ptime start_n = stmr::algorithms::extrapolate_time(start_times.begin(), start_times.end(), n , mu_s, lambda_s);
            
            stmr::time_interval_type tinterval(start_n + a, start_n + b);
            temporal_map += std::make_pair(tinterval, rank);
        }
    }
    
    return temporal_map;
}

