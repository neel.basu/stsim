#ifndef TRACKTREEMODEL_H
#define TRACKTREEMODEL_H

#include "tracklayer.h"
#include "treemodelaux.h"
#include <QAbstractItemModel>
#include <QMap>


class TrackTreeModel : public QAbstractItemModel, public TreeModelAUX<AbstractTrackLayer, 2>{
  typedef TreeModelAUX<AbstractTrackLayer, 2>         aux_type;
  typedef TreeModelAUXItem<AbstractTrackLayer, 2, -1> RootNode;
  typedef TreeModelAUXItem<AbstractTrackLayer, 2,  0> TrackNode;
  typedef TreeModelAUXItem<AbstractTrackLayer, 2,  1> LocationNode;
  typedef TreeModelAUXItem<AbstractTrackLayer, 2,  2> TimeNode;
  
  Q_OBJECT
  public:
    virtual QVariant value(AbstractType* item, std::size_t column, int role) const;
    virtual QModelIndex populate(data_ptr_type data) override;
  public:
    QModelIndex repopulate(data_ptr_type data);
  public:
    explicit TrackTreeModel (QObject* parent = nullptr);
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;
  public:
    stmr::gps_coordinate lastLocation(const QModelIndex &index) const;
    boost::posix_time::ptime lastTime(const QModelIndex &index) const;
    std::string lastLabel(const QModelIndex &index) const;
    unsigned lastIndex(const QModelIndex &index) const;
    void addObservationPoint(const QModelIndex& parent, const stmr::observation_data& vertex);
    void insertObservationPoint(const QModelIndex& index, const stmr::observation_data& vertex);
    void reset();
};

#endif // TRACKTREEMODEL_H
