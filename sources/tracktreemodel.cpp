#include "tracktreemodel.h"
#include <QIcon>
#include <boost/format.hpp>
#include <QDebug>

TrackTreeModel::TrackTreeModel (QObject* parent): QAbstractItemModel(parent), aux_type(){}

Qt::ItemFlags TrackTreeModel::flags(const QModelIndex& index) const{
    if (!index.isValid())
        return 0;
    return aux_type::flags(index, QAbstractItemModel::flags(index));
}

QModelIndex TrackTreeModel::index(int row, int column, const QModelIndex& parent) const{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    AbstractType* parentItem;

    if (!parent.isValid()){
        parentItem = const_cast<AbstractType*>(self());
    }else{
        parentItem = static_cast<AbstractType*>(parent.internalPointer());
    }
    AbstractType* childItem = parentItem->item(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TrackTreeModel::parent(const QModelIndex& index) const{
    if (!index.isValid())
        return QModelIndex();

    AbstractType* childItem = static_cast<AbstractType*>(index.internalPointer());
    AbstractType* parentItem = childItem->parentItem();
    if (parentItem->depth() == -1)
        return QModelIndex();

    return createIndex(parentItem->position(), 0, parentItem);
}

int TrackTreeModel::rowCount(const QModelIndex& parent) const{
    return aux_type::rowCount(parent);
}

int TrackTreeModel::columnCount(const QModelIndex& parent) const{
    return aux_type::columnCount(parent);
}

QVariant TrackTreeModel::data(const QModelIndex& index, int role) const{
    return aux_type::data(index, role);
}

bool TrackTreeModel::setData(const QModelIndex& index, const QVariant& value, int role){
    return aux_type::setData(index, value, role);
}

QModelIndex TrackTreeModel::populate(AbstractTreeModelAUXItem<AbstractTrackLayer>::data_ptr_type data){
    auto rc = rowCount();
    beginInsertRows(QModelIndex(), rc, rc);
    auto track = create(data);
    for(int i = 0; i != data->count(); ++i){ // iterate through points
        auto child = track->create(data);
        for(int j = 0; j != data->expectations(i); ++j){ // iterate through times
            child->create(data);
        }
    }
    endInsertRows();
    return createIndex(rc, rc, track);
}

QModelIndex TrackTreeModel::repopulate(data_ptr_type data){
    TrackNode* track = 0x0;
    for(auto child: _children){
        if(child->actual() == data){
            track = child;
            break;
        }
    }
    if(track == 0x0){
        return QModelIndex();
    }
    
    QModelIndex parent = createIndex(track->position(), track->position(), track);
    beginRemoveRows(parent, 0, track->count());
    track->clear();
    endRemoveRows();
    
    beginInsertRows(parent, 0, data->count());
    for(int i = 0; i != data->count(); ++i){ // iterate through points
        auto child = track->create(data);
        for(int j = 0; j != data->expectations(i); ++j){ // iterate through times
            child->create(data);
        }
    }
    endInsertRows();
    
    return createIndex(track->position(), track->position(), track);
}

QVariant TrackTreeModel::value(AbstractType* item, std::size_t column, int role) const{
    if(item->depth() == -1) return QVariant();

    if(role == Qt::DecorationRole && item->actual()->visible()){
        if(item->depth() == 0){
            QColor color = item->actual()->color();
            QPixmap pixmap(20, 20);
            pixmap.fill(Qt::transparent);
            QPainter painter(&pixmap);
            painter.setPen(QPen(color));
            painter.setBrush(QBrush(color));
            painter.drawEllipse(0, 0, 16, 16);
            if(item->actual()->type() == MergedTrack){
                painter.setBrush(Qt::white);
                painter.drawEllipse(2, 2, 12, 12);
            }
            return QIcon(pixmap);
        }else if(item->depth() == 1){
            QPixmap pixmap = QPixmap(":/pix/gps.png");
            return QIcon(pixmap);
        }else if(item->depth() == 2){
            QPixmap pixmap = QPixmap(":/pix/clock.png");
            return QIcon(pixmap);
        }
    }
    if(role == Qt::DisplayRole){
         if(item->depth() == 0){
            return (boost::format("%1% (%2%)") % item->actual()->name() % item->actual()->count()).str().c_str();
        }else if(item->depth() == 1){
            stmr::gps_coordinate point = item->actual()->at(item->position());
            std::string str = (boost::format("(%3.5f, %3.5f)") % point.lat % point.lng).str();
            return str.c_str();
        }else if(item->depth() == 2){
            boost::posix_time::ptime time = item->actual()->expectation(item->parentItem()->position(), item->position());
            try{
                std::string time_str = boost::posix_time::to_simple_string(time);
                return time_str.c_str();
            }catch(...){
                return std::string("----").c_str();
            }
        }
    }
    return QVariant();
}

stmr::gps_coordinate TrackTreeModel::lastLocation(const QModelIndex& index) const{
    AbstractType* item = static_cast<AbstractType*>(index.internalPointer())->ancestor(0);
    AbstractTrackLayer* layer = item->actual();
    stmr::gps_coordinate loc = layer->at(layer->count()-1);
    return loc;
}

boost::posix_time::ptime TrackTreeModel::lastTime(const QModelIndex& index) const{
    AbstractType* item = static_cast<AbstractType*>(index.internalPointer())->ancestor(0);
    AbstractTrackLayer* layer = item->actual();
    boost::posix_time::ptime time = layer->expectation(layer->count()-1, 0);
    return time;
}

std::string TrackTreeModel::lastLabel(const QModelIndex& index) const{
    AbstractType* item = static_cast<AbstractType*>(index.internalPointer())->ancestor(0);
    AbstractTrackLayer* layer = item->actual();
    return layer->label(layer->count()-1);
}

unsigned TrackTreeModel::lastIndex(const QModelIndex& index) const{
    AbstractType* item = static_cast<AbstractType*>(index.internalPointer())->ancestor(0);
    AbstractTrackLayer* layer = item->actual();
    return layer->idx(layer->count()-1);
}


void TrackTreeModel::addObservationPoint(const QModelIndex& parent, const stmr::observation_data& vertex){
    AbstractType* item = static_cast<AbstractType*>(parent.internalPointer());
    QModelIndex rowIndex = parent;
    while(item->depth() != 0){
        rowIndex = rowIndex.parent();
        item = static_cast<AbstractType*>(rowIndex.internalPointer());
    }
    data_ptr_type data = item->actual();
    if(data->type() == ObservationTrack){
        QAbstractItemModel::beginInsertRows(rowIndex, rowCount(rowIndex), rowCount(rowIndex));
        ObservationTrackLayer* observation_layer = dynamic_cast<ObservationTrackLayer*>(data);
        observation_layer->append(vertex);
        
        LocationNode* node = new LocationNode(data);
        dynamic_cast<TrackNode*>(item)->add(node);
        TimeNode* tnode = new TimeNode(data);
        node->add(tnode);
        
        QAbstractItemModel::endInsertRows();
    }
}

void TrackTreeModel::insertObservationPoint(const QModelIndex& index, const stmr::observation_data& vertex){
    AbstractType* item = static_cast<AbstractType*>(index.internalPointer());
    if(item->depth() == 0){
        return addObservationPoint(index, vertex);
    }
    
    data_ptr_type data = item->actual();
    ObservationTrackLayer* layer = dynamic_cast<ObservationTrackLayer*>(data);
    if(!layer) return;
    
    QModelIndex parent = index.parent();
    int row = item->position();

    QAbstractItemModel::beginInsertRows(parent, row, row);
    layer->pivot_insert(vertex);
    LocationNode* node = new LocationNode(data);
    dynamic_cast<TrackNode*>(item->parentItem())->add(node);
    TimeNode* tnode = new TimeNode(data);
    node->add(tnode);
    QAbstractItemModel::endInsertRows();
}


void TrackTreeModel::reset(){
    beginResetModel();
    endResetModel();
}
