#include "mergedialog.h"
#include "stmr.h"
#include "ui_mergedialog.h"
#include <QColorDialog>
#include <QDebug>

MergeDialog::MergeDialog(TrackTreeModel* treeModel, QModelIndex observationIndex, QModelIndex mergedIndex, QWidget *parent): QDialog(parent), ui(new Ui::MergeDialog), _observationTrack(0x0), _mergedTrack(0x0){
    ui->setupUi(this);

    _color = Qt::red;
    ui->buttonBox->setDisabled(true);
    
    connect(ui->colorButton, SIGNAL(clicked()), this, SLOT(colorSelectionClicked()));
    
    if(observationIndex.isValid()){
        AbstractTreeModelAUXItem<AbstractTrackLayer>* internal_data = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(observationIndex.internalPointer());
        ui->observationTreeView->setModel(treeModel);
        ui->observationTreeView->setRootIndex(observationIndex);
        _observationTrack = dynamic_cast<ObservationTrackLayer*>(internal_data->actual());
    }
    
    if(mergedIndex.isValid()){
        AbstractTreeModelAUXItem<AbstractTrackLayer>* internal_data = static_cast<AbstractTreeModelAUXItem<AbstractTrackLayer>*>(mergedIndex.internalPointer());
        ui->mergedTreeView->setModel(treeModel);
        ui->mergedTreeView->setRootIndex(mergedIndex);
        ui->mergedTrackLabelWidget->hide();
        _mergedTrack = dynamic_cast<MergedTrackLayer*>(internal_data->actual());
        _color = _mergedTrack->color();
        
        QPalette pal = ui->colorButton->palette();
        pal.setColor(QPalette::Button, _color);
        ui->colorButton->setAutoFillBackground(true);
        ui->colorButton->setPalette(pal);
        ui->colorButton->update();
        
        ui->buttonBox->setDisabled(false);
    }else{
        ui->mergedTrackLabelWidget->show();
        connect(ui->lineEdit, &QLineEdit::textChanged, [this](const QString& text){
            ui->buttonBox->setEnabled(!ui->lineEdit->text().isEmpty());
        });
    }

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(okaySlot()));
}

MergeDialog::~MergeDialog(){
    delete ui;
}

QColor MergeDialog::color() const{
    return _color;
}

QString MergeDialog::name() const{
    return ui->lineEdit->text();
}

void MergeDialog::colorSelectionClicked(){
    _color = QColorDialog::getColor(Qt::blue, this);
    QPalette pal = ui->colorButton->palette();
    pal.setColor(QPalette::Button, _color);
    ui->colorButton->setAutoFillBackground(true);
    ui->colorButton->setPalette(pal);
    ui->colorButton->update();
}

void MergeDialog::okaySlot(){
    std::vector<stmr::observation_data> observation_collection = _observationTrack->collection();
    if(!_mergedTrack){
        _merged_collection = stmr::algorithms::merge(observation_collection);
    }else{
       _iterations.clear();
        auto lambda = [this](const stmr::local_view& view, const stmr::merged_data& target, stmr::skewness skew){
            std::string state_str = "NULL";
            if(view.pstate() == stmr::state_a){
                state_str = "A";
            }else if(view.pstate() == stmr::state_b){
                state_str = "B";
            }else if(view.pstate() == stmr::state_c){
                state_str = "C";
            }
            
            std::string skew_str = "?";
            if(skew == stmr::skew_p){
                skew_str = "P";
            }else if(skew == stmr::skew_q){
                skew_str = "Q";
            }else if(skew == stmr::skew_v){
                skew_str = "V";
            }
            
            _iterations.push_back(std::make_pair(view.current, target));
            std::cout   << view.current.p.label << "/" << view.current.p.index << " "
                        << view.current.q.label << "/" << view.current.q.index << " "
                        << view.current.t.label << "/" << view.current.t.index << " "
                        << target.label << "/" << target.index << " "
                        << state_str << " " << skew_str << std::endl;
            
        };
        
        std::vector<stmr::merged_data> merged_collection_old = _mergedTrack->collection();
        int K = merged_collection_old.front().times.size()+1;
        _merged_collection = stmr::algorithms::merge(merged_collection_old, observation_collection, K, lambda);
    }
}

MergedTrackLayer* MergeDialog::createMergedLayer(Marble::MarbleWidget* widget, MergedTrackLayer* layer_old) const{
    MergedTrackLayer* layer = layer_old;
    if(!layer_old){
        layer = new MergedTrackLayer(widget);
    }
    
    layer->clear();
    
    for(auto it = _merged_collection.cbegin(); it != _merged_collection.cend(); ++it){
        layer->append(*it);
    }
    return layer;
}
