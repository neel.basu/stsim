#include "sources/mergedtrackhullmodel.h"
#include "tracktreemodel.h"
#include <QDebug>

MergedTrackHullModel::MergedTrackHullModel(QObject* parent): QSortFilterProxyModel(parent){}


bool MergedTrackHullModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const{
    QAbstractItemModel* source = sourceModel();
    if(!source) return false;
    TrackTreeModel* sourceTree = dynamic_cast<TrackTreeModel*>(source);
    if(!sourceTree) return false;
    
    if(!source_parent.isValid()){
        AbstractTreeModelAUXItem<AbstractTrackLayer>* abstractLayer = sourceTree->item(source_row);
        return abstractLayer->actual()->type() == MergedTrack;
    }
    return true;
}

QVariant MergedTrackHullModel::data(const QModelIndex& index, int role) const{
    typedef AbstractTreeModelAUXItem<AbstractTrackLayer> AbstractType;
    QAbstractItemModel* source = sourceModel();
    QVariant d = QSortFilterProxyModel::data(index, role);
    qDebug() << d;
    if(role == Qt::DecorationRole && index.isValid() && index.parent().isValid() && rowCount(index) == 1){
        QModelIndex sindex = mapToSource(index);
        AbstractType* item = static_cast<AbstractType*>(sindex.internalPointer());
        if(item->depth() == 1){
            AbstractTrackLayer* layer = item->actual();
            MergedTrackLayer* mlayer = dynamic_cast<MergedTrackLayer*>(layer);
            stmr::gps_coordinate location = layer->at(item->position());
            for(auto i = mlayer->hbegin(); i != mlayer->hend(); ++i){
                if(location == i->location){
                    TrackTreeModel* sourceTree = dynamic_cast<TrackTreeModel*>(sourceModel());
                    return sourceTree->value(item->parentItem(), 0, Qt::DecorationRole);
                }
            }
        }
    }
    return d;
}
