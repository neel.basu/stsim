/*
 * Copyright 2019 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIMELINEGRAPHICSVIEW_H
#define TIMELINEGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>

class FenceTrackResultsModel;

/**
 * @todo write docs
 */
class TimelineGraphicsView : public QGraphicsView{
  Q_OBJECT
  double _rulerHeight;
  double _labelHeight;
  int    _extraWidth;
  QGraphicsScene _scene;
  public:
    TimelineGraphicsView(QWidget* parent = 0x0);
  public:
    typedef std::pair<double, double> bounds_type;
    typedef double value_type;
  public:
    virtual size_t count() const = 0;
    virtual bounds_type bounds(size_t i) const = 0;
    virtual value_type value(size_t i) const = 0;
    virtual QString lower(size_t i) const = 0;
    virtual QString upper(size_t i) const = 0;
    virtual int length() const;
  public:
    void render();
    virtual void resizeEvent(QResizeEvent* event) override;
    virtual void wheelEvent(QWheelEvent* event) override;
};

class TimelineView: public TimelineGraphicsView{
  FenceTrackResultsModel* _model;
  public:
    TimelineView(FenceTrackResultsModel* model);
  public:
    virtual size_t count() const;
    virtual bounds_type bounds(size_t i) const;
    virtual value_type value(size_t i) const;
    virtual QString lower(size_t i) const;
    virtual QString upper(size_t i) const;
    virtual int length() const;
};

#endif // TIMELINEGRAPHICSVIEW_H
