Implementation and Visualization of the following paper
Sunanda Bose, Sumit Paul, and Nandini Mukherjee (2021). [“Predicting Spatio-Temporal Phenomena of Mobile Resources in Sensor Cloud Infrastructure”](https://dl.acm.org/doi/abs/10.1145/3446936). In: ACM Trans. Spatial Algorithms Syst. 7.3.


The paper proposes p-time algorithm to merge multiple travel routes into one without requiring map matching. The algorithm doesn't require existence of a road map. Rather it can be used to discover roads/travel paths from historical data.


### Demonstration Video (Youtube https://www.youtube.com/watch?v=ldbPtIvzxPY)

[![Demonstration Video](screenshots/5-tracks-animation.gif "Demonstration Video")](https://www.youtube.com/watch?v=ldbPtIvzxPY)


### Screenshots
![](screenshots/5-tracks-observations.png?raw=true)
![](screenshots/5-tracks-merge.png?raw=true)
